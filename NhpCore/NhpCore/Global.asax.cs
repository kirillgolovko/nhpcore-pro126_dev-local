﻿using NhpCore.Common;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.Infrastructure.Data.Ef.Initializer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace NhpCore
{
    internal enum AppMode
    {
        NotDefined = 0,
        Debug = 1,
        Release = 2,
        Custom = 3
    }

    internal static class ApplicationInfo
    {
        public static AppMode Mode { get; private set; }

        public static void DefineAppMode()
        {
#if DEBUG
            Mode = AppMode.Debug;
#else
            Mode = AppMode.Release;
#endif
        }
    }

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()

        {
            var mode = ApplicationInfo.Mode;

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DbConfig.Configure();
            DependencyConfig.Configure();

            // Database.SetInitializer(new MigrateDatabaseToLatestVersion<NhpCoreDbContext, NhpCoreDbContextConfiguration>());
            Database.SetInitializer(new NhpCoreDbContextCreateIfNotExistInitializer());
            using (var db = NhpCoreDbContext.Create())
            {
                db.Database.Initialize(true);
            }
            using (var db = NhpIdentityDbContext.Create())
            {
                db.Database.Initialize(true);
            }
        }

        
    }
}