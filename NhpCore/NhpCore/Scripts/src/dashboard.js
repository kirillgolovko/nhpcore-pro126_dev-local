﻿$(document).ready(function () {
    getAvailableOrgs();
    getSubscriptions();
    getGeneralUserInformation();
    $('#myModal').modal('hide');
});

var periods = [{ duration: 12, title: "1 год" }, { duration: 24, title: "2 год" }];
var generalUserInformation = { displayName: '', email:'' };

var vueInviteTabState = {
    users: [],
    organisations: [],
    selectedOrganisation: {},
    emails: "",
    orgLoading: false,
    userLoading: false,
    isInviting: false
};

var vueSubscriptionTabState = {
    balance: 0,
    users: [],
    organisations: [],
    subscriptions: [],
    subscriptionPeriods: periods,

    selectedOrganisation: {},
    selectedSubscription: '',
    selectedSubscriptionPeriod: '',
    //  sumValue: 0, computed
    useOrgBalance: false,
    isAllSelected: false,
    orgLoading: false,
    subscrLoading: false,
    subscrUsersLoading: false
};

//создание новых организаций, редактирование реквизитов
var vueOrganisationManagementTabState = {
    organisations: [],
    selectedOrganisation: {},
    selectedEntry: {},
    newOrganisation: { name: '' },
    isDetailsActive: false,
    orgLoading: false,
    isCreating: false,
    isCreatingProgress: false,
    //yandexStorageLink:'', computed
    isYandexStorageConnected: false
};

// редактирование личных данных
var vueProfileDataTabState = {
    organisations: [],
    profiles: [],
    selectedOrganisation: {},
    selectedUserOrganisationProfile: {},
    generalUserInfo: generalUserInformation,
    orgLoading: false,
    isCreateMode: false,
    isEditMode: false,
    isGeneralLoading: false,
    isGeneralSelected: false
};

var vueApp = new Vue({
    el: "#app",
    data: {
        generalUserInfo: generalUserInformation
    }
});

var vueInviteTab = new Vue({
    el: "#inviteTab",
    data: vueInviteTabState,
    methods: {
        selectOrganisation: function (organisation) {
            /*
                1* reload invited users list if selected another organisation
                2* clear invite-emails text-area
                3* set selectedOrganisation
                4* clear users before load new InvitedUsers
            */

            // 4*
            this.user = [];

            // 1*
            loadInvitetedUsersInfo(organisation.id);

            // 2*
            this.emails = "";

            // 3*
            this.selectedOrganisation = organisation;
        },
        invite: function () {

            // null если одна строка
            // [] если есть разрывы
            var isMultiline = /\r|\n/.exec(this.emails);

            var splitted = this.emails.split(/(\r\n|\n|\r)/gm);

            var filtered = splitted.filter(function (value) {
                var filterResult = value !== '\n' && value !== '\r' && value !== '\r\n';
                return filterResult;
            });

            var emailsArray = isMultiline === null ? [this.emails] : filtered;

            $.ajax({
                url: '../Organisation/InviteUsers',
                type: 'POST',
                data: {
                    organisationId: this.selectedOrganisation.id,
                    emails: emailsArray /*передать массив имейл адресов*/
                },
                dataType: "json",
                traditional: true,
                beforeSend: function () { /*запустить анимацию*/

                },
                success: function (response) {
                    /* уведомление об успешном добавлении*/
                    var parsedResponse = JSON.parse(response);
                    var invalidEmails = parsedResponse.invalidEmails;
                    var failedEmails = parsedResponse.inviteResult.FailedEmails;
                    loadInvitetedUsersInfo(vueInviteTabState.selectedOrganisation.id);
                    //TODO: show window with failed and invalid emails
                },
                complete: function () {

                }
            });
        },
        validateEmails: function () {
            // regexp validation
            /* something like that = .{0,}@\w{0,}\.\w{0,} = */
        },
        remove: function (user) {
            user.isDeleting = true;

            $.ajax({
                url: '../Organisation/DeleteInvitedUser',
                type: 'POST',
                data: {
                    id: user.id
                },
                beforeSend: function () { /*запустить  анимацию об удалении*/ },
                success: function (response) {
                    var parsedResponse = JSON.parse(response);

                    // удалить из приглашений
                    var findedUser = vueInviteTabState.users.filter(function (user) {
                        var expression = user.id === parsedResponse.DeletedId;
                        return expression;
                    });
                    if (findedUser.length === 1) {
                        var delUser = findedUser[0];

                        var index = vueInviteTabState.users.indexOf(delUser);
                        vueInviteTabState.users.splice(index, 1);

                        if (delUser.status === true) {
                            // remove from subscribe tab (удалить из стейта подписок, обновить суммарную цену)
                            var findedSubscrUser = vueSubscriptionTabState.users.filter(function (el) {
                                return el.id === user.userId;
                            });

                            if (findedSubscrUser.length === 1) {
                                var subUser = findedSubscrUser[0];
                                var indexInSubscr = vueSubscriptionTabState.users.indexOf(subUser);
                                vueSubscriptionTabState.splice(indexInSubscr, 1);
                            }
                        }
                    }
                },
                error: function () {
                    user.isDeleting = false;
                },
                complete: function () {

                }
            });
        }
    },
    watch: {
        selectedOrganisation: function (selectedOrg) {
            loadInvitetedUsersInfo(selectedOrg.id);
        }
    }
});

var vueSubscriptionTab = new Vue({
    el: "#subscriptionTab",
    data: vueSubscriptionTabState,
    methods: {
        selectOrganisation: function (organisation) {
            /*
                1* reload bought subscriptions if selected another organisation
                2* set balance of organisation
                3* set selectedOrganisation
                4* clear users before load new InvitedUsers

            */

            if (this.selectedOrganisation.id === organisation.id) { return; } // TODO Uncomment after tests

            // 4*
            this.users = [];

            // 1*
            if (this.selectedSubscription !== '') {
                loadSubscriptionUserInfo(organisation.id, this.selectedSubscription.id);
            }

            // 2*
            //get balanceValue from attribute data-balanceValue and set like current organisation balance
            //var balanceValue = organisation.currentTarget.firstChild.getAttribute("data-balanceValue");
            //var number = Number(balanceValue);
            this.balance = organisation.balanceValue;

            // 3*
            this.selectedOrganisation = organisation;
        },
        onSubscriptionChanged: function (event) {
            loadSubscriptionUserInfo(this.selectedOrganisation.id, this.selectedSubscription.id);
        },
        pay: function (event) {
            var userIds = [];
            for (var i = 0; i < this.users.length; i++) {
                if (this.users[i].isSelected) {
                    userIds.push(this.users[i].userId);
                }
            }

            $.ajax({
                url: '../Subscriptions/Pay',
                type: 'POST',
                data: {
                    organisationId: this.selectedOrganisation.id,
                    subscriptionId: this.selectedSubscription.id,
                    duration: this.selectedSubscriptionPeriod.duration,
                    users: userIds
                },
                beforeSend: function () { },
                success: function () {
                }
            });
        },
        selectAll: function (event) {
            var assignableValue = this.isAllSelected;

            for (var i = 0; i < this.users.length; i++) {
                this.users[i].isSelected = assignableValue;
            }
        },
    },
    computed: {
        subscrRetailPrice: function () {
            var result = "";

            // расчет стоимости 1 подписки на выбранный период
            var price = this.selectedSubscriptionPeriod.duration * this.selectedSubscription.price;
            var isNan = Number.isNaN(price);

            result = isNan === true ? "" : Math.ceil(price);

            return result;
        },
        sumValue: function () {
            var result = 0;

            for (var i = 0; i < this.users.length; i++) {
                var item = this.users[i];

                if (item.isSelected) {
                    result += this.subscrRetailPrice;
                }
            }

            return result;
        }
    },
    watch: {
        selectedOrganisation: function (selectedOrg) {
            this.balance = selectedOrg.balanceValue;

            if (this.selectedSubscription !== '') {
                loadSubscriptionUserInfo(selectedOrg.id, this.selectedSubscription.id);
            }
        },
    }
});

var vueOrganisationManagementTab = new Vue({
    el: '#organisationManagementTab',
    data: vueOrganisationManagementTabState,
    methods: {
        selectOrganisation: function (organisation) {
            //this.yandexStorageLink= 'https://oauth.yandex.ru/authorize?response_type=code&client_id=845248035b2642b6ad1b12c14e2f90bb&state=';
            //this.yandexStorageLink += uid + 'o' + organisation.id;
            this.selectedOrganisation = organisation;
        },
        showCreateNewOrganisation: function () {
            this.isCreating = true;
        },
        cancelIsCreating: function () {
            this.isCreating = false;
            this.newOrganisation.name = '';
        },
        createNewOrganisation: function (organisation) {
            $.ajax({
                url: '../Organisation/CreateOrganisation',
                type:'POST',
                data: {
                    name: organisation.name
                },
                beforeSend: function () {
                    vueOrganisationManagementTabState.isCreatingProgress = true;
                },
                success: function (response) {
                    getAvailableOrgs();
                },
                complete: function () {
                    vueOrganisationManagementTabState.isCreatingProgress = false;
                }
            });
        }
    },
    computed: {
    },
    watch: {
        selectedOrganisation: function (selectedOrg) {
            this.yandexStorageLink = 'https://oauth.yandex.ru/authorize?response_type=code&client_id=845248035b2642b6ad1b12c14e2f90bb&state=';
            this.yandexStorageLink += uid + 'o' + selectedOrg.id;
        }
    }
});

var vuePersonalDataTab = new Vue({
    el: '#personalDataTab',
    data: vueProfileDataTabState,
    methods: {
        selectProfile: function (profile) {
            $('.toolbar .btn-warning').removeClass('disabled');
            $('.toolbar .btn-danger ').removeClass('disabled');
            var copiedProfile = jQuery.extend(true, {}, profile);
            this.selectedUserOrganisationProfile = copiedProfile;
        },
        createProfile: function () {
            this.selectedUserOrganisationProfile = {};
            this.isCreateMode = true;
        },
        editProfile: function (e) {
            if ($(e.target).hasClass('disabled')) { return false }
            this.isEditMode = true;
        },
        deleteProfile: function (e) {
            //Удаление профиля с проверкой
            if ($(e.target).hasClass('disabled')) { return false }
            if (confirm('Вы действительно хотите удалить профиль?')) {
                var copiedProfile = $.extend(true, {}, this.selectedUserOrganisationProfile);
                $.ajax({
                    url: '../Dashboard/DeleteProfile',
                    type: 'POST',
                    data: copiedProfile,
                    beforeSend: function () {
                        this.isGeneralLoading = true;
                    },
                    success: function (response) {
                        getGeneralUserInformation();
                    },
                    complete: function () {
                        this.isGeneralLoading = false;
                    }
                });
            }
        },
        saveProfile: function () {
            this.selectedUserOrganisationProfile.SNP = this.selectedUserOrganisationProfile.SecondName + ' ' + this.selectedUserOrganisationProfile.FirstName + ' ' + this.selectedUserOrganisationProfile.PatronymicName;
            var copiedProfile = $.extend(true, {}, this.selectedUserOrganisationProfile);
            if (this.isCreateMode) {
                //Создание профиля в базе
                $.ajax({
                    url: '../Dashboard/CreateProfile',
                    type: 'POST',
                    data: copiedProfile,
                    beforeSend: function () {
                        this.isGeneralLoading = true;
                    },
                    success: function (response) {
                        getGeneralUserInformation();
                    },
                    complete: function () {
                        this.isGeneralLoading = false;
                    }
                });
                
                this.isCreateMode = false;
            }
            if (this.isEditMode) {
                //Редактирование профиля в базе
                $.ajax({
                    url: '../Dashboard/EditProfile',
                    type: 'POST',
                    data: copiedProfile,
                    beforeSend: function () {
                        this.isGeneralLoading = true;
                    },
                    success: function (response) {
                        getGeneralUserInformation();
                    },
                    complete: function () {
                        this.isGeneralLoading = false;
                    }
                });
                this.isEditMode = false;
            }
        },
        changeOrgProfile: function (org) {
            alert(org.name);
        },
        generalInfoClick: function () {

        },
        selectOrganisation: function (organisation) {

        },
        saveGeneralName: function (name) {
            generalUserInformation.displayName = name;
            //TODO: Сохранение в базе
        }
    }
});

function loadSubscriptionUserInfo(organisationId, subscriptionId) {
    if (organisationId !== '' && organisationId !== null && subscriptionId !== '' && subscriptionId !== null) {
        $.ajax({
            url: '../Subscriptions/GetUsersInfo',
            type: 'GET',
            data: {
                organisationId: organisationId,
                subscriptionId: subscriptionId
            },
            beforeSend: function () {
                vueSubscriptionTabState.subscrUsersLoading = true;
            },
            success: function (response) {
                var parsedResponse = JSON.parse(response);
                vueSubscriptionTabState.users = [];

                for (var i = 0; i < parsedResponse.length; i++) {
                    // push objects in collection
                    //  vueSubscriptionTabState.users.push(parsedResponse[i]);
                    var user = parsedResponse[i];
                    user.isSelected = false;
                    vueSubscriptionTabState.users.push(user);
                }
            },
            complete: function () {
                vueSubscriptionTabState.subscrUsersLoading = false;
            }
        });
    }
};

function loadInvitetedUsersInfo(organisationId) {
    $.ajax({
        url: '../Organisation/GetInvitedUsers',
        type: 'GET',
        data: {
            organisationId: organisationId
        },
        beforeSend: function () {
            vueInviteTabState.userLoading = true;
        },
        success: function (response) {
            // проверить реализацию
            var parsedResponse = JSON.parse(response);
            // чистим массив приглашенных пользователей
            vueInviteTabState.users = [];

            for (var i = 0; i < parsedResponse.length; i++) {
                var user = parsedResponse[i];
                user.isDeleting = false;
                vueInviteTabState.users.push(user);
            }
        },
        complete: function () {
            vueInviteTabState.userLoading = false;
        }
    });
};

function getAvailableOrgs() {
    $.ajax({
        url: "../Organisation/GetAvailable",
        type: "GET",
        beforeSend: function () {
            // $(/*отобразить анимацию ожидания*/).show();
            vueInviteTabState.organisations = [];
            vueSubscriptionTabState.organisations = [];
            vueOrganisationManagementTabState.organisations = [];
            vueProfileDataTabState.organisations = [];

            vueInviteTabState.orgLoading = true;
            vueSubscriptionTabState.orgLoading = true;
            vueOrganisationManagementTabState.orgLoading = true;
            vueProfileDataTabState.orgLoading = true;
        },
        success: function (response) {
            //$(/*скрыть анимацию ожидания*/).hide();
            //if (response.ErrorCode !== 0) {
            //    showError(response.ErrorMessage);
            //    alert(response.ErrorMessage);
            //} else { //success req
            var selectedOrg = null;

            var parsedResponse = JSON.parse(response);
            // TODO: переделать чтобы с сервера отдавался общий массив, а здесь в цикли заполнялся каждый массив своими значениями
            for (var i = 0; i < parsedResponse.length; i++) {
                vueInviteTabState.organisations.push(parsedResponse[i]);
                vueSubscriptionTabState.organisations.push(parsedResponse[i]);
                vueOrganisationManagementTabState.organisations.push(parsedResponse[i]);
                vueProfileDataTabState.organisations.push(parsedResponse[i]);

                if (parsedResponse[i].isSelected === true) {
                    vueInviteTabState.selectedOrganisation = parsedResponse[i];
                    vueSubscriptionTabState.selectedOrganisation = parsedResponse[i];
                    vueOrganisationManagementTabState.selectedOrganisation = parsedResponse[i];
                    vueProfileDataTabState.selectedOrganisation = parsedResponse[i];
                }
            }
            //}
        },
        complete: function () {
            vueInviteTabState.orgLoading = false;
            vueSubscriptionTabState.orgLoading = false;
            vueOrganisationManagementTabState.orgLoading = false;
            vueProfileDataTabState.orgLoading = false;

        }
    });
};

function getSubscriptions() {
    $.ajax({
        url: "../Subscriptions/GetAvailableSubscriptions",
        type: "GET",
        beforeSend: function () {
            vueSubscriptionTabState.subscrLoading = true;
        },
        success: function (response) {
            var parsedResponse = JSON.parse(response);

            for (var i = 0; i < parsedResponse.length; i++) {
                // push objects in collection
                vueSubscriptionTabState.subscriptions.push(parsedResponse[i]);
            }

        },
        complete: function () {
            vueSubscriptionTabState.subscrLoading = false;
        }
    });
};

function getGeneralUserInformation() {
    // Получаем список доступных профилей пользователя
    $.ajax({
        url: "../Dashboard/GetAvailableProfiles",
        type: "GET",
        beforeSend: function () {
            vueProfileDataTabState.profiles = [];
            vueProfileDataTabState.selectedUserOrganisationProfile = {};
            vueProfileDataTabState.isGeneralLoading = true;
        },
        success: function (response) {
            var parsedResponse = JSON.parse(response);
            for (var i = 0; i < parsedResponse.length; i++) {
                if (i === 0) {
                    generalUserInformation.displayName = parsedResponse[i].DisplayName;
                    generalUserInformation.email = "dev@dev";
                }
                vueProfileDataTabState.profiles.push(parsedResponse[i]);
            }
        },
        complete: function () {
            vueProfileDataTabState.isGeneralLoading = false;
        }
    });
    /*
    $.ajax({
        url: '',
        type: 'GET',
        success: function (response) {
            var parsedResponse = JSON.parse(response);

            generalUserInformation.displayName = parsedResponse.displayName;
            generalUserInformation.email = parsedResponse.email;

        }
    });
    */
};