﻿using IdentityServer3.Core.Configuration;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.Owin.Security.Cookies;
using System.Web.Helpers;
using System.IdentityModel.Tokens;
using IdentityServer3.Core;
using IdentityServer.Configuration;
using IdentityManager.Configuration;
using System.Configuration;
using IdentityServerAdmin.Configuration;
using IdentityServer.CustomViewsConfiguration;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Protocols;
using System.Security.Claims;
using Microsoft.Owin.Security;
using IdentityModel.Client;
using NhpCore.CoreLayer.Entities;
using NhpCore.Infrastructure.Data.Ef;
using Serilog;
using System.Web.Http;
using IdentityServer3.AccessTokenValidation;
using Autofac;
using NhpCore.Infrastructure.DependencyResolution;
using Autofac.Integration.WebApi;
using Common.Mapping;
using NhpCore.Infrastructure.Utils.Mapping;

[assembly: OwinStartup(typeof(NhpCore.Startup))]

namespace NhpCore
{
    public static class OpenIdConnectAuthenticationOptionsGenerator
    {
        public async static Task<OpenIdConnectAuthenticationOptions> IdentityManagerAsync()
        {
            OpenIdConnectAuthenticationOptions result = new OpenIdConnectAuthenticationOptions()
            {
                AuthenticationType = "oidc",
                Authority = "https://localhost:57556/identity",
                ClientId = "idadm_client",
                RedirectUri = "https://localhost:57556",
                ResponseType = "id_token",
                UseTokenLifetime = false,
                Scope = "openid idadm",
                SignInAsAuthenticationType = "Cookies",
                Notifications = new Microsoft.Owin.Security.OpenIdConnect.OpenIdConnectAuthenticationNotifications
                {
                    SecurityTokenValidated = n =>
                    {
                        n.AuthenticationTicket.Identity.AddClaim(new Claim("id_token", n.ProtocolMessage.IdToken));
                        return Task.FromResult(0);
                    },
                    RedirectToIdentityProvider = async n =>
                    {
                        //if (n.ProtocolMessage.RequestType == Microsoft.IdentityModel.Protocols.OpenIdConnectRequestType.LogoutRequest)
                        //{
                        //    var res = await n.OwinContext.Authentication.AuthenticateAsync("Cookies");
                        //    if (res != null)
                        //    {
                        //        var id_token = res.Identity.Claims.GetValue("id_token");
                        //        if (id_token != null)
                        //        {
                        //            n.ProtocolMessage.IdTokenHint = id_token;
                        //            n.ProtocolMessage.PostLogoutRedirectUri = "https://localhost:44337/idm";
                        //        }
                        //    }
                        //}
                    }
                }
            };

            return result;
        }

        public async static Task<OpenIdConnectAuthenticationOptions> IdentityAdminAsync()
        {
            OpenIdConnectAuthenticationOptions result = new Microsoft.Owin.Security.OpenIdConnect.OpenIdConnectAuthenticationOptions
            {
                AuthenticationType = "oidc",
                Authority = "https://localhost:57556/identity",
                ClientId = "idadm_client",
                RedirectUri = "https://localhost:57556",
                ResponseType = "id_token",
                UseTokenLifetime = false,
                Scope = "openid idadm",
                SignInAsAuthenticationType = "Cookies",
                Notifications = new Microsoft.Owin.Security.OpenIdConnect.OpenIdConnectAuthenticationNotifications
                {
                    SecurityTokenValidated = n =>
                    {
                        n.AuthenticationTicket.Identity.AddClaim(new Claim("id_token", n.ProtocolMessage.IdToken));
                        return Task.FromResult(0);
                    },
                    RedirectToIdentityProvider = async n =>
                    {
                        //if (n.ProtocolMessage.RequestType == Microsoft.IdentityModel.Protocols.OpenIdConnectRequestType.LogoutRequest)
                        //{
                        //    var res = await n.OwinContext.Authentication.AuthenticateAsync("Cookies");
                        //    if (res != null)
                        //    {
                        //        var id_token = res.Identity.Claims.GetValue("id_token");
                        //        if (id_token != null)
                        //        {
                        //            n.ProtocolMessage.IdTokenHint = id_token;
                        //            n.ProtocolMessage.PostLogoutRedirectUri = "https://localhost:44337/idm";
                        //        }
                        //    }
                        //}
                    }
                }
            };

            return result;
        }

        public async static Task<OpenIdConnectAuthenticationOptions> DefaultAsync()
        {
            OpenIdConnectAuthenticationOptions result = new OpenIdConnectAuthenticationOptions
            {
                AuthenticationType = "oidc",
                Authority = "http://localhost:57556/identity",
                ClientId = "mvc.owin.hybrid",
                RedirectUri = "http://localhost:57556/",
                PostLogoutRedirectUri = "http://localhost:57556/",
                Scope = "openid profile read write offline_access",
                ResponseType = "code id_token",

                TokenValidationParameters = new TokenValidationParameters
                {
                    NameClaimType = "name",
                    RoleClaimType = "role"
                },

                SignInAsAuthenticationType = "Cookies",

                Notifications = new OpenIdConnectAuthenticationNotifications
                {
                    MessageReceived = n =>
                    {
                        return Task.FromResult(0);
                    },
                    SecurityTokenReceived = n =>
                    {
                        return Task.FromResult(0);
                    },
                    AuthorizationCodeReceived = async n =>
                    {
                        // use the code to get the access and refresh token
                        var tokenClient = new TokenClient(
                            "http://localhost:57556/identity/connect/token",
                            "mvc.owin.hybrid",
                            "secret");

                        var tokenResponse = await tokenClient.RequestAuthorizationCodeAsync(
                            n.Code, n.RedirectUri);

                        if (tokenResponse.IsError)
                        {
                            throw new Exception(tokenResponse.Error);
                        }

                        // use the access token to retrieve claims from userinfo
                        var userInfoClient = new UserInfoClient(
                        new Uri("http://localhost:52825/identity/connect/userinfo"),
                        tokenResponse.AccessToken);

                        var userInfoResponse = await userInfoClient.GetAsync();

                        // create new identity
                        var id = new ClaimsIdentity(n.AuthenticationTicket.Identity.AuthenticationType);
                        id.AddClaims(userInfoResponse.GetClaimsIdentity().Claims);

                        id.AddClaim(new Claim("access_token", tokenResponse.AccessToken));
                        id.AddClaim(new Claim("expires_at", DateTime.Now.AddSeconds(tokenResponse.ExpiresIn).ToLocalTime().ToString()));
                        id.AddClaim(new Claim("refresh_token", tokenResponse.RefreshToken));
                        id.AddClaim(new Claim("id_token", n.ProtocolMessage.IdToken));
                        id.AddClaim(new Claim("sid", n.AuthenticationTicket.Identity.FindFirst("sid").Value));

                        n.AuthenticationTicket = new AuthenticationTicket(
                            new ClaimsIdentity(id.Claims, n.AuthenticationTicket.Identity.AuthenticationType, "name", "role"),
                            n.AuthenticationTicket.Properties);
                    },

                    RedirectToIdentityProvider = n =>
                    {
                        // if signing out, add the id_token_hint
                        if (n.ProtocolMessage.RequestType == OpenIdConnectRequestType.LogoutRequest)
                        {
                            var idTokenHint = n.OwinContext.Authentication.User.FindFirst("id_token");

                            if (idTokenHint != null)
                            {
                                n.ProtocolMessage.IdTokenHint = idTokenHint.Value;
                            }
                        }

                        return Task.FromResult(0);
                    }
                }
            };

            return result;
        }

        public static OpenIdConnectAuthenticationOptions IdentityManager()
        {
            OpenIdConnectAuthenticationOptions result = new OpenIdConnectAuthenticationOptions()
            {
                AuthenticationMode = AuthenticationMode.Passive,
                AuthenticationType = "oidc_idmgr",
                Authority = "https://localhost:44313/identity",
                ClientId = "idadm_client",
                RedirectUri = "https://localhost:44313",
                ResponseType = "id_token",
                UseTokenLifetime = false,
                Scope = "openid idadm",
                SignInAsAuthenticationType = "Cookies",
                Notifications = new Microsoft.Owin.Security.OpenIdConnect.OpenIdConnectAuthenticationNotifications
                {
                    SecurityTokenValidated = n =>
                    {
                        n.AuthenticationTicket.Identity.AddClaim(new Claim("id_token", n.ProtocolMessage.IdToken));
                        return Task.FromResult(0);
                    },
                    RedirectToIdentityProvider = async n =>
                    {
                        //if (n.ProtocolMessage.RequestType == Microsoft.IdentityModel.Protocols.OpenIdConnectRequestType.LogoutRequest)
                        //{
                        //    var res = await n.OwinContext.Authentication.AuthenticateAsync("Cookies");
                        //    if (res != null)
                        //    {
                        //        var id_token = res.Identity.Claims.GetValue("id_token");
                        //        if (id_token != null)
                        //        {
                        //            n.ProtocolMessage.IdTokenHint = id_token;
                        //            n.ProtocolMessage.PostLogoutRedirectUri = "https://localhost:44337/idm";
                        //        }
                        //    }
                        //}
                    }
                }
            };

            return result;
        }

        public static OpenIdConnectAuthenticationOptions IdentityAdmin()
        {
            OpenIdConnectAuthenticationOptions result = new Microsoft.Owin.Security.OpenIdConnect.OpenIdConnectAuthenticationOptions
            {
                AuthenticationType = "oidc_idsrv_adm",
                Authority = "https://localhost:44313/identity",
                ClientId = "idadm_client",
                RedirectUri = "https://localhost:44313",
                ResponseType = "id_token",
                UseTokenLifetime = false,
                Scope = "openid idadm",
                SignInAsAuthenticationType = "Cookies",
                Notifications = new Microsoft.Owin.Security.OpenIdConnect.OpenIdConnectAuthenticationNotifications
                {
                    SecurityTokenValidated = n =>
                    {
                        n.AuthenticationTicket.Identity.AddClaim(new Claim("id_token", n.ProtocolMessage.IdToken));
                        return Task.FromResult(0);
                    },
                    RedirectToIdentityProvider = async n =>
                    {
                        //if (n.ProtocolMessage.RequestType == Microsoft.IdentityModel.Protocols.OpenIdConnectRequestType.LogoutRequest)
                        //{
                        //    var res = await n.OwinContext.Authentication.AuthenticateAsync("Cookies");
                        //    if (res != null)
                        //    {
                        //        var id_token = res.Identity.Claims.GetValue("id_token");
                        //        if (id_token != null)
                        //        {
                        //            n.ProtocolMessage.IdTokenHint = id_token;
                        //            n.ProtocolMessage.PostLogoutRedirectUri = "https://localhost:44337/idm";
                        //        }
                        //    }
                        //}
                    }
                }
            };

            return result;
        }

        public static OpenIdConnectAuthenticationOptions Default()
        {
            OpenIdConnectAuthenticationOptions result = new OpenIdConnectAuthenticationOptions
            {
                AuthenticationType = "oidc",
                Authority = "https://online.nhp-soft.ru/identity",
                ClientId = "core.mvc",
                RedirectUri = "https://online.nhp-soft.ru/",
                PostLogoutRedirectUri = "https://online.nhp-soft.ru/",
                Scope = "openid profile read write offline_access",
                ResponseType = "code id_token",

                TokenValidationParameters = new TokenValidationParameters
                {
                    NameClaimType = "name",
                    RoleClaimType = "role"
                },

                SignInAsAuthenticationType = "Cookies",

                Notifications = new OpenIdConnectAuthenticationNotifications
                {
                    MessageReceived = n =>
                    {
                        return Task.FromResult(0);
                    },
                    SecurityTokenReceived = n =>
                    {
                        return Task.FromResult(0);
                    },
                    AuthorizationCodeReceived = async n =>
                    {
                        // use the code to get the access and refresh token
                        var tokenClient = new TokenClient(
                            "https://online.nhp-soft.ru/identity/connect/token",
                            "core.mvc",
                            "146cd13c");

                        var tokenResponse = await tokenClient.RequestAuthorizationCodeAsync(
                            n.Code, n.RedirectUri);

                        if (tokenResponse.IsError)
                        {
                            throw new Exception(tokenResponse.Error);
                        }

                        // use the access token to retrieve claims from userinfo
                        var userInfoClient = new UserInfoClient(
                        new Uri("https://online.nhp-soft.ru/identity/connect/userinfo"),
                        tokenResponse.AccessToken);

                        var userInfoResponse = await userInfoClient.GetAsync();

                        // create new identity
                        var id = new ClaimsIdentity(n.AuthenticationTicket.Identity.AuthenticationType);
                        id.AddClaims(userInfoResponse.GetClaimsIdentity().Claims);

                        id.AddClaim(new Claim("access_token", tokenResponse.AccessToken));
                        id.AddClaim(new Claim("expires_at", DateTime.Now.AddSeconds(tokenResponse.ExpiresIn).ToLocalTime().ToString()));
                        id.AddClaim(new Claim("refresh_token", tokenResponse.RefreshToken));
                        id.AddClaim(new Claim("id_token", n.ProtocolMessage.IdToken));
                        id.AddClaim(new Claim("sid", n.AuthenticationTicket.Identity.FindFirst("sid").Value));

                        n.AuthenticationTicket = new AuthenticationTicket(
                            new ClaimsIdentity(id.Claims, n.AuthenticationTicket.Identity.AuthenticationType, "name", "role"),
                            n.AuthenticationTicket.Properties);
                    },

                    RedirectToIdentityProvider = n =>
                    {
                        // if signing out, add the id_token_hint
                        if (n.ProtocolMessage.RequestType == OpenIdConnectRequestType.LogoutRequest)
                        {
                            var idTokenHint = n.OwinContext.Authentication.User.FindFirst("id_token");

                            if (idTokenHint != null)
                            {
                                n.ProtocolMessage.IdTokenHint = idTokenHint.Value;
                            }
                        }

                        return Task.FromResult(0);
                    }
                }
            };

            return result;
        }

        public static OpenIdConnectAuthenticationOptions DefaultDebug()
        {
            var site = "http://localhost:57556";
            OpenIdConnectAuthenticationOptions result = new OpenIdConnectAuthenticationOptions
            {
                AuthenticationType = "oidc",
                Authority = $"{site}/identity",
                ClientId = "core",
                RedirectUri = $"{site}/",
                PostLogoutRedirectUri = $"{site}/",
                Scope = "openid profile read write offline_access",
                ResponseType = "code id_token",

                TokenValidationParameters = new TokenValidationParameters
                {
                    NameClaimType = "name",
                    RoleClaimType = "role"
                },

                SignInAsAuthenticationType = "Cookies",

                Notifications = new OpenIdConnectAuthenticationNotifications
                {
                    MessageReceived = n =>
                    {
                        return Task.FromResult(0);
                    },
                    SecurityTokenReceived = n =>
                    {
                        return Task.FromResult(0);
                    },
                    AuthorizationCodeReceived = async n =>
                    {
                        // use the code to get the access and refresh token
                        var tokenClient = new TokenClient(
                            $"{site}/identity/connect/token",
                            "core",
                            "b588");

                        var tokenResponse = await tokenClient.RequestAuthorizationCodeAsync(
                            n.Code, n.RedirectUri);

                        if (tokenResponse.IsError)
                        {
                            throw new Exception(tokenResponse.Error);
                        }

                        // use the access token to retrieve claims from userinfo
                        var userInfoClient = new UserInfoClient(
                        new Uri($"{site}/identity/connect/userinfo"),
                        tokenResponse.AccessToken);

                        var userInfoResponse = await userInfoClient.GetAsync();

                        // create new identity
                        var id = new ClaimsIdentity(n.AuthenticationTicket.Identity.AuthenticationType);
                        id.AddClaims(userInfoResponse.GetClaimsIdentity().Claims);

                        id.AddClaim(new Claim("access_token", tokenResponse.AccessToken));
                        id.AddClaim(new Claim("expires_at", DateTime.Now.AddSeconds(tokenResponse.ExpiresIn).ToLocalTime().ToString()));
                        id.AddClaim(new Claim("refresh_token", tokenResponse.RefreshToken));
                        id.AddClaim(new Claim("id_token", n.ProtocolMessage.IdToken));
                        id.AddClaim(new Claim("sid", n.AuthenticationTicket.Identity.FindFirst("sid").Value));

                        n.AuthenticationTicket = new AuthenticationTicket(
                            new ClaimsIdentity(id.Claims, n.AuthenticationTicket.Identity.AuthenticationType, "name", "role"),
                            n.AuthenticationTicket.Properties);
                    },

                    RedirectToIdentityProvider = n =>
                    {
                        // if signing out, add the id_token_hint
                        if (n.ProtocolMessage.RequestType == OpenIdConnectRequestType.LogoutRequest)
                        {
                            var idTokenHint = n.OwinContext.Authentication.User.FindFirst("id_token");

                            if (idTokenHint != null)
                            {
                                n.ProtocolMessage.IdTokenHint = idTokenHint.Value;
                            }
                        }

                        return Task.FromResult(0);
                    }
                }
            };

            return result;
        }
    }

    public static class IdentityServerConfigurationExtension
    {
        public static void ConfigureAuthentication(this IAppBuilder app)
        {
            app.UseResourceAuthorization(new AuthorizationManager());

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "Cookies"
            });

            //            OpenIdConnectAuthenticationOptions settings = null;
            //#if DEBUG
            //            settings = OpenIdConnectAuthenticationOptionsGenerator.DefaultDebug();

            //#else
            //            settings = OpenIdConnectAuthenticationOptionsGenerator.Default();
            //#endif
            //app.UseOpenIdConnectAuthentication(settings);
#if DEBUG
            app.UseOpenIdConnectAuthentication(OpenIdConnectAuthenticationOptionsGenerator.DefaultDebug());
#else
            app.UseOpenIdConnectAuthentication(OpenIdConnectAuthenticationOptionsGenerator.Default());
#endif


            //app.UseOpenIdConnectAuthentication(OpenIdConnectAuthenticationOptionsGenerator.IdentityAdmin());
            //app.UseOpenIdConnectAuthentication(OpenIdConnectAuthenticationOptionsGenerator.IdentityManager());
        }
    }

    public class Startup
    {
        private bool _isSslRequired = false;
        private string _connstrIdSrv = "NhpIdSrvDbContext";
        private string _connstrIdentity = "NhpIdentityDbContext";

        public void Configuration(IAppBuilder app)
        {
            Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .WriteTo
                    .File($"{AppDomain.CurrentDomain.BaseDirectory}\\logs\\IdentityServer3\\idsrv-logs.txt")
                    .CreateLogger();
            // AppDomain.CurrentDomain.BaseDirectory + "\\logs\\IdSrv3-Log.txt"
            //

            //  DbStringInit();


            // Настройка контекста базы данных, диспетчера пользователей и диспетчера входа для использования одного экземпляра на запрос
            app.CreatePerOwinContext(NhpIdentityDbContext.Create);
            app.CreatePerOwinContext(NhpCoreDbContext.Create);
            app.CreatePerOwinContext<CustomUserManager>(CustomUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            AntiForgeryConfig.UniqueClaimTypeIdentifier = Constants.ClaimTypes.Subject;
            JwtSecurityTokenHandler.InboundClaimTypeMap = new Dictionary<string, string>();

            app.ConfigureAuthentication();
            app.ConfigureIdentityServer(GetIdSrvConfigurationOptions());

            app.Map("/api", inner =>
            {
                var config = new HttpConfiguration();

                var builder = new ContainerBuilder();

                builder.RegisterApiControllers(typeof(MvcApplication).Assembly);
                builder.RegisterWebApiFilterProvider(config);

                builder.RegisterModule(new InfrastructureDataModule());
                builder.RegisterModule(new InfrastructureUtilsModule());
                builder.RegisterModule(new BusinessServicesModule());

                var mapper = new AutoMapperUtilityImpl();
                mapper.Initialize(MappingConfig.Configure());
                builder.RegisterInstance(mapper).As<IMapperUtility>();

                var container = builder.Build();

                config.DependencyResolver = new AutofacWebApiDependencyResolver(container);



                JwtSecurityTokenHandler.InboundClaimTypeMap.Clear();

                //app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
                //{
                //    Authority = "https://online.nhp-soft.ru/identity",
                //    EnableValidationResultCache = true,
                //});

                config.Routes.MapHttpRoute(
                   name: "DefaultApi",
                   routeTemplate: "{controller}/{id}",
                   defaults: new { id = RouteParameter.Optional }
               );

                inner.UseWebApi(config);
            });
        }

        private void DbStringInit()
        {
#if DEBUG
            _connstrIdSrv = "NhpIdSrvDbContext";
            _connstrIdentity = "NhpCoreDbContext";
#else
            _connstrIdSrv = "NhpIdSrv3";
            _connstrIdentity = "NhpIdentity";
#endif
        }

        public IdentityServerAdminConfigurationOptions GetIdSrvAdmConfigurationOptions()
        {
            IdentityServerAdminConfigurationOptions result = new IdentityServerAdminConfigurationOptions();
            result.IdSrvConnStr = _connstrIdSrv;
            result.IsSslRequired = _isSslRequired;

            return result;
        }

        private IdentityManagerConfigurationOptions GetIdMngrConfigurationOptions()
        {
            IdentityManagerConfigurationOptions result = new IdentityManagerConfigurationOptions();

            result.RequireSsl = _isSslRequired;
            result.IdentityDbConnStr = _connstrIdentity;

            return result;
        }

        private IdentityServerConfurationOptions GetIdSrvConfigurationOptions()
        {
            IdentityServerConfurationOptions result = new IdentityServerConfurationOptions();

            result.RequireSsl = _isSslRequired;
            result.CertificatePath = @"{0}bin\IdentityServer\{1}.pfx";
            result.CertificateName = "onlinenhpsoft";
            result.CertificatePassword = "helgard240490";
            result.SiteName = ConfigurationManager.AppSettings["options.issuerUri"];
            result.IdentityDbConnStr = _connstrIdentity;
            result.IdSrvDbConnStr = _connstrIdSrv;

            result.SetCustomViews = factory =>
            {
                factory.ConfigureIdentityServerCustomViews();
            };

            return result;
        }
    }
}