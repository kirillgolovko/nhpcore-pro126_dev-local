﻿using Common.Commands;
using Common.Logging;
using Common.Mapping;
using MoreLinq;
using Newtonsoft.Json;
using NhpCore.Attributes;
using NhpCore.Business.Commands;
using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Services;
using NhpCore.Infrastructure.Utils.ServiceSynchronization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NhpCore.Controllers
{
    public class OrganisationController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IOrganisationService _organisationService;
        private readonly IMapperUtility _mapperUtility;
        private readonly IServiceSynchronization _syncUtil;

        public OrganisationController(IUserService userService,
            IOrganisationService organisationService,
            IMapperUtility mapper,
            IServiceSynchronization syncUtil)
        {
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }
            if (mapper == null) { throw new ArgumentNullException(nameof(mapper)); }
            if (syncUtil == null) { throw new ArgumentNullException(nameof(syncUtil)); }

            _userService = userService;
            _organisationService = organisationService;
            _mapperUtility = mapper;
            _syncUtil = syncUtil;
        }

        [HttpPost, AjaxOnly]
        public async Task<ActionResult> CreateOrganisation(string name)
        {
            var result = string.Empty;

            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);

            // 1. создание организации
            var createCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateOrganisationArgs, CreateOrganisationResult>>();
            var createResult = await createCommand.Execute(new CreateOrganisationArgs { CreatorId = userId, ShortName = name });

            if (!createResult.IsSuccess)
            {
            }

            // 2. синхронизация по базе
            await _syncUtil.AddOrganisation(createResult.OrganisationId, createResult.Name, createResult.RegistrationDate);

            // 3. создание записи о пользователе в этой организации
            var createUserEntryCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserEntryArgs, CreateUserEntryResult>>();
            var userEntryResult = await createUserEntryCommand.Execute(new CreateUserEntryArgs { IdentityId = userId, IsPersonal = true, OrganisationId = createResult.OrganisationId });

            if (!userEntryResult.IsSuccess)
            {
            }

            // 4. синхронизация добавленных пользователей
            await _syncUtil.AddUserToOrganisation(userId,createResult.OrganisationId);

            // 5. присвоение роли пользователю в организации
            var organisationService = DependencyResolver.Current.GetService<IOrganisationService>();
            var role = await organisationService.GetOrganisationUserRoleTypeByTitle("SuperAdmin");

            var createUserRoleCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserRoleArgs, CreateUserRoleResult>>();
            var createUserRoleCommandResult = await createUserRoleCommand.Execute(new CreateUserRoleArgs { OrganisationUserEntryId = userEntryResult.CreatedUserEntryId, UserRoleTypeId = role.Id });

            if (!createUserRoleCommandResult.IsSuccess)
            {
            }
            result = JsonConvert.SerializeObject(createResult);
            return Json(result);
        }

        [HttpGet, AjaxOnly] // получить пользователей организации
        public async Task<ActionResult> GetOrganisationUserEntries(int organisationId)
        {
            var userEntries = await _organisationService.GetUserEntries(organisationId);
            // TODO : сделать маппинг нужных полей в DTO - сделалЪ
            var userEntriesDto = userEntries.Select(x => new
            {
                Id = x.Id,
                UserId = x.UserId,
                ProfileId = x.ProfileId
            }).ToList();
            var result = JsonConvert.SerializeObject(userEntriesDto);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        class OrgDto
        {
            public int id { get; set; }
            public string name { get; set; }
            public double balanceValue { get; set; }
        }
        [HttpGet, AjaxOnly] //получить доступные организации
        //public async Task<ActionResult> GetAvailable(int userId)
        public async Task<ActionResult> GetAvailable()
        {
            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);

            var availableOrgs = await _userService.GetAvailableOrganisations(userId);

            var availableOrgsDto = availableOrgs.Select(x => new
            {
                id = x.Id,
                name = x.Name,
                balanceValue = x.Balance.Value,
                isSelected = true,
                profile=1
                //profile = x.OrganisationUserEntry.Select(y => new { Id = y.ProfileId })
            }).ToList();
            //profile = x.Users.Select(y => new { Id=y.ProfileId }).ToList()
            // TODO : сделать маппинг нужных полей в DTO
            var result = JsonConvert.SerializeObject(availableOrgsDto);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region Invites
        [HttpGet, AjaxOnly] // получить список приглашенных юзеров
        public async Task<ActionResult> GetInvitedUsers(int organisationId)
        {
            var invitedUsers = await _organisationService.GetInvitedUsers(organisationId);

            var invitedUsersDto = invitedUsers.Select(x =>
            {
                var dispName = x.IsAccepted == true ? x.OrganisationUserEntry.UserProfile.DisplayName : $"{x.Email} (профиль не заполнен)";
                var statusText = x.IsAccepted == true ? "Зарегистрирован" : "Приглашен";
                return new { id = x.Id, userId = x.UserId, inviteDate = x.InviteSentDate.ToString("dd.MM.yyyy"), displayName = dispName, status = x.IsAccepted };
            });
            // TODO : сделать маппинг нужных полей в DTO
            var result = JsonConvert.SerializeObject(invitedUsersDto);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, AjaxOnly] // удалить юзера. IsDeleted = true
        public async Task<ActionResult> DeleteInvitedUser(Guid id)
        {
            // команда
            var deleteUserInviteCommand = DependencyResolver.Current.GetService<IBusinessCommand<DeleteUserInviteArgs, DeleteUserInviteResult>>();

            var commandResult = await deleteUserInviteCommand.Execute(new DeleteUserInviteArgs { Id = id });

            var result = JsonConvert.SerializeObject(commandResult);

            return Json(result);
        }

        [HttpPost, AjaxOnly] // пригласить юзеров
        public async Task<ActionResult> InviteUsers(int organisationId, string[] emails)
        {
            // валидация имейлов
            List<string> invalidEmails = new List<string>();
            List<string> validEmails = new List<string>();

            string emailPattern = @"^\w+([-.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
            Regex regex = new Regex(emailPattern, RegexOptions.IgnoreCase);

            for (int i = 0; i < emails.Length; i++)
            {
                var isValid = regex.IsMatch(emails[i]);

                if (isValid) { validEmails.Add(emails[i]); }
                else { invalidEmails.Add(emails[i]); }
            }

            var emailsInDb = await _organisationService.GetIdentityUsers(validEmails);
            Dictionary<string, int> dict = new Dictionary<string, int>();
            emailsInDb.ForEach(x => dict.Add(x.Email, x.Id));

            // добавление команд
            var inviteCommand = DependencyResolver.Current.GetService<IBusinessCommand<InviteUsersArgs, InviteUsersResult>>();
            var inviteCommandResult = await inviteCommand.Execute(new InviteUsersArgs
            {
                OrganisationId = organisationId,
                Emails = validEmails,
                ExistedEmails = dict,
                RegistrationUrl = Url.Action("Register", "Account", null, protocol: Request.Url.Scheme),
                AcceptInviteUrl = Url.Action("AcceptInvite", "Organisation", null, protocol: Request.Url.Scheme)
            });

            var resultDto = new
            {
                inviteResult = inviteCommandResult,
                invalidEmails = invalidEmails
            };

            // TODO: сделать на клиенте в ajax success response
            // добавление валидных в список инвайченых, а о инвалидных вывести уведомление
            var result = JsonConvert.SerializeObject(resultDto);
            return Json(result);
        }

        // названия входных параметров используются в текстовом виде в InviteUsersCommand - не менять
        public async Task<ActionResult> AcceptInvite(string inviteCode, int identityId)
        {
            var logger = DependencyResolver.Current.GetService<ILoggerUtility>();

            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);


            var invite = await _userService.GetUserInviteByInviteCode(inviteCode);

            //Этот инвайт уже принят или его не существует
            if (invite == null || invite.IsAccepted)
            {
                logger.Debug("invite wasn't found or already accepted");

                return View("Error");
            }
            try
            {


                // 1. подтверждение инвайта
                var acceptCommand = DependencyResolver.Current.GetService<IBusinessCommand<AcceptUserInviteArgs, AcceptUserInviteResult>>();
                var acceptInviteResult = await acceptCommand.Execute(new AcceptUserInviteArgs { InviteCode = inviteCode, IdentityId = identityId });

                if (!acceptInviteResult.IsSuccess)
                {
                    logger.Debug($"AcceptUserInvite Command failed - {acceptInviteResult.ErrorCode} - {acceptInviteResult.ErrorMessage}");
                    return View("Error");
                }

                // 2. создание записи о пользователе в организации
                var createUserEntryCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserEntryArgs, CreateUserEntryResult>>();
                var createUserEntryResult = await createUserEntryCommand.Execute(new CreateUserEntryArgs { IdentityId = identityId, OrganisationId = acceptInviteResult.UserInviteEntity.OrganisationId });

                if (!createUserEntryResult.IsSuccess)
                {
                    logger.Debug($"createUserEntryFailed - {createUserEntryResult.ErrorCode} - {createUserEntryResult.ErrorMessage}");
                    return View("Error");
                }

                // 3: добавление пользователя в организацию на сторонних сервисах
                await _syncUtil.AddUserToOrganisation(userId, invite.OrganisationId);

                // TODO 4: назначение пользователя и записи о пользователе к инвайту
                var setUserAndUserEntryCommand = DependencyResolver.Current.GetService<IBusinessCommand<SetUserAndUserEntryToUserInviteArgs, SetUserAndUserEntryToUserInviteResult>>();
                var setUserAndUserEntryResult = await setUserAndUserEntryCommand.Execute(new SetUserAndUserEntryToUserInviteArgs { InviteId = acceptInviteResult.UserInviteEntity.Id, UserEntryId = createUserEntryResult.CreatedUserEntryId });

                if (!setUserAndUserEntryResult.IsSuccess)
                {
                    logger.Debug($"Set User And UserEntry to Invite - failed - {setUserAndUserEntryResult.ErrorCode} - {setUserAndUserEntryResult.ErrorMessage}");
                    return View("Error");
                }

                var organisationService = DependencyResolver.Current.GetService<IOrganisationService>();
                var role = await organisationService.GetOrganisationUserRoleTypeByTitle("Member");

                var createUserRoleCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserRoleArgs, CreateUserRoleResult>>();
                var createUserRoleCommandResult = await createUserRoleCommand.Execute(new CreateUserRoleArgs { OrganisationUserEntryId = setUserAndUserEntryResult.OrganisationUserEntryId, UserRoleTypeId = role.Id });

                if (!createUserRoleCommandResult.IsSuccess)
                {
                    logger.Debug($"create user role - failed - {createUserRoleCommandResult.ErrorCode} - {createUserRoleCommandResult.ErrorMessage}");

                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                logger.Debug("AcceptInvite Action was failed", ex);
                return View("Error");

            }
            return View();
        }

        #endregion

        #region OrganisationUserProfile
        [HttpGet, AjaxOnly]
        public async Task<ActionResult> GetOrganisationUserProfile(int organisationId)
        {
            var result = "";
            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);

            var organisationUserEntry = await _userService.GetUserEntryByIdentityId(organisationId, userId);


            if (organisationUserEntry == null || organisationUserEntry.UserProfile == null) { result = "error"; }

            result = JsonConvert.SerializeObject(organisationUserEntry.UserProfile);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost, AjaxOnly]
        //public async Task<ActionResult> EditOrganisationUserProfile(OrganisationUserProfile profile)
        //{
        //    var result = string.Empty;
        //    if (!ModelState.IsValid)
        //    {
        //        JsonConvert.SerializeObject("error");
        //        return Json(result);
        //    }

        //    var editCommand = DependencyResolver.Current.GetService<IBusinessCommand<EditOrganisationUserProfileArgs, EditOrganisationUserProfileResult>>();
        //    var editResult = editCommand.Execute(new EditOrganisationUserProfileArgs { Profile = profile });

        //    result = JsonConvert.SerializeObject(editResult);

        //    return Json(result);
        //}
        #endregion

        #region OrganisationProfile
        [HttpGet, AjaxOnly]
        public async Task<ActionResult> GetOrganisationDetails(int organisationId)
        {
            var result = "";

            var organisation = await _organisationService.GetOrganisation(organisationId);
            if (organisation == null || organisation.Details == null) { result = "error"; }

            var details = organisation.Details;

            result = JsonConvert.SerializeObject(organisation.Details);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, AjaxOnly]
        public async Task<ActionResult> EditOrganisationDetails(int id, OrganisationDetailCard model)
        {
            var result = "";

            // команда для редактирования карточки организации
            var editDetailsCommand = DependencyResolver.Current.GetService<IBusinessCommand<EditOrganisationDetailsArgs, EditOrganisationDetailsResult>>();
            var editResult = editDetailsCommand.Execute(new EditOrganisationDetailsArgs { Id = id, DetailCard = model });

            result = JsonConvert.SerializeObject(editResult);

            return Json(result);
        }

        #endregion


        [HttpGet, AjaxOnly] // переход на яндекс кассу с редиректом
        public async Task<ActionResult> AddBalance()
        {

            return Redirect("");
        }

        [HttpPost] // запрос от яндекса при успешной оплате
        public async Task<ActionResult> AddBalanceSuccess()
        {
            return null;
        }
    }
}