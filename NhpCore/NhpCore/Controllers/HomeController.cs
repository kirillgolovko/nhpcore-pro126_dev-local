﻿using Common.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NhpCore.Attributes;
using NhpCore.Common;
using NhpCore.CoreLayer.Entities;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NhpCore.Controllers
{
    [AllowAnonymous]
    public class HomeController : BaseController
    {
        private ApplicationSignInManager _appSignInManager;
        private readonly ILoggerUtility _logger;

        public HomeController(ApplicationSignInManager appSignInMngr)
        {
            if (appSignInMngr == null) throw new ArgumentNullException(nameof(appSignInMngr));

            _appSignInManager = appSignInMngr;
        }
        public HomeController(ILoggerUtility logger, ApplicationSignInManager appSignInMngr)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            if (appSignInMngr == null) throw new ArgumentNullException(nameof(appSignInMngr));

            _appSignInManager = appSignInMngr;
            _logger = logger;
        }
        // GET: Home
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Dashboard");
            }

            return View();
        }

        [Auth]
        public ActionResult Test()
        {
            return View();
        }

        [Auth, HttpGet]
        public async Task<ActionResult> TestApi()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> TestApi(int? id = null)
        {
            var token = (User as ClaimsPrincipal).FindFirst("access_token").Value;

            var client = new HttpClient();
            client.SetBearerToken(token);

            var i = id ?? 1;
            var result = await client.GetStringAsync($"http://nhpserverapi.azurewebsites.net/api/values/{i}");
            var res = JValue.Parse(result).ToString(Formatting.Indented);

            return View(new TestApi { Result = res });
        }



        public PartialViewResult Header()
        {
            var model = new object();
            return PartialView(this.GetPartialPath(NhpConstants.Partials.Header), model);
        }

        public PartialViewResult NavigationMenu()
        {
            var model = new object();

            return PartialView(this.GetPartialPath(NhpConstants.Partials.NavigationMenu), model);
        }
    }
}