﻿using Common.Commands;
using Common.Logging;
using Newtonsoft.Json;
using NhpCore.Business.Commands;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NhpCore.Controllers
{
    public class StorageController : Controller
    {
        private readonly ILoggerUtility _logger;

        public StorageController(ILoggerUtility logger)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));

            _logger = logger;

        }

        public async Task GetYandexToken()
        {

            Redirect("http://oauth.yandex.ru/authorize?response_type=code&client_id&state");
        }

        class yandexResponseDto
        {
            public string access_token { get; set; }
            public string refresh_token { get; set; }
            public string token_type { get; set; }
            public string expires_in { get; set; }

            public string error { get; set; }
            public string error_description { get; set; }
        }
        class YandexState
        {
            Tuple<int, int> tuple;

            public int UserId { get { return tuple.Item1; } }
            public int OrganisationId { get { return tuple.Item2; } }


            public YandexState(string state)
            {
                var splitted = state.Split('o');
                var userId = int.Parse(splitted[0]);
                var orgId = int.Parse(splitted[1]);
                tuple = new Tuple<int, int>(userId, orgId);
            }
        }

        YandexState _yandexState { get; set; }

        public async Task<ActionResult> YandexCallback(string code, string error, string error_description, string state)
        {
            _yandexState = new YandexState(state);

            if (code != null)
            {
                try
                {

                    var clientId = ConfigurationManager.AppSettings["yandexClientId"];
                    var clientPassword = ConfigurationManager.AppSettings["yandexClientPassword"];

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("https://oauth.yandex.ru");
                        var content = new FormUrlEncodedContent(new[]
                        {
                    new KeyValuePair<string, string>("grant_type", "authorization_code"),
                    new KeyValuePair<string, string>("code", code),
                    new KeyValuePair<string, string>("client_id", clientId),
                    new KeyValuePair<string, string>("client_secret", clientPassword)
                });

                        var tokenResponse = await client.PostAsync("/token", content);
                        string resultContent = await tokenResponse.Content.ReadAsStringAsync();

                        var response = JsonConvert.DeserializeObject<yandexResponseDto>(resultContent);

                        if (response.error != null)
                        {
                            _logger.Error($"Ошибка ({response.error}) при получении токена. ({response.error_description})");
                            return View("Error");
                        }

                        _logger.Trace($"Токен хранилища успешно выдан организации ({_yandexState.OrganisationId})");

                        var addStorageCredentialsCommand = DependencyResolver.Current.GetService<IBusinessCommand<AddStorageCredentialsArgs, AddStorageCredentialsResult>>();

                        var addStorageResult = await addStorageCredentialsCommand.Execute(new AddStorageCredentialsArgs
                        {
                            AccessType = CoreLayer.Entities.AccessType.Token,
                            Token = response.access_token,
                            OrganisationId = _yandexState.OrganisationId,
                            StorageType = CoreLayer.Entities.StorageType.YandexDisk,
                            UserId = _yandexState.UserId,
                            RefreshToken = response.refresh_token,
                            ExpiresIn = response.expires_in,
                        });

                        if (addStorageResult.IsSuccess)
                        {
                            _logger.Trace($"Токен хранилища успешно сохранен в базе для организации ({_yandexState.OrganisationId})");
                            return Redirect(Url.Action("index","dashboard"));
                        }
                        else
                        {
                            _logger.Error($"Ошибка в процессе сохранения токена для организации ({_yandexState.OrganisationId})");
                        }
                    }
                }
                catch (Exception ex)
                {

                    _logger.Fatal("Фатальная ошбика в процессе обработки токена", ex);
                }
            }
            else if (error != null)
            {
                _logger.Error($"У организации {_yandexState.OrganisationId} возникла ошибка ({error}) в процессе выдачи кода для получения токена. ({error_description})");
            }

            return View("Error");

        }
    }
}