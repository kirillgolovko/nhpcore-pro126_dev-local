﻿using Common.Commands;
using Common.Logging;
using Common.Mapping;
using MoreLinq;
using Newtonsoft.Json;
using NhpCore.Attributes;
using NhpCore.Business.Commands;
using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Services;
using NhpCore.Infrastructure.Utils.ServiceSynchronization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NhpCore.Controllers
{
    public class DashboardController : BaseController
    {

        private readonly IUserService _userService;

        public DashboardController(IUserService userService)
        {
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            _userService = userService;
        }

        // GET: Dashboard
        public ActionResult Index()
        {
            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);

            ViewBag.UserId = userId;
            return View();
        }

        [HttpGet, AjaxOnly] //получить доступные профили
        public async Task<ActionResult> GetAvailableProfiles()
        {
            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);

            var availableProfiles = await _userService.GetAvailableProfiles(userId);

            var availableProfDto = availableProfiles.Select(x => new
            {
                Id = x.Id,
                UserId = x.UserId,
                DisplayName = x.DisplayName,
                FirstName = x.FirstName,
                SecondName = x.SecondName,
                PatronymicName = x.PatronymicName,
                BirthYear = x.BirthYear.ToString("yyyy-MM-dd"),
                BirthDate = x.BirthDate.ToString("yyyy-MM-dd"),
                AboutMyself = x.AboutMyself,
                City = x.City,
                Address = x.Address,
                Gender = x.Gender,
                PassportSeries = x.PassportSeries,
                PassportNumber = x.PassportNumber,
                PassportSubdivisionCode = x.PassportSubdivisionCode,
                PassportWhoAndWhenGave = x.PassportWhoAndWhenGave,
                PassportRegistrationAddress = x.PassportRegistrationAddress,
                INN = x.INN,
                Salary = x.Salary,
                JobTitle = x.JobTitle,
                SNILS = x.SNILS,
                MobilePhone1 = x.MobilePhone1,
                MobilePhone2 = x.MobilePhone2,
                BusinessPhone = x.BusinessPhone,
                Email = x.Email,
                Skype = x.Skype,
                Telegram = x.Telegram,
                IsTelegram = x.IsTelegram,
                IsEmail = x.IsEmail,
                ICQ = x.ICQ,
                Facebook = x.Facebook,
                LinkedIn = x.LinkedIn,
                VK = x.VK,
                Skills = x.Skills,
                Interests = x.Interests,
                SNP = $"{x.SecondName} {x.FirstName} {x.PatronymicName}",
            }).ToList();

            var resultDto = JsonConvert.SerializeObject(availableProfDto);

            return Json(resultDto, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, AjaxOnly] //Создать пустой профиль юзеру
        public async Task<ActionResult> CreateProfile(UserProfile _profile)
        {
            var result = string.Empty;

            var subClaim = ((ClaimsIdentity)this.User.Identity).FindFirst("sub");
            if (subClaim == null) { throw new ArgumentNullException(nameof(subClaim)); }

            var userId = int.Parse(subClaim.Value);

            var dt1900 = DateTime.Parse("01.01.1900");
            var dtNow = DateTime.Now;
            _profile.UserId = userId;
            _profile.BirthDate = _profile.BirthDate <= dt1900 ? dtNow : _profile.BirthDate;
            _profile.BirthYear = _profile.BirthDate.Date;

            var createUserProfileCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserProfileArgs, CreateUserProfileResult>>();
            var createUserProfileCommandResult = await createUserProfileCommand.Execute(new CreateUserProfileArgs { Profile = _profile });

            result = JsonConvert.SerializeObject(createUserProfileCommandResult);

            return Json(result);
        }

        [HttpPost, AjaxOnly] //Изменить профиль юзеру
        public async Task<ActionResult> EditProfile(UserProfile _profile)
        {
            var result = string.Empty;
            _profile.BirthYear = _profile.BirthDate.Date;
            var editUserProfileCommand = DependencyResolver.Current.GetService<IBusinessCommand<EditUserProfileArgs, EditUserProfileResult>>();
            var editUserProfileCommandResult = await editUserProfileCommand.Execute(new EditUserProfileArgs { Profile = _profile });

            result = JsonConvert.SerializeObject(editUserProfileCommandResult);

            return Json(result);
        }

        [HttpPost, AjaxOnly] //Удалить профиль юзеру
        public async Task<ActionResult> DeleteProfile(UserProfile _profile)
        {
            var result = string.Empty;
            var deleteUserProfileCommand = DependencyResolver.Current.GetService<IBusinessCommand<DeleteUserProfileArgs, EmptyBusinessCommandResult>>();
            var deleteUserProfileCommandResult = await deleteUserProfileCommand.Execute(new DeleteUserProfileArgs { Id = _profile.Id });

            result = JsonConvert.SerializeObject(deleteUserProfileCommandResult);

            return Json(result);
        }
    }
}