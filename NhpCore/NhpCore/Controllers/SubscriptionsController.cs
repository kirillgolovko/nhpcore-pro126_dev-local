﻿using Common.Mapping;
using Newtonsoft.Json;
using NhpCore.Attributes;
using NhpCore.CoreLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NhpCore.Controllers
{
    public class SubscriptionsController : BaseController
    {
        private readonly ISubscriptionService _subscriptionService;
        private readonly IOrganisationService _organisationService;
        private readonly IMapperUtility _mapper;

        public SubscriptionsController(ISubscriptionService subscriptionService, IOrganisationService organisationService, IMapperUtility mapper)
        {
            if (subscriptionService == null) { throw new ArgumentNullException(nameof(subscriptionService)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }
            if (mapper == null) { throw new ArgumentNullException(nameof(mapper)); }


            _subscriptionService = subscriptionService;
            _organisationService = organisationService;
            _mapper = mapper;
        }

        [HttpGet, AjaxOnly]// подписки которые будут отображаться в комбо-боксе
        public async Task<ActionResult> GetAvailableSubscriptions()
        {
            var availableSubscriptions = await _subscriptionService.GetAvailable();

            var dto = availableSubscriptions.Select(x => new { id = x.Id, name = x.Name, price = x.Price }).ToList();

            var result = JsonConvert.SerializeObject(dto);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary> получение юзеров с имеющимися у них подписками для списка с выбором юзеров при покупке </summary>
        [HttpGet, AjaxOnly]
        public async Task<ActionResult> GetUsersInfo(int organisationId, int subscriptionId)
        {
            //1. выбор юзеров
            var users = await _organisationService.GetUserEntries(organisationId);
            //2. прикрепить к ним информацию о уже купленных подписках
            var usersIds = users.Select(x => x.UserId).ToList();

            var subscriptionInfo = await _subscriptionService.GetUserEntries(subscriptionId, usersIds);
            
            // TODO: в идеале - нужно проверять наличие более расширеных подписок у пользователя.
            var subscriptionInfoDto = users.Select(x =>
            {
                var userSubscription = subscriptionInfo.Where(u => x.UserId == u.UserId).OrderBy(u => u.ExpirationDate).FirstOrDefault();


                var expirationDate = userSubscription == null ? // проверяем, есть ли подписка
                    string.Empty :// подписки нет - string.empty
                    userSubscription.ExpirationDate.HasValue == true ? // подписка есть - проверка наличия значения ExpirationDate
                            userSubscription.ExpirationDate.Value.ToString("dd.MM.yyyy") // значение есть
                            : string.Empty; // значения нет - string.empty

                string name = name = x.UserProfile.DisplayName ?? x.UserProfile.FirstName; ;

                return new
                {
                    userOrgEntryId = x.Id,
                    userId = x.UserId,
                    expirationDate = expirationDate,
                    name = name
                };
            }).ToList();

            var result = JsonConvert.SerializeObject(subscriptionInfoDto);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, AjaxOnly] // действие происходит при нажатии на кнопку "оплатить"
        public async Task<ActionResult> Pay(int organisationId, int subscriptionId, int duration, int[] users)
        {
            return RedirectToAction("Index", "Home");
        }

        [HttpPost, AllowAnonymous] // обработка редиректа от платежной системы
        public async Task<ActionResult> SuccessfulPaid()
        {
            return null;
        }
    }
}