﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NhpCore.Controllers
{
    public class ClientController : BaseController
    {
        public async Task<ActionResult> GetAvailableOrganisations(int userId)
        {


            return Json("", JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetHighestSubscription(int userId, string client)
        {


            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}