﻿using Common.Commands;
using Common.Logging;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using NhpCore.Attributes;
using NhpCore.Business.Commands;
using NhpCore.Business.Errors;
using NhpCore.Common;
using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Services;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static NhpCore.Common.NhpConstants;
using Dapper;
using SyncDto;
using NhpCore.Infrastructure.Utils.ServiceSynchronization;
using NhpCore.Infrastructure.Utils.MessageSender;

namespace NhpCore.Controllers
{
    class CreatePersonalOrganisationInfo
    {
        public int CreatedOrganisationId { get; set; }
        public Guid CreatedUserEntryId { get; set; }
    }

    public class AccountController : BaseController
    {
        #region Fields & Properties

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public CustomUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<CustomUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationSignInManager _signInManager;
        private CustomUserManager _userManager;

        #endregion Fields & Properties

        #region customFields
        private readonly IServiceSynchronization _syncUtil;
        private readonly IMessageSender _messageSender;
        private readonly ILoggerUtility _logger;
        #endregion

        #region ctor

        public AccountController(IServiceSynchronization syncUtil, IMessageSender messageSender, ILoggerUtility logger)
        {
            if (syncUtil == null) throw new ArgumentNullException(nameof(syncUtil));
            if (messageSender == null) throw new ArgumentNullException(nameof(messageSender));
            if (logger == null) throw new ArgumentNullException(nameof(logger));

            _syncUtil = syncUtil;
            _messageSender = messageSender;
            _logger = logger;
        }

        #endregion ctor

        #region Custom NHP Behaviour

        /// <summary> Ajax для проверки наличия email в базе</summary>   
        [AjaxOnly]
        public async Task<ActionResult> CheckEmailForUnique(string email)
        {
            bool isUnique = false;
            var str = $@"""value"":""{isUnique}""";
            return Json(str);
        }

        /// <summary>
        /// При подтверждении регистрации, если регистрировалась организация - редиректили сюда,
        /// для заполнения информации о организации
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> FillOrganisationDetails(FillOrganisationDetailsModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Error");
            }
            var userId = model.UserId;
            //–>–>–>–>–>–> Комманда вставки компании
            var createOrganisationCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateOrganisationArgs, CreateOrganisationResult>>();
            var args = new CreateOrganisationArgs { CreatorId = model.UserId, ShortName = model.ShortName };

            var createOrgResult = await createOrganisationCommand.Execute(args);

            return View("ConfirmEmail");
        }

        /// <summary>отсюда мы уходим на редирект для аутентификации</summary>
        public void Login()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                RedirectToAction("Index", "Dashboard");
            }
        }

        /// <summary>
        /// Действия для колбека от IdSrv при успешной аутентификации 
        /// </summary>
        public async Task<ActionResult> SuccessfulLogin()
        {
            // редиректимся отсюда в личный кабинет
            return RedirectToAction("Index", "Home");
        }

        public ActionResult SignOut()
        {
            Request.GetOwinContext().Authentication.SignOut();
            return Redirect("/");
        }
        #endregion Custom NHP Behaviour

        #region Default Identity Behaviour


        ////
        //// GET: /Account/Login
        //[AllowAnonymous]
        //public ActionResult Login(string returnUrl)
        //{
        //    ViewBag.ReturnUrl = returnUrl;
        //    return View();
        //}

        ////
        //// POST: /Account/Login
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }

        //    // Сбои при входе не приводят к блокированию учетной записи
        //    // Чтобы ошибки при вводе пароля инициировали блокирование учетной записи, замените на shouldLockout: true
        //    var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
        //    switch (result)
        //    {
        //        case SignInStatus.Success:
        //            return RedirectToLocal(returnUrl);

        //        case SignInStatus.LockedOut:
        //            return View("Lockout");

        //        case SignInStatus.RequiresVerification:
        //            return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });

        //        case SignInStatus.Failure:
        //        default:
        //            ModelState.AddModelError("", "Неудачная попытка входа.");
        //            return View(model);
        //    }
        //}

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Требовать предварительный вход пользователя с помощью имени пользователя и пароля или внешнего имени входа
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // Приведенный ниже код защищает от атак методом подбора, направленных на двухфакторные коды.
            // Если пользователь введет неправильные коды за указанное время, его учетная запись
            // будет заблокирована на заданный период.
            // Параметры блокирования учетных записей можно настроить в IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Неправильный код.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public async Task<ActionResult> Register(int organisationId = -1, string inviteCode = "")
        {
            if (inviteCode != "")
            {
                // TODO: Сделать валидацию инвайта, если инвайт отменен, то уведомить пользователя.
                var userService = DependencyResolver.Current.GetService<IUserService>();
                var invite = await userService.GetUserInviteByInviteCode(inviteCode);

                //Этот инвайт уже принят или его не существует
                if (invite == null || invite.IsAccepted)
                {
                    return View("Error");
                }
                return View(new RegisterViewModel { Email = invite.Email, InviteCode = inviteCode });
            }

            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new CustomUser { UserName = model.Email, Email = model.Email };

                _logger.Debug("start identity user creating");
                var result = await UserManager.CreateAsync(user, model.Password);

                _logger.Debug($"creating identity user - {result.Succeeded}");

                if (result.Succeeded)
                {
                    _logger.Debug($"identity User created succesfully, id:{user.Id}");

                    _logger.Debug("start identity user claim creating");
                    var addClaimResult = await UserManager.AddClaimAsync(user.Id, new Claim(NhpClaimTypes.DisplayName, model.DisplayName));
                    _logger.Debug($"creating identity user claim status - {result.Succeeded}");

                    if (addClaimResult.Succeeded)
                    {
                        _logger.Debug($"identity User name claim added succesfully, name: {model.DisplayName}");

                        // –>–>–>–>–> Команда вставки пользователя в Кор проект
                        var createUserCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserArgs, CreateUserResult>>();
                        //var createUserCommandArgs = new CreateUserArgs { IdentityId = user.Id };
                        var createUserCommandArgs = new CreateUserArgs { Id = user.Id };

                        var createUserCommandResult = await createUserCommand.Execute(createUserCommandArgs);
                        _logger.Debug($"CoreUser created succesfully, id: {createUserCommandResult.CreatedUserId}");

                        await _syncUtil.AddUser(user.Id, user.Email, model.DisplayName);
                        // CreateUserEntriesInServices(user);

                        if (createUserCommandResult.IsSuccess)
                        {
                            var createPersonalResult = await CreatePersonalOrganisation(createUserCommandResult);

                            if (createPersonalResult.Item1.IsSuccess)
                            {
                                _logger.Debug($"Personal Organisation created successfully");

                                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                                // Дополнительные сведения о том, как включить подтверждение учетной записи и сброс пароля,
                                // см. по адресу: http://go.microsoft.com/fwlink/?LinkID=320771
                                // Отправка сообщения электронной почты с этой ссылкой
                                string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);

                                var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code, regType = model.RegistrationType, inviteCode = model.InviteCode }, protocol: Request.Url.Scheme);
#if !DEBUG
                                await SendInformationAboutUserToInfoMail(model, Request);
                                await UserManager.SendEmailAsync(user.Id, "Подтверждение учетной записи", "Подтвердите вашу учетную запись, щелкнув <a href=" + callbackUrl + ">здесь</a>");
#endif
                                _logger.Debug($"confirmation email sended");

                                _logger.Debug($"Pre-return View");

                                return View("SuccessfullRegistration");

                            }
                            else
                            {
                                _logger.Debug($"error while creating personal organisation;");

                                // rollback в зависимости от этапа на котором произошла ошибка.
                                if (createPersonalResult.Item2.CreatedOrganisationId == 0 && createPersonalResult.Item2.CreatedUserEntryId == Guid.Empty)
                                {
                                    _logger.Debug($"start rollback type 1");

                                    await RollbackRegistrationFirstStage(createUserCommandResult.CreatedUserId, user);
                                }
                                if (createPersonalResult.Item2.CreatedOrganisationId != 0 && createPersonalResult.Item2.CreatedUserEntryId == Guid.Empty)
                                {
                                    _logger.Debug($"start rollback type 2");

                                    var firstStageTask = RollbackRegistrationFirstStage(createUserCommandResult.CreatedUserId, user);
                                    var secondStageTask = RollbackRegistrationSecondStage(createPersonalResult.Item2.CreatedOrganisationId);
                                    await Task.WhenAll(firstStageTask, secondStageTask);
                                }
                                if (createPersonalResult.Item2.CreatedOrganisationId != 0 && createPersonalResult.Item2.CreatedUserEntryId != Guid.Empty)
                                {
                                    _logger.Debug($"start rollback type 3");

                                    var firstStageTask = RollbackRegistrationFirstStage(createUserCommandResult.CreatedUserId, user);
                                    var secondStageTask = RollbackRegistrationSecondStage(createPersonalResult.Item2.CreatedOrganisationId);
                                    var thirdStageTask = RollbackRegistrationThirdStage(createPersonalResult.Item2.CreatedUserEntryId);
                                    await Task.WhenAll(firstStageTask, secondStageTask, thirdStageTask);
                                }

                                return View("Error");
                            }
                        }
                        else
                        {
                            //var userInBase = await UserManager.FindByEmailAsync(model.Email);
                            var delRes = await UserManager.DeleteAsync(user);

                            if (delRes.Succeeded)
                            {
                                AddErrors(delRes);
#warning // TODO: решить какую вьюшку возвращать и ее содержание, при неудачной регистрации.
                                return View("Error");
                            }
                        }
                    }
                    else
                    {

                        var delUser = await UserManager.DeleteAsync(user);
                        AddErrors(addClaimResult);
                        return View("Error");
                    }
                }
                else
                {
                    AddErrors(result);
                }

            }

            // Появление этого сообщения означает наличие ошибки; повторное отображение формы
            return View(model);
        }

        private async Task SendInformationAboutUserToInfoMail(RegisterViewModel model, HttpRequestBase request)
        {

            var messageBody = $@"Новый пользователь: 

Email:{model.Email}
Показываемое имя: {model.DisplayName}
Браузер пользователя: {request.Browser}
Юзер агент: {request.UserAgent}
Языки: {request.UserLanguages}";

            var message = new Message("info@nhp-soft.ru", "Новый пользователь online.nhp-soft.ru", messageBody);
            await _messageSender.SendAsync(message);
        }

        private async Task CreateUserEntriesInServices(CustomUser user)
        {
            var list = new List<Task>();
            GetListOfTasksForCreatingUserEntries(ref list, user);

            await Task.WhenAll(list);
        }

        private void GetListOfTasksForCreatingUserEntries(ref List<Task> list, CustomUser user)
        {
            var toProjectDb = Task.Run(() => AddUserToService(user, "data source=nhp-soft.database.windows.net;initial catalog=NHPProjectDebugDbModified;persist security info=True;user id=autumninthecity240490;password=Helgard240490;MultipleActiveResultSets=True;App=EntityFramework"));

            list.Add(toProjectDb);
        }

        private async void AddUserToService(CustomUser user, string connectionString)
        {
            //проверить есть ли пользователь
            // если есть - уведомить о наличии, если нет - добавить
            IDbConnection db = new SqlConnection(connectionString);
            var gettedUser = db.Query<GetUserSync>("SELECT UserID, Email FROM NHP_Users where UserId=@id", new { id = user.Id }).FirstOrDefault();

            if (gettedUser == null)
            {
                try
                {
                    var query = @"INSERT INTO NHP_Users (
UserID,
Email,
Password,
IsDepartmentChief,
AccessRoleId,
IsMailSended,
IsDeleted,
IsBlocked,
CoreId,
IdentityId) values (@Id, @Email, @PasswordHash, 0, 5, 1, 0, 0, @Id, @Id)";

                    await db.ExecuteAsync(query, user);
                    db.Dispose();
                }
                catch (Exception)
                {
                    // TODO: отправить имейл о возникшей ошибке
                    throw new Exception();
                }
            }
        }


        private async Task<Tuple<CreateUserResult, CreatePersonalOrganisationInfo>> CreatePersonalOrganisation(CreateUserResult createUserResult)
        {
            var logger = DependencyResolver.Current.GetService<ILoggerUtility>();

            logger.Debug($"started personal organisation creating");

            Tuple<CreateUserResult, CreatePersonalOrganisationInfo> result = null;
            // 1. создание организации
            var createOrganisationCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateOrganisationArgs, CreateOrganisationResult>>();
            var createOrganisationCommandResult = await createOrganisationCommand.Execute(new CreateOrganisationArgs { CreatorId = createUserResult.IdentityId, ShortName = $"Личная-{createUserResult.CreatedUserId}" });

            if (!createOrganisationCommandResult.IsSuccess)
            {
                logger.Debug($"personal organisation didn't create");

                createUserResult.AddError(BusinessErrors.Organisation.CREATING_ERROR, BusinessErrors.Organisation.CREATING_ERROR_CODE);
                return result = new Tuple<CreateUserResult, CreatePersonalOrganisationInfo>(createUserResult, new CreatePersonalOrganisationInfo());
            }
            // TODO: 2. добавление созданной организации в сервисы
            await _syncUtil.AddOrganisation(createOrganisationCommandResult.OrganisationId, createOrganisationCommandResult.Name, createOrganisationCommandResult.RegistrationDate);
            //CreateOrganisationEntriesInServices(createOrganisationCommandResult);

            // 3. создание записи о пользователе в этой организации
            var createUserEntryCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserEntryArgs, CreateUserEntryResult>>();
            var userEntryResult = await createUserEntryCommand.Execute(new CreateUserEntryArgs { IdentityId = createUserResult.IdentityId, IsPersonal = true, OrganisationId = createOrganisationCommandResult.OrganisationId });

            if (!userEntryResult.IsSuccess)
            {
                logger.Debug($"personal organisation user entry didn't create");

                createUserResult.AddError(BusinessErrors.Organisation.ORGANISATION_USER_ENTRY_CREATING_ERROR, BusinessErrors.Organisation.ORGANISATION_USER_ENTRY_CREATING_ERROR_CODE);

                return result = new Tuple<CreateUserResult, CreatePersonalOrganisationInfo>(createUserResult, new CreatePersonalOrganisationInfo { CreatedOrganisationId = createOrganisationCommandResult.OrganisationId });
            }
            // TODO: 4. добавление пользователей в организации
            await _syncUtil.AddUserToOrganisation(createUserResult.IdentityId, createOrganisationCommandResult.OrganisationId);
            // CreateUserInOrganisationtryInServices(createOrganisationCommandResult, createUserResult);

            // 5. присвоение роли пользователю в организации
            var organisationService = DependencyResolver.Current.GetService<IOrganisationService>();
            var role = await organisationService.GetOrganisationUserRoleTypeByTitle("SuperAdmin");

            var createUserRoleCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserRoleArgs, CreateUserRoleResult>>();
            var createUserRoleCommandResult = await createUserRoleCommand.Execute(new CreateUserRoleArgs { OrganisationUserEntryId = userEntryResult.CreatedUserEntryId, UserRoleTypeId = role.Id });

            if (!createUserRoleCommandResult.IsSuccess)
            {
                logger.Debug($"personal organisation user role didn't create");

                createUserResult.AddError(BusinessErrors.Organisation.ORGANISATION_USERENTRY_ROLE_CREATING_ERROR, BusinessErrors.Organisation.ORGANISATION_USERENTRY_ROLE_CREATING_ERROR_CODE);
                return result = new Tuple<CreateUserResult, CreatePersonalOrganisationInfo>(createUserResult, new CreatePersonalOrganisationInfo { CreatedOrganisationId = createOrganisationCommandResult.OrganisationId, CreatedUserEntryId = userEntryResult.CreatedUserEntryId });
            }



            return result = new Tuple<CreateUserResult, CreatePersonalOrganisationInfo>(createUserResult, new CreatePersonalOrganisationInfo());
        }

        private async void CreateUserInOrganisationtryInServices(CreateOrganisationResult createOrganisationCommandResult, CreateUserResult createUserResult)
        {
            var list = new List<Task>();
            GetListOfTasksForCreatingUserInOrganisation(ref list, createOrganisationCommandResult, createUserResult);
            await Task.WhenAll(list);

        }

        private void GetListOfTasksForCreatingUserInOrganisation(ref List<Task> list, CreateOrganisationResult createOrganisationCommandResult, CreateUserResult createUserResult)
        {
            var projectService = Task.Run(() => CreateUserInOrganisation(createOrganisationCommandResult, createUserResult, ""));
        }

        private async void CreateUserInOrganisation(CreateOrganisationResult createOrganisationCommandResult, CreateUserResult createUserResult, string connectionString)
        {
            //проверить есть ли пользователь
            // если есть - уведомить о наличии, если нет - добавить
            IDbConnection db = new SqlConnection(connectionString);
            var gettedUser = db.Query<GetOrganisationUserSync>("SELECT OrganisationId, UserId FROM OrganisationUsers where OrganisationId=@orgId and UserId=@uId", new { orgId = createOrganisationCommandResult.OrganisationId, uId = createUserResult.IdentityId }).FirstOrDefault();

            if (gettedUser == null)
            {
                try
                {
                    var query = @"INSERT INTO OrganisationUsers (
OrganisationId,
UserId,
) values (@OrganisationId, @UserId)";

                    await db.ExecuteAsync(query, new { OrganisationId = createOrganisationCommandResult.OrganisationId, UserId = createUserResult.IdentityId });
                    db.Dispose();
                }
                catch (Exception)
                {
                    // TODO: отправить имейл о возникшей ошибке
                    throw new Exception();
                }
            }

        }

        private async void CreateOrganisationEntriesInServices(CreateOrganisationResult createOrganisationCommandResult)
        {
            var list = new List<Task>();
            GetListOfTasksForCreatingOrganisationsEntries(ref list, createOrganisationCommandResult);
            await Task.WhenAll(list);
        }

        private void GetListOfTasksForCreatingOrganisationsEntries(ref List<Task> list, CreateOrganisationResult createOrganisationCommandResult)
        {
            var projectService = Task.Run(() => AddOrganisationToService(createOrganisationCommandResult, ""));

            list.Add(projectService);
        }

        private async void AddOrganisationToService(CreateOrganisationResult model, string connectionString)
        {
            //проверить есть ли пользователь
            // если есть - уведомить о наличии, если нет - добавить
            IDbConnection db = new SqlConnection(connectionString);
            var gettedUser = db.Query<GetUserSync>("SELECT Id, Name FROM NHP_Organisations where Id=@id", new { id = model.OrganisationId }).FirstOrDefault();

            if (gettedUser == null)
            {
                try
                {
                    var query = @"INSERT INTO NHP_Organisations (
Id,
Name,
RegisterDate,
CoreId,
IsDeleted) values (@OrganisationId, @Name, @RegistrationDate, @OrganisationId, 0)";

                    await db.ExecuteAsync(query, model);
                    db.Dispose();
                }
                catch (Exception)
                {
                    // TODO: отправить имейл о возникшей ошибке
                    throw new Exception();
                }
            }
        }

        #region registration fail data rollbace
        private async Task RollbackRegistrationFirstStage(int createdUserId, CustomUser user)
        {
            var deleteUserFromIdentityDbTask = UserManager.DeleteAsync(user);

            var removeUserCommand = DependencyResolver.Current.GetService<IBusinessCommand<DeleteUserArgs, EmptyBusinessCommandResult>>();
            var removeUserCommandTasks = removeUserCommand.Execute(new DeleteUserArgs { UserId = createdUserId });

            await Task.WhenAll(deleteUserFromIdentityDbTask, removeUserCommandTasks);
        }

        private async Task RollbackRegistrationSecondStage(int createdOrganisationId)
        {
            var removeOrganisation = DependencyResolver.Current.GetService<IBusinessCommand<DeleteOrganisationArgs, DeleteOrganisationResult>>();
            var removeOrganisationResult = await removeOrganisation.Execute(new DeleteOrganisationArgs { OrganisationId = createdOrganisationId });
        }

        private async Task RollbackRegistrationThirdStage(Guid createdUserEntryId)
        {
            var removeOrgUserEntry = DependencyResolver.Current.GetService<IBusinessCommand<DeleteUserEntryArgs, DeleteUserEntryResult>>();
            var removeOrgUserEntryResult = await removeOrgUserEntry.Execute(new DeleteUserEntryArgs { Id = createdUserEntryId });
        }
        #endregion

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(int userId, string code, string regType, string inviteCode = "")
        {
            if (userId <= 0 || code == "" || regType == "")
            {
                return View("Error");
            }

            AcceptUserInviteResult acceptInviteResult = null;
            if (inviteCode != "")
            {
                var acceptInviteCommand = DependencyResolver.Current.GetService<IBusinessCommand<AcceptUserInviteArgs, AcceptUserInviteResult>>();
                acceptInviteResult = await acceptInviteCommand.Execute(new AcceptUserInviteArgs { IdentityId = userId, InviteCode = inviteCode });

                if (!acceptInviteResult.IsSuccess)
                {
                    return View("Error");
                }
            }

            var result = await UserManager.ConfirmEmailAsync(userId, code);

            if (result.Succeeded)
            {
                if (inviteCode != "")
                {// нужно создать OrganisationUserEntry в организации к которой добавляется пользователь
                    var createUserEntryCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserEntryArgs, CreateUserEntryResult>>();
                    var createUserEntryResult = await createUserEntryCommand.Execute(new CreateUserEntryArgs { IdentityId = userId, OrganisationId = acceptInviteResult.UserInviteEntity.OrganisationId });

                    if (!createUserEntryResult.IsSuccess)
                    {
                        return View("Error");
                    }

                    // 3: добавление пользователя в организацию на сторонних сервисах
                    await _syncUtil.AddUserToOrganisation(userId, acceptInviteResult.UserInviteEntity.OrganisationId);

                    var setUserAndUserEntryCommand = DependencyResolver.Current.GetService<IBusinessCommand<SetUserAndUserEntryToUserInviteArgs, SetUserAndUserEntryToUserInviteResult>>();
                    var setUserAndUserEntryResult = await setUserAndUserEntryCommand.Execute(new SetUserAndUserEntryToUserInviteArgs { InviteId = acceptInviteResult.UserInviteEntity.Id, UserEntryId = createUserEntryResult.CreatedUserEntryId });

                    if (!setUserAndUserEntryResult.IsSuccess)
                    {
                        return View("Error");
                    }

                    var organisationService = DependencyResolver.Current.GetService<IOrganisationService>();
                    var role = await organisationService.GetOrganisationUserRoleTypeByTitle("Member");

                    var createUserRoleCommand = DependencyResolver.Current.GetService<IBusinessCommand<CreateUserRoleArgs, CreateUserRoleResult>>();
                    var createUserRoleCommandResult = await createUserRoleCommand.Execute(new CreateUserRoleArgs { OrganisationUserEntryId = setUserAndUserEntryResult.OrganisationUserEntryId, UserRoleTypeId = role.Id });

                    if (!createUserRoleCommandResult.IsSuccess)
                    {
                        return View("Error");
                    }

                    // успешное подтвержение
                    // TODO: сделать соответствующую вьюшку
                    return View("ConfirmEmail");
                }
                // если regType - организация - редиректим на FillOrganisationDetails
                // если regType - юзер - редиректим на ConfirmEmail с уведомлением об успешной активации аккаунта
                string resultViewName = regType == NhpConstants.Controllers.Account.UserRegType ?
                    "ConfirmEmail" :
                        regType == NhpConstants.Controllers.Account.OrganisationRegType ?
                            "FillOrganisationDetails" :
                    "Error";



                return View(resultViewName, new FillOrganisationDetailsModel { UserId = userId });
            }
            // return View(result.Succeeded ? "ConfirmEmail" : "Error");

            return View("Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Не показывать, что пользователь не существует или не подтвержден
                    return View("ForgotPasswordFault");
                }

                //Дополнительные сведения о том, как включить подтверждение учетной записи и сброс пароля, см.по адресу: http://go.microsoft.com/fwlink/?LinkID=320771
                //Отправка сообщения электронной почты с этой ссылкой
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                try
                {
                    await UserManager.SendEmailAsync(user.Id, "Сброс пароля", "Сбросьте ваш пароль, щелкнув <a href=\"" + callbackUrl + "\">здесь</a>");
                }
                catch (Exception ex)
                {
                    _logger.Fatal("Проблема при отправке сообщения для восстановления пароля",ex);
                    return View("Error");
                }

                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // Появление этого сообщения означает наличие ошибки; повторное отображение формы
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Не показывать, что пользователь не существует
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Запрос перенаправления к внешнему поставщику входа
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == 0)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Создание и отправка маркера
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Выполнение входа пользователя посредством данного внешнего поставщика входа, если у пользователя уже есть имя входа
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);

                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });

                case SignInStatus.Failure:
                default:
                    // Если у пользователя нет учетной записи, то ему предлагается создать ее
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Получение сведений о пользователе от внешнего поставщика входа
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new CustomUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Вспомогательные приложения

        // Используется для защиты от XSRF-атак при добавлении внешних имен входа
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        #endregion Вспомогательные приложения

        #endregion Default Identity Behaviour
    }
}