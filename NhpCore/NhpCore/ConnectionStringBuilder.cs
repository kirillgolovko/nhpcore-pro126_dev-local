﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Configuration;

namespace NhpCore
{
    public class ConnectionStringBuilder
    {
        public static void CloneEnvironmentConfigSource()
        {
            ApplicationInfo.DefineAppMode();

            String path = AppDomain.CurrentDomain.BaseDirectory;
            Debug.WriteLine("Path = " + path);

            string mode = ApplicationInfo.Mode.ToString();

            var destinationPath = path + @"ConnectionStrings.config";
#if !DEBUG
            var isExist = File.Exists(destinationPath);
            if (isExist) { return; }
            else { goto copy; }


            copy:
#endif
            try
            {
                File.Copy(path + $@"EnvironmentConfiguration\ConnectionStrings-{mode}.config", destinationPath, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }
    }
}