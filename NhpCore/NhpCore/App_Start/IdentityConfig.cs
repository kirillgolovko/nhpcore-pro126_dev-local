﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using NhpCore.CoreLayer.Entities;

//using NhpCore.Infrastructure.Data.Ef.Extension;
using NhpCore.Infrastructure.Data.Ef;

namespace NhpCore
{
    //public class EmailService : IIdentityMessageService
    //{
    //    public Task SendAsync(IdentityMessage message)
    //    {
    //        // Подключите здесь службу электронной почты для отправки сообщения электронной почты.
    //        return Task.FromResult(0);
    //    }
    //}

    //public class SmsService : IIdentityMessageService
    //{
    //    public Task SendAsync(IdentityMessage message)
    //    {
    //        // Подключите здесь службу SMS, чтобы отправить текстовое сообщение.
    //        return Task.FromResult(0);
    //    }
    //}

    // Настройка диспетчера входа для приложения.
    //public class ApplicationSignInManager : SignInManager<CustomUser, int>
    //{
    //    public ApplicationSignInManager(CustomUserManager userManager, IAuthenticationManager authenticationManager)
    //        : base(userManager, authenticationManager)
    //    {
    //    }

    //    public override Task<ClaimsIdentity> CreateUserIdentityAsync(CustomUser user)
    //    {
    //        return user.GenerateUserIdentityAsync((CustomUserManager)UserManager);
    //    }

    //    public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
    //    {
    //        return new ApplicationSignInManager(context.GetUserManager<CustomUserManager>(), context.Authentication);
    //    }
    //}
}