﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.QueryExtensions
{
    internal static class GenericQueryableExtensions
    {
        internal static IQueryable<T> NotDeleted<T>(this IQueryable<T> query)
            where T : class, IDeletableState
        {
            return query.Where(x => !x.IsDeleted);
        }
    }
}