﻿using Common.DataAccess.UnitOfWork;
using NhpCore.CoreLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;
using NhpCore.Infrastructure.Data.Ef.QueryExtensions;
using System.Data.Entity;

namespace NhpCore.Infrastructure.Data.Ef.Services
{
    public class OrganisationService : BaseService, IOrganisationService
    {
        private readonly IUnitOfWork<NhpIdentityDbContext> _identityUoW;

        public OrganisationService(IUnitOfWork<NhpCoreDbContext> coreUoW,
            IUnitOfWork<NhpIdentityDbContext> identityUoW
            ) : base(coreUoW)
        {
            if (identityUoW == null) { throw new ArgumentNullException(nameof(identityUoW)); }

            _identityUoW = identityUoW;
        }

        public async Task<IEnumerable<CustomUser>> GetIdentityUsers(IEnumerable<string> emails)
        {
            IEnumerable<CustomUser> result = null;

            result = await _identityUoW.GetRepository<CustomUser>()
                .Query()
                .AsNoTracking()
                .Where(x => emails.Contains(x.Email))
                .ToListAsync();

            return result;
        }

        public async Task<OrganisationUserInvite> GetInvitedUser(Guid inviteId)
        {
            return await UnitOfWork.GetRepository<OrganisationUserInvite>()
                .Query()
                .FirstOrDefaultAsync(x => x.Id == inviteId);
        }

        public async Task<IEnumerable<OrganisationUserInvite>> GetInvitedUsers(int organisationId)
        {
            return await UnitOfWork.GetRepository<OrganisationUserInvite>()
                 .Query()
                 .Where(x => x.OrganisationId == organisationId)
                 .ToListAsync();
        }

        public async Task<Organisation> GetOrganisation(int id)
        {
            return await UnitOfWork.GetRepository<Organisation>()
                .Query()
                .Where(x => x.Id == id)
                .NotDeleted()
                .FirstOrDefaultAsync();
        }

        public async Task<OrganisationUserRoleType> GetOrganisationUserRoleTypeByTitle(string title)
        {
            var result = await UnitOfWork.GetRepository<OrganisationUserRoleType>()
                .Query()
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Title == title);

            return result;
        }

        public async Task<IEnumerable<OrganisationDocumentsStorageAccess>> GetStorageAccess(int organisationId)
        {
            var result = await UnitOfWork.GetRepository<OrganisationDocumentsStorageAccess>()
                 .Query()
                 .AsNoTracking()
                 .Where(x => x.OrganisationId == organisationId)
                 .ToListAsync();

            return result;
        }

        public async Task<IEnumerable<OrganisationUserEntry>> GetUserEntries(int organisationId)
        {
            return await UnitOfWork.GetRepository<OrganisationUserEntry>()
                .Query()
                .AsNoTracking()
                .Where(x => x.OrganisationId == organisationId)
                .ToListAsync();
        }
    }
}