﻿using Common.DataAccess.UnitOfWork;
using NhpCore.CoreLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;
using System.Data.Entity;

namespace NhpCore.Infrastructure.Data.Ef.Services
{
    public class SubscriptionService : BaseService, ISubscriptionService
    {
        public SubscriptionService(IUnitOfWork<NhpCoreDbContext> coreUoW) : base(coreUoW)
        {

        }

        public async Task<IEnumerable<Subscription>> GetAvailable()
        {
            return await UnitOfWork.GetRepository<Subscription>()
                .Query()
                .Where(x => x.IsActive).ToListAsync();
        }

        public async Task<IEnumerable<UserSubscriptionEntry>> GetUserEntries(int subscriptionId, IEnumerable<int> userIds)
        {
            return await UnitOfWork.GetRepository<UserSubscriptionEntry>()
                .Query()
                .AsNoTracking()
                .Where(x => x.SubscriptionId == subscriptionId && userIds.Contains(x.UserId))
                .ToListAsync();
        }
    }
}