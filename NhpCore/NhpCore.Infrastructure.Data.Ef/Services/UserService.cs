﻿using Common.DataAccess.UnitOfWork;
using NhpCore.CoreLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;
using NhpCore.Infrastructure.Data.Ef.QueryExtensions;
using System.Data.Entity;
using static NhpCore.Common.NhpConstants;

namespace NhpCore.Infrastructure.Data.Ef.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly IUnitOfWork<NhpIdentityDbContext> _identityUoW;

        public UserService(IUnitOfWork<NhpCoreDbContext> coreUoW, IUnitOfWork<NhpIdentityDbContext> identityUoW) : base(coreUoW)
        {
            if (identityUoW == null) { throw new ArgumentNullException(nameof(identityUoW)); }
            _identityUoW = identityUoW;
        }

        public async Task<IEnumerable<Organisation>> GetAvailableOrganisations(int userId)
        {
            var result = new List<Organisation>();
            var tmoResult = new Dictionary<Guid, Organisation>();

            var adminRoleTypes = await UnitOfWork.GetRepository<OrganisationUserRoleType>()
                .Query()
                .AsNoTracking()
                .Where(x => x.Title == "Admin" || x.Title == "SuperAdmin")
                .Select(x => x.Id)
                .ToListAsync();

            // записи о пользователе
            var OrganisationUserEntries = await UnitOfWork.GetRepository<OrganisationUserEntry>()
               .Query()
               .AsNoTracking()
               .Where(x => x.UserId == userId && x.IsEnabled && !x.IsDeleted)
               .ToListAsync();

            var entryIds = OrganisationUserEntries
                .Select(x => x.Id)
                .ToList();

            // роли
            var roles = await UnitOfWork.GetRepository<OrganisationUserRole>()
                .Query()
                .AsNoTracking()
                .Where(x => entryIds.Contains(x.OrganisationUserEntryId)
                        && adminRoleTypes.Contains(x.OrganisationUserRoleTypeId))
                .ToListAsync();

            //
            await Task.Run(() => OrganisationUserEntries.ForEach(x =>
            {
                if (tmoResult.ContainsKey(x.Id)) return;

                var isContains = roles.Any(c => c.OrganisationUserEntryId == x.Id);
                if (isContains)
                    tmoResult.Add(x.Id, x.Organisation);

            }));

            foreach (var kvp in tmoResult) result.Add(kvp.Value);

            return result;
        }

        public async Task<IEnumerable<Organisation>> GetAvailableOrganisationsByIdentityId(int identityId)
        {
            var user = await UnitOfWork.GetRepository<User>()
                .Query()
                 //.FirstOrDefaultAsync(x => x.IdentityId == identityId);
                 .FirstOrDefaultAsync(x => x.Id == identityId);

            if (user == null) { return null; }

            return await GetAvailableOrganisations(user.Id);
        }

        public async Task<User> GetCoreUser(int id)
        {
            var result = await UnitOfWork.GetRepository<User>()
                .Query()
                .AsNoTracking()
                .Where(x => x.Id == id)
                .NotDeleted()
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<User> GetCoreUserByIdentityId(int identityId)
        {
            var result = await UnitOfWork.GetRepository<User>()
                .Query()
                .AsNoTracking()
                //.Where(x => x.IdentityId == identityId)
                .Where(x => x.Id == identityId)
                .NotDeleted()
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<string> GetDisplayName(int identityId)
        {
            var query = await _identityUoW.GetRepository<CustomUserClaim>()
                .Query()
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.UserId == identityId && x.ClaimType == NhpClaimTypes.DisplayName);


            return query == null ? string.Empty : query.ClaimValue;
        }

        public async Task<CustomUser> GetIdentityUserById(int identityId)
        {
            var repository = _identityUoW.GetRepository<CustomUser>();

            return await repository.Query()
                .Where(x => x.Id == identityId)
                .FirstOrDefaultAsync();
        }

        public async Task<OrganisationUserEntry> GetUserEntry(Guid userEntryId)
        {
            return await UnitOfWork.GetRepository<OrganisationUserEntry>()
                .Query()
                .Where(x => x.Id == userEntryId)
                .FirstOrDefaultAsync();
        }

        public async Task<OrganisationUserEntry> GetUserEntryByIdentityId(int organisationId, int identityUserId)
        {
            var user = await this.GetCoreUserByIdentityId(identityUserId);

            if (user == null) { return null; }

            var result = await UnitOfWork.GetRepository<OrganisationUserEntry>()
                .Query()
                .AsNoTracking()
                .Where(x => x.OrganisationId == organisationId && x.UserId == user.Id)
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<OrganisationUserInvite> GetUserInvite(Guid id)
        {
            var result = await UnitOfWork.GetRepository<OrganisationUserInvite>()
                .Query()
                .FirstOrDefaultAsync(x => x.Id == id);

            return result;
        }

        public async Task<OrganisationUserInvite> GetUserInviteByInviteCode(Guid inviteCode)
        {
            var result = await UnitOfWork.GetRepository<OrganisationUserInvite>()
                 .Query()
                 .AsNoTracking()
                 .FirstOrDefaultAsync(x => x.InviteCode == inviteCode);

            return result;
        }

        public async Task<OrganisationUserInvite> GetUserInviteByInviteCode(string inviteCode)
        {
            Guid guid = Guid.Parse(inviteCode);

            var result = await GetUserInviteByInviteCode(guid);

            return result;
        }

        //public async Task<OrganisationUserProfile> GetOrganisationUserProfile(Guid id)
        //{
        //    var result = await UnitOfWork.GetRepository<OrganisationUserProfile>()
        //        .Query()
        //        .FirstOrDefaultAsync(x => x.Id == id);

        //    return result;
        //}

        public async Task<UserProfile> GetUserProfile(int id)
        {
            var result = await UnitOfWork.GetRepository<UserProfile>()
                .Query()
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);

            return result;
        }

        public async Task<IEnumerable<UserProfile>> GetAvailableProfiles(int userId)
        {
            var result = await UnitOfWork.GetRepository<UserProfile>()
               .Query()
               .AsNoTracking()
               .Where(x => x.UserId == userId)
               .ToListAsync();
            return result;
        }

    }
}