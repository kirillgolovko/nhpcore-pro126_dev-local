﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    public class SeedDocumentTemplates
    {
        public static IEnumerable<DocumentTemplateBase> GetTemplates()
        {
            return new List<DocumentTemplateBase>
            {
                new DocumentTemplateBinary{Name="BaseBin", BinaryTemplate=Encoding.UTF8.GetBytes("template") },
                new DocumentTemplateString {Name="Html", StringTemplate="<html></html>"}
            };
        }

        public static IEnumerable<DocumentTemplateString> GetStringTemplates()
        {
            return new List<DocumentTemplateString>
            {
                new DocumentTemplateString {Name="Html", StringTemplate="<html></html>"}
            };
        }

        public static IEnumerable<DocumentTemplateBinary> GetBinaryTemplates()
        {
            return new List<DocumentTemplateBinary>
            {
               new DocumentTemplateBinary{Name="BaseBin", BinaryTemplate=Encoding.UTF8.GetBytes("template"), FileSize=10, FileExtension="docx" },
            };
        }
    }
}
