﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    public class DbSeeder
    {
        public static void Seed(NhpCoreDbContext db)
        {
            db.BinaryDocumentTemplates.AddRange(SeedDocumentTemplates.GetBinaryTemplates());
            db.StringDocumentTemplates.AddRange(SeedDocumentTemplates.GetStringTemplates());
            db.OperationTypes.AddRange(SeedOperationType.SeedOperationTypes());
            db.OperationDocumentTypes.AddRange(SeedOperationDocumentType.SeedTypes());
            db.OrganisationUserRoleTypes.AddRange(SeedOrganisationUserRoleType.GetRoleTypes());
            db.OperationStatuses.AddRange(SeedOperationStatuses.GetOperationStatuses());
            db.PaymentTypes.AddRange(SeedPaymentTypes.GetPaymentTypes());
            db.OwnerDetails.AddRange(SeedOwnerDetails.GetOwnerDetails());
            db.SubscriptionOptionTypes.AddRange(SeedSubscriptionOptionType.GetOptionTypes());
            db.Users.AddRange(SeedUser.GetUsers());
            db.SaveChanges();
            SeedUser.GenerateUserProfile(db);
            SeedOrganisation.GetOrganisations(db);
            db.ITProducts.AddRange(SeedITProducts.GetServices());
            db.SaveChanges();

            SeedUser.ExcludeUsers(db);
            //SeedUser.GenerateUserProfile(db);

            SeedOrganisation.SeedPersonalOrganisations(db);
            SeedDepartment.SeedDepartments(db);
            SeedUserEntriesAndUserProfiles.SeedUserEntriesAndProfiles(db);
            SeedOrganisationUserRoles.SeedUserRoles(db);
            SeedOrganisationBalanceEntries.SeedEntries(db);
            SeedOrganisationUserInvites.SeedInvites(db);

            SeedSubscriptionOptions.SeedOptions(db);
            db.SaveChanges();

            SeedSubscriptions.Seed(db);
            db.SaveChanges();

            SeedOperations.Seed(db);
        }
    }
}