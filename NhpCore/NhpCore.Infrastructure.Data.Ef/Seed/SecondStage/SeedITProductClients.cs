﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    public class SeedITProductClients
    {
        public static void SeedClients(NhpCoreDbContext db)
        {
            foreach (var product in db.ITProducts)
            {
                product.Clients.Add(new ITProductClient { Name = $"Десктоп клиент для {product.Name}" });
            }
        }
    }
}
