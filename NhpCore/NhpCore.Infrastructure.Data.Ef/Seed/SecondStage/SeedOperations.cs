﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    public class SeedOperations
    {
        public static void Seed(NhpCoreDbContext db)
        {
            var org = db.Organisations.First();
            var owner = db.OwnerDetails.First();

            var details = new BusinessContractDetails
            {
                IssuerBankAddress = owner.BankAddress,
                IssuerBankBIK = owner.BankBIK,
                IssuerBankCorporateAccount = owner.BankCorporateAccount,
                IssuerBankCorrespondentAccount = owner.BankCorrespondentAccount,
                IssuerBankName = owner.BankName,
                IssuerBankOGRN = owner.BankOGRN,
                IssuerBankOKPO = owner.BankOKPO,
                IssuerChiefAccountant = owner.ChiefAccountant,
                IssuerChiefAccountantString = owner.ChiefAccountantString,
                IssuerDirector = owner.Director,
                IssuerDirectorString = owner.DirectorString,
                IssuerEmail = owner.Email,
                IssuerFullName = owner.FullName,
                IssuerINN = owner.INN,
                IssuerKPP = owner.KPP,
                IssuerOGRN = owner.KPP,
                IssuerLegalAddress = owner.LegalAddress,
                IssuerOKATO = owner.OKATO,
                IssuerOKPO = owner.OKPO,
                IssuerPhoneNumber = owner.PhoneNumber,
                IssuerPhoneNumberAdditional = owner.PhoneNumber,
                IssuerPhysicalAddress = owner.PhysicalAddress,
                IssuerShortName = owner.ShortName,
                ReceiverBankAddress = org.Details.BankAddress,
                ReceiverBankBIK = org.Details.BankBIK,
                ReceiverBankCorporateAccount = org.Details.BankCorporateAccount,
                ReceiverBankCorrespondentAccount = org.Details.BankCorrespondentAccount,
                ReceiverBankName = org.Details.BankName,
                ReceiverBankOGRN = org.Details.BankOGRN,
                ReceiverBankOKPO = org.Details.BankOKPO,
                ReceiverChiefAccountant = org.Details.ChiefAccountant,
                ReceiverChiefAccountantString = org.Details.ChiefAccountantString,
                ReceiverDirector = org.Details.Director,
                ReceiverDirectorString = org.Details.DirectorString,
                ReceiverEmail = org.Details.Email,
                ReceiverFullName = org.Details.FullName,
                ReceiverINN = org.Details.INN,
                ReceiverKPP = org.Details.KPP,
                ReceiverLegalAddress = org.Details.LegalAddress,
                ReceiverOGRN = org.Details.OGRN,
                ReceiverOKATO = org.Details.OKATO,
                ReceiverOKPO = org.Details.OKPO,
                ReceiverPhoneNumber = org.Details.PhoneNumber,
                ReceiverPhoneNumberAdditional = org.Details.PhoneNumberAdditional,
                ReceiverPhysicalAddress = org.Details.PhysicalAddress,
                ReceiverShortName = org.Details.ShortName
            };
            db.BusinessContractDetails.Add(details);
            db.SaveChanges();

            var payment = new SubscriptionPayment
            {
                BuyeerId = 1,
                IsPayed = true,
                InvalidationTime = DateTime.Now.AddMonths(12),
                Duration = 12,
                DateOfSuccesfullyPaid = DateTime.Now,
                BoughtSubscriptionId = 1
            };

            var operation = new Operation_SubscriptionPayment
            {
                CreationDate = DateTime.Now,
                CreationTime = DateTime.Now,
                OperationStatusId = 1,
                OperationTypeId = 1,
                PaymentId = payment.Id

            };

            var userSubscrEntry = new UserSubscriptionEntry
            {
                ActivateDate = DateTime.Now,
                IsActive = true,
                IsRefund = false,
                RefundDate = null,
                SubscriptionId = 1,
                UserId=14,
                SubscriptionPayment=payment
            };

            var operationUserEntry = new OperationUserEntry_SubscriptionPayment
            {
                Operation = operation,
                UserSubscriptionEntry = userSubscrEntry,
                UserId = 1,
            };

            db.UserSubscriptions.Add(userSubscrEntry);
            db.OperationUserEntries.Add(operationUserEntry);
            db.SubscriptionPayments.Add(payment);
            db.Operations.Add(operation);
            db.SaveChanges();



            var contract = new OperationDocument_SubscriptionPaymentContract
            {
                DocumentTypeId = 1,
                DocumentTemplateId = 1,
                ContractDetails = details,
                OperationId = operation.Id,
            };

            db.OperationDocuments.Add(contract);

            var bill = new OperationDocument_SubscriptionPaymentBill
            {
                DocumentTemplateId = 1,
                DocumentTypeId = 2,
                ContractId = contract.Id,
                OperationId = operation.Id,
                BillDetails = details
            };

            var billEntry = new SubscriptionPaymentBillEntry
            {
                Bill = bill,
                Position = 1,
                Price = 100,
                Quantity = 1,
            };
            db.OperationDocuments.Add(bill);
            db.SubscriptionPaymentBillEntries.Add(billEntry);
            db.SaveChanges();
            contract.BillId = bill.Id;
            bill.ContractId = contract.Id;

            db.SaveChanges();

            var subscrRefund = new SubscriptionRefund
            {
                RefundDate = DateTime.Now
            };

            var refundOperation = new Operation_SubscriptionRefund
            {
                SubscriptionRefund = subscrRefund,
                CreationDate=DateTime.Now,
                CreationTime=DateTime.Now,
                OperationStatusId=1,
                OperationTypeId=1,
                
            };

            var refundDoc = new OperationDocument_SubscriptionRefund
            {
                DocumentTemplateId=1,
                DocumentTypeId=1,
                Operation=refundOperation,
                Title="Refund"
            };
            db.SubscriptionRefunds.Add(subscrRefund);
            db.Operations.Add(refundOperation);

            db.OperationDocuments.Add(refundDoc);
            db.SaveChanges();

        }
    }
}
