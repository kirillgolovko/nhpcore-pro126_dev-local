﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    internal class SeedSubscriptionOptions
    {
        public static void SeedOptions(NhpCoreDbContext db)
        {
            Random rnd = new Random();

            var subTypes = db.SubscriptionOptionTypes.ToList();
            var products = db.ITProducts.ToList();

            products.ForEach(x =>
            {
                db.SubscriptionOptions.Add(new SubscriptionOption
                {
                    TypeId = 1,
                    Name = "BaseOption",
                    Description = "Базовая опция - характеризует минимально доступный функционал программы",
                    Cost = 500
                });
            });
        }
    }
}