﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    internal class SeedDepartment
    {
        public static void SeedDepartments(NhpCoreDbContext db)
        {
            var users = db.Users.ToList();
            var organisations = db.Organisations.ToList();

            ICollection<Department> result = new List<Department>
            {
                new Department { Name = "Отдел вентиляции", ChiefId=users[0].Id, OrganisationId=organisations[0].Id },
                new Department { Name = "Отдел отопления", ChiefId=users[1].Id, OrganisationId=organisations[1].Id },
                new Department { Name = "Отдел канализации" , ChiefId=users[2].Id, OrganisationId=organisations[2].Id},
            };

            db.Departments.AddRange(result);
        }
    }
}