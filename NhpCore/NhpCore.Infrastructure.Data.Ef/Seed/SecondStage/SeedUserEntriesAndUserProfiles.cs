﻿using NhpCore.CoreLayer.Entities;
//using NhpProject.Infrastructure.Data.Ef;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    public class SeedUserEntriesAndUserProfiles
    {

        public static void SeedUserEntriesAndProfiles(NhpCoreDbContext db)
        {
            var crossContainer = new Dictionary<int, List<int>>
            {
                {3, new List<int>{2, 4, 45,46,47,48} },
                {1, new List<int>{2,3, 4,6,7,8,9,10,12,14,15,17,18,19,20,21,22,23,24,28,29,31,32,33,34, 35,38,52,53,54,57, 58, 59 } },
                {4, new List<int>{2,42, 50,51 } }
            };
            var releaseConnStr = GetConnectionString("NhpIdentityDbContext", "Debug");
            var iddb = new NhpIdentityDbContext(releaseConnStr);
            var identityUsers = iddb.Users.ToList();

            //var prodb = new NhpProjectDbContext();
            //var orgs = prodb.Organisations.Where(x => x.Id < 5).ToList();
            //foreach (var org in orgs)
            //{
            //    foreach (var user in org.Users)
            //    {
            //        var idUser = identityUsers.FirstOrDefault(u => u.Id == user.Id);
            //        var entry = new OrganisationUserEntry
            //        {
            //            IsEnabled = true,
            //            UserId = user.Id,
            //            OrganisationId = org.Id,
            //            UserProfile = new OrganisationUserProfile { DisplayName = "Вы" },
            //            ProfileId = user.Id,
            //            IsDeleted = false,


            //        };
            //        if (idUser != null)
            //        {
            //            var displayName = idUser.Claims.FirstOrDefault(c => c.ClaimType == "displayname");
            //            if (displayName != null)
            //            {
            //                entry.UserProfile.DisplayName = displayName.ClaimValue;
            //            }
            //        }
            //        db.OrganisationUserEntries.Add(entry);
            //    }
            //}
            //prodb.Dispose();
            //iddb.Dispose();















            //var rnd = new Random();
            //var users = db.Users.ToList();
            //var organisations = db.Organisations.ToList();
            //var userRoles = db.OrganisationUserRoles.ToList();
            //var releaseConnStr = GetConnectionString("NhpIdentityDbContext", "Release");

            //List<CustomUser> identityUsers = null;
            //using (var iddb = new NhpIdentityDbContext(releaseConnStr))
            //{
            //    identityUsers = iddb.Users.ToList();

            //    List<int> orgs = null;
            //    for (int i = 0; i < users.Count; i++)
            //    {
            //        orgs = new List<int>();
            //        var userId = users[i].Id;

            //        foreach (var kvp in crossContainer)
            //        {
            //            var isContains = kvp.Value.Any(x => x == userId);

            //            if (isContains) orgs.Add(kvp.Key);
            //        }

            //        orgs.ForEach(x =>
            //        {
            //            // var idUser = identityUsers.FirstOrDefault(u => u.Id == users[i].IdentityId);
            //            var idUser = identityUsers.FirstOrDefault(u => u.Id == users[i].Id);

            //            var entry = new OrganisationUserEntry
            //            {
            //                IsEnabled = true,
            //                UserId = userId,
            //                OrganisationId = x,
            //                UserProfile = new OrganisationUserProfile
            //                {

            //                },
            //                ProfileId = users[i].Id,
            //                IsDeleted = false
            //            };
            //            if (idUser != null)
            //            {
            //                var displayName = idUser.Claims.FirstOrDefault(c => c.ClaimType == "displayname");
            //                if (displayName != null)
            //                {
            //                    entry.UserProfile.DisplayName = displayName.ClaimValue;
            //                }
            //            }
            //            db.OrganisationUserEntries.Add(entry);
            //        });
            //    }

            //}
            db.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="mode"></param>
        /// <returns>строка подключения, в случае ошибки - пустая строка (string.Empty)</returns>
        private static string GetConnectionString(string name, string mode)
        {
            string result = string.Empty;

            string path = AppDomain.CurrentDomain.BaseDirectory + $@"EnvironmentConfiguration\ConnectionStrings-{mode}.config";

            var isExist = File.Exists(path);
            if (isExist)
            {
                StreamReader sr = new StreamReader(path);

                while (result == string.Empty)
                {
                    var line = sr.ReadLine();
                    var splitedBySpace = line.Split(' ');
                    if (splitedBySpace.Length > 0 && splitedBySpace[0] != "<connectionStrings>")
                    {
                        if (splitedBySpace[3] == $@"name=""{name}""")
                        {

                            result = line.Split('"')[3];
                        }
                    }
                }
            }

            return result;
        }

    }
}