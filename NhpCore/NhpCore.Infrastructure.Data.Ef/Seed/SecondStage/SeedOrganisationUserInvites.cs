﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    internal class SeedOrganisationUserInvites
    {
        public static void SeedInvites(NhpCoreDbContext db)
        {
            var organisations = db.Organisations.ToList();

            for (int i = 0; i < organisations.Count; i++)
            {
                Enumerable.Range(1, 3).ToList().ForEach(x => {
                    db.OrganisationUserInvites.Add(new OrganisationUserInvite
                    {
                        Organisation = organisations[i],
                        Email = $"invite_{x}@mail.com",
                        InviteCode = Guid.NewGuid(),
                        InviteSentDate = DateTime.Now.Date,
                        InviteSentTime = DateTime.Now,
                    });
                });
            }
        }
    }
}