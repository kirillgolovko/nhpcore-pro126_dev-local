﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef.Seed
{
    internal class SeedSubscriptions
    {
        public static void Seed(NhpCoreDbContext db)
        {
            Random rnd = new Random();

            var products = db.ITProducts.ToList();
            var max = products.Count;
            var subs = db.SubscriptionOptions.ToList();

            products.ForEach(x =>
            {
                db.Subscriptions.Add(new Subscription
                {
                    Name = $"{x.Name} Базовая",
                    ItProductId = x.Id,
                    Options = new HashSet<SubscriptionOption> { subs[0] },
                    Price = 500,
                    IsActive = true
                });
            });

            //for (int i = 0; i < max; i++)
            //{
            //    for (int j = 1; j <= 3; j++)
            //    {
            //        db.Subscriptions.Add(new Subscription
            //        {
            //            Name = $"Подписка {j}",
            //            ItProductId = rnd.Next(1, max),
            //            Options = GetRandomOptions(subs),
            //            IsActive = true,
            //            Price = rnd.Next(100, 1000)
            //        });
            //    }
            //}
        }

        private static ICollection<SubscriptionOption> GetRandomOptions(List<SubscriptionOption> subs)
        {
            Random rnd = new Random();

            ICollection<SubscriptionOption> result = new HashSet<SubscriptionOption>();

            for (int i = 0; i < 3; i++)
            {
                var max = subs.Count - 1 >= 0 ? subs.Count - 1 : 0;

                var position = rnd.Next(0, max);
                result.Add(subs[position]);
            }

            return result;
        }
    }
}