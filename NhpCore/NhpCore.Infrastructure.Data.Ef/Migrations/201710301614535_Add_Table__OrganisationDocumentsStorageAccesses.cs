namespace NhpCore.Infrastructure.Data.Ef.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Table__OrganisationDocumentsStorageAccesses : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrganisationDocumentsStorageAccesses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccessName = c.String(),
                        StorageType = c.Int(nullable: false),
                        AccessType = c.Int(nullable: false),
                        OrganisationId = c.Int(nullable: false),
                        AssignerId = c.Int(nullable: false),
                        Token = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        RefreshToken = c.String(),
                        ExpiresIn = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.AssignerId, cascadeDelete: true)
                .ForeignKey("dbo.Organisations", t => t.OrganisationId, cascadeDelete: true)
                .Index(t => t.OrganisationId)
                .Index(t => t.AssignerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrganisationDocumentsStorageAccesses", "OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.OrganisationDocumentsStorageAccesses", "AssignerId", "dbo.Users");
            DropIndex("dbo.OrganisationDocumentsStorageAccesses", new[] { "AssignerId" });
            DropIndex("dbo.OrganisationDocumentsStorageAccesses", new[] { "OrganisationId" });
            DropTable("dbo.OrganisationDocumentsStorageAccesses");
        }
    }
}
