namespace NhpCore.Infrastructure.Data.Ef.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DocumentTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        FileSize = c.Int(),
                        BinaryTemplate = c.Binary(),
                        FileExtension = c.String(),
                        StringTemplate = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BusinessContractDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IssuerFullName = c.String(),
                        IssuerShortName = c.String(nullable: false),
                        IssuerOGRN = c.String(),
                        IssuerINN = c.String(nullable: false),
                        IssuerKPP = c.String(nullable: false),
                        IssuerOKPO = c.String(),
                        IssuerOKATO = c.String(),
                        IssuerLegalAddress = c.String(nullable: false),
                        IssuerPhysicalAddress = c.String(),
                        IssuerPhoneNumber = c.String(nullable: false),
                        IssuerPhoneNumberAdditional = c.String(),
                        IssuerEmail = c.String(nullable: false),
                        IssuerDirectorString = c.String(nullable: false),
                        IssuerDirectorId = c.Int(),
                        IssuerChiefAccountantString = c.String(nullable: false),
                        IssuerChiefAccountantId = c.Int(),
                        IssuerBankCorporateAccount = c.String(nullable: false),
                        IssuerBankName = c.String(nullable: false),
                        IssuerBankAddress = c.String(),
                        IssuerBankOGRN = c.String(nullable: false),
                        IssuerBankOKPO = c.String(nullable: false),
                        IssuerBankBIK = c.String(nullable: false),
                        IssuerBankCorrespondentAccount = c.String(nullable: false),
                        ReceiverFullName = c.String(),
                        ReceiverShortName = c.String(nullable: false),
                        ReceiverOGRN = c.String(),
                        ReceiverINN = c.String(nullable: false),
                        ReceiverKPP = c.String(nullable: false),
                        ReceiverOKPO = c.String(),
                        ReceiverOKATO = c.String(),
                        ReceiverLegalAddress = c.String(nullable: false),
                        ReceiverPhysicalAddress = c.String(),
                        ReceiverPhoneNumber = c.String(nullable: false),
                        ReceiverPhoneNumberAdditional = c.String(),
                        ReceiverEmail = c.String(nullable: false),
                        ReceiverDirectorString = c.String(nullable: false),
                        ReceiverDirectorId = c.Int(),
                        ReceiverChiefAccountantString = c.String(nullable: false),
                        ReceiverChiefAccountantId = c.Int(),
                        ReceiverBankCorporateAccount = c.String(nullable: false),
                        ReceiverBankName = c.String(nullable: false),
                        ReceiverBankAddress = c.String(),
                        ReceiverBankOGRN = c.String(nullable: false),
                        ReceiverBankOKPO = c.String(nullable: false),
                        ReceiverBankBIK = c.String(nullable: false),
                        ReceiverBankCorrespondentAccount = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.IssuerChiefAccountantId)
                .ForeignKey("dbo.Users", t => t.IssuerDirectorId)
                .ForeignKey("dbo.Users", t => t.ReceiverChiefAccountantId)
                .ForeignKey("dbo.Users", t => t.ReceiverDirectorId)
                .Index(t => t.IssuerDirectorId)
                .Index(t => t.IssuerChiefAccountantId)
                .Index(t => t.ReceiverDirectorId)
                .Index(t => t.ReceiverChiefAccountantId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        PhoneNumber = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleteTime = c.DateTime(),
                        IsEnabled = c.Boolean(nullable: false),
                        DisablingTime = c.DateTime(),
                        LastVisitedOrganisationId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Organisations", t => t.LastVisitedOrganisationId)
                .Index(t => t.LastVisitedOrganisationId);
            
            CreateTable(
                "dbo.OrganisationDetailCards",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        FullName = c.String(),
                        ShortName = c.String(nullable: false),
                        OGRN = c.String(),
                        INN = c.String(nullable: false),
                        KPP = c.String(nullable: false),
                        OKPO = c.String(),
                        OKATO = c.String(),
                        LegalAddress = c.String(nullable: false),
                        PhysicalAddress = c.String(),
                        PhoneNumber = c.String(nullable: false),
                        PhoneNumberAdditional = c.String(),
                        Email = c.String(nullable: false),
                        DirectorString = c.String(nullable: false),
                        DirectorId = c.Int(),
                        ChiefAccountantString = c.String(nullable: false),
                        ChiefAccountantId = c.Int(),
                        BankCorporateAccount = c.String(nullable: false),
                        BankName = c.String(nullable: false),
                        BankAddress = c.String(),
                        BankOGRN = c.String(nullable: false),
                        BankOKPO = c.String(nullable: false),
                        BankBIK = c.String(nullable: false),
                        BankCorrespondentAccount = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Organisations", t => t.Id)
                .ForeignKey("dbo.Users", t => t.ChiefAccountantId)
                .ForeignKey("dbo.Users", t => t.DirectorId)
                .Index(t => t.Id)
                .Index(t => t.DirectorId)
                .Index(t => t.ChiefAccountantId);
            
            CreateTable(
                "dbo.Organisations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        RegisterDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleteTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Balances",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Value = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Organisations", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.BalanceEntries",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        BalanceId = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Time = c.DateTime(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        Value = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Balances", t => t.BalanceId, cascadeDelete: true)
                .Index(t => t.BalanceId);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrganisationId = c.Int(nullable: false),
                        ChiefId = c.Int(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Organisations", t => t.OrganisationId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.ChiefId)
                .Index(t => t.OrganisationId)
                .Index(t => t.ChiefId);
            
            CreateTable(
                "dbo.OrganisationUserInvites",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        OrganisationId = c.Int(nullable: false),
                        Email = c.String(),
                        UserId = c.Int(),
                        InviteSentDate = c.DateTime(nullable: false),
                        InviteSentTime = c.DateTime(nullable: false),
                        AcceptDate = c.DateTime(),
                        AcceptTime = c.DateTime(),
                        IsAccepted = c.Boolean(nullable: false),
                        InviteCode = c.Guid(nullable: false),
                        OrganisationUserEntryId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Organisations", t => t.OrganisationId, cascadeDelete: true)
                .ForeignKey("dbo.OrganisationUserEntries", t => t.OrganisationUserEntryId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.OrganisationId)
                .Index(t => t.UserId)
                .Index(t => t.OrganisationUserEntryId);
            
            CreateTable(
                "dbo.OrganisationUserEntries",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        OrganisationId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        IsOwner = c.Boolean(),
                        IsPersonal = c.Boolean(),
                        IsEnabled = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleteTime = c.DateTime(),
                        DisablingTime = c.DateTime(),
                        ProfileId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Organisations", t => t.OrganisationId, cascadeDelete: true)
                .ForeignKey("dbo.UserProfiles", t => t.ProfileId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.OrganisationId)
                .Index(t => t.UserId)
                .Index(t => t.ProfileId);
            
            CreateTable(
                "dbo.UserProfiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        DisplayName = c.String(),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        PatronymicName = c.String(),
                        BirthYear = c.DateTime(nullable: false),
                        BirthDate = c.DateTime(nullable: false),
                        AboutMyself = c.String(),
                        City = c.String(),
                        Address = c.String(),
                        Gender = c.Boolean(nullable: false),
                        PassportSeries = c.String(),
                        PassportNumber = c.String(),
                        PassportSubdivisionCode = c.String(),
                        PassportWhoAndWhenGave = c.String(),
                        PassportRegistrationAddress = c.String(),
                        INN = c.String(),
                        Salary = c.String(),
                        JobTitle = c.String(),
                        SNILS = c.String(),
                        MobilePhone1 = c.String(),
                        MobilePhone2 = c.String(),
                        BusinessPhone = c.String(),
                        Email = c.String(),
                        Skype = c.String(),
                        Telegram = c.String(),
                        IsTelegram = c.String(),
                        IsEmail = c.String(),
                        ICQ = c.String(),
                        Facebook = c.String(),
                        LinkedIn = c.String(),
                        VK = c.String(),
                        Skills = c.String(),
                        Interests = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.OrganisationUserProfiles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PhoneNumber = c.String(),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        PatronymicName = c.String(),
                        DisplayName = c.String(),
                        Telegram = c.String(),
                        Email = c.String(),
                        Skype = c.String(),
                        Avatar = c.Binary(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrganisationUserEntries", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.OrganisationUserRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrganisationUserEntryId = c.Guid(nullable: false),
                        OrganisationUserRoleTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrganisationUserEntries", t => t.OrganisationUserEntryId, cascadeDelete: true)
                .ForeignKey("dbo.OrganisationUserRoleTypes", t => t.OrganisationUserRoleTypeId, cascadeDelete: true)
                .Index(t => t.OrganisationUserEntryId)
                .Index(t => t.OrganisationUserRoleTypeId);
            
            CreateTable(
                "dbo.OrganisationUserRoleTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SubscriptionPayments",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        IsPayed = c.Boolean(nullable: false),
                        BoughtSubscriptionId = c.Int(nullable: false),
                        Duration = c.Int(nullable: false),
                        DateOfSuccesfullyPaid = c.DateTime(),
                        InvalidationTime = c.DateTime(),
                        BuyeerId = c.Int(nullable: false),
                        PaymentType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Subscriptions", t => t.BoughtSubscriptionId, cascadeDelete: true)
                .ForeignKey("dbo.Organisations", t => t.BuyeerId, cascadeDelete: true)
                .ForeignKey("dbo.PaymentTypes", t => t.PaymentType_Id)
                .Index(t => t.BoughtSubscriptionId)
                .Index(t => t.BuyeerId)
                .Index(t => t.PaymentType_Id);
            
            CreateTable(
                "dbo.Subscriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ItProductId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ITProducts", t => t.ItProductId, cascadeDelete: true)
                .Index(t => t.ItProductId);
            
            CreateTable(
                "dbo.ITProducts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ITProductClients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ITProduct_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ITProducts", t => t.ITProduct_Id)
                .Index(t => t.ITProduct_Id);
            
            CreateTable(
                "dbo.SubscriptionOptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeId = c.Int(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Cost = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SubscriptionOptionTypes", t => t.TypeId, cascadeDelete: true)
                .Index(t => t.TypeId);
            
            CreateTable(
                "dbo.SubscriptionOptionTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserSubscriptionEntries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        UserId = c.Int(nullable: false),
                        SubscriptionPaymentId = c.Guid(nullable: false),
                        SubscriptionId = c.Int(nullable: false),
                        ActivateDate = c.DateTime(),
                        IsRefund = c.Boolean(nullable: false),
                        RefundDate = c.DateTime(),
                        ExpirationDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Subscriptions", t => t.SubscriptionId, cascadeDelete: true)
                .ForeignKey("dbo.SubscriptionPayments", t => t.SubscriptionPaymentId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.SubscriptionPaymentId)
                .Index(t => t.SubscriptionId);
            
            CreateTable(
                "dbo.OperationDocuments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentTypeId = c.Int(nullable: false),
                        DocumentTemplateId = c.Int(),
                        OperationId = c.Guid(nullable: false),
                        ContractId = c.Int(),
                        BillDetailsId = c.Int(),
                        ContractDetailsId = c.Int(),
                        BillId = c.Int(),
                        Title = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DocumentTemplates", t => t.DocumentTemplateId)
                .ForeignKey("dbo.OperationDocumentTypes", t => t.DocumentTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Operations", t => t.OperationId, cascadeDelete: true)
                .ForeignKey("dbo.BusinessContractDetails", t => t.BillDetailsId, cascadeDelete: true)
                .ForeignKey("dbo.BusinessContractDetails", t => t.ContractDetailsId)
                .Index(t => t.DocumentTypeId)
                .Index(t => t.DocumentTemplateId)
                .Index(t => t.OperationId)
                .Index(t => t.ContractId)
                .Index(t => t.BillDetailsId)
                .Index(t => t.ContractDetailsId)
                .Index(t => t.BillId);
            
            CreateTable(
                "dbo.OperationDocumentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Operations",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        CreationDate = c.DateTime(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        OperationTypeId = c.Int(nullable: false),
                        OperationStatusId = c.Int(nullable: false),
                        PaymentId = c.Guid(),
                        SubscriptionRefundId = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OperationStatuses", t => t.OperationStatusId, cascadeDelete: true)
                .ForeignKey("dbo.OperationTypes", t => t.OperationTypeId, cascadeDelete: true)
                .ForeignKey("dbo.SubscriptionPayments", t => t.PaymentId, cascadeDelete: true)
                .ForeignKey("dbo.SubscriptionRefunds", t => t.SubscriptionRefundId)
                .Index(t => t.OperationTypeId)
                .Index(t => t.OperationStatusId)
                .Index(t => t.PaymentId)
                .Index(t => t.SubscriptionRefundId);
            
            CreateTable(
                "dbo.OperationStatuses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OperationTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OperationUserEntries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        OperationId = c.Guid(nullable: false),
                        UserSubscriptionEntryId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Operations", t => t.OperationId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.UserSubscriptionEntries", t => t.UserSubscriptionEntryId)
                .Index(t => t.UserId)
                .Index(t => t.OperationId)
                .Index(t => t.UserSubscriptionEntryId);
            
            CreateTable(
                "dbo.SubscriptionRefunds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RefundDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SubscriptionPaymentBillEntries",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Position = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                        BillId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OperationDocuments", t => t.BillId, cascadeDelete: true)
                .Index(t => t.BillId);
            
            CreateTable(
                "dbo.OwnerDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullName = c.String(),
                        ShortName = c.String(nullable: false),
                        OGRN = c.String(),
                        INN = c.String(nullable: false),
                        KPP = c.String(nullable: false),
                        OKPO = c.String(),
                        OKATO = c.String(),
                        LegalAddress = c.String(nullable: false),
                        PhysicalAddress = c.String(),
                        PhoneNumber = c.String(nullable: false),
                        PhoneNumberAdditional = c.String(),
                        Email = c.String(nullable: false),
                        DirectorString = c.String(nullable: false),
                        DirectorId = c.Int(),
                        ChiefAccountantString = c.String(nullable: false),
                        ChiefAccountantId = c.Int(),
                        BankCorporateAccount = c.String(nullable: false),
                        BankName = c.String(nullable: false),
                        BankAddress = c.String(),
                        BankOGRN = c.String(nullable: false),
                        BankOKPO = c.String(nullable: false),
                        BankBIK = c.String(nullable: false),
                        BankCorrespondentAccount = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.ChiefAccountantId)
                .ForeignKey("dbo.Users", t => t.DirectorId)
                .Index(t => t.DirectorId)
                .Index(t => t.ChiefAccountantId);
            
            CreateTable(
                "dbo.PaymentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SubscriptionOptionSubscriptions",
                c => new
                    {
                        SubscriptionOption_Id = c.Int(nullable: false),
                        Subscription_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SubscriptionOption_Id, t.Subscription_Id })
                .ForeignKey("dbo.SubscriptionOptions", t => t.SubscriptionOption_Id, cascadeDelete: true)
                .ForeignKey("dbo.Subscriptions", t => t.Subscription_Id, cascadeDelete: true)
                .Index(t => t.SubscriptionOption_Id)
                .Index(t => t.Subscription_Id);
            
            CreateTable(
                "dbo.UsersDepartments",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        Department_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Department_Id })
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Departments", t => t.Department_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Department_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubscriptionPayments", "PaymentType_Id", "dbo.PaymentTypes");
            DropForeignKey("dbo.OwnerDetails", "DirectorId", "dbo.Users");
            DropForeignKey("dbo.OwnerDetails", "ChiefAccountantId", "dbo.Users");
            DropForeignKey("dbo.OperationDocuments", "ContractDetailsId", "dbo.BusinessContractDetails");
            DropForeignKey("dbo.SubscriptionPaymentBillEntries", "BillId", "dbo.OperationDocuments");
            DropForeignKey("dbo.OperationDocuments", "BillDetailsId", "dbo.BusinessContractDetails");
            DropForeignKey("dbo.Operations", "SubscriptionRefundId", "dbo.SubscriptionRefunds");
            DropForeignKey("dbo.Operations", "PaymentId", "dbo.SubscriptionPayments");
            DropForeignKey("dbo.OperationUserEntries", "UserSubscriptionEntryId", "dbo.UserSubscriptionEntries");
            DropForeignKey("dbo.OperationUserEntries", "UserId", "dbo.Users");
            DropForeignKey("dbo.OperationUserEntries", "OperationId", "dbo.Operations");
            DropForeignKey("dbo.Operations", "OperationTypeId", "dbo.OperationTypes");
            DropForeignKey("dbo.Operations", "OperationStatusId", "dbo.OperationStatuses");
            DropForeignKey("dbo.OperationDocuments", "OperationId", "dbo.Operations");
            DropForeignKey("dbo.OperationDocuments", "DocumentTypeId", "dbo.OperationDocumentTypes");
            DropForeignKey("dbo.OperationDocuments", "DocumentTemplateId", "dbo.DocumentTemplates");
            DropForeignKey("dbo.BusinessContractDetails", "ReceiverDirectorId", "dbo.Users");
            DropForeignKey("dbo.BusinessContractDetails", "ReceiverChiefAccountantId", "dbo.Users");
            DropForeignKey("dbo.BusinessContractDetails", "IssuerDirectorId", "dbo.Users");
            DropForeignKey("dbo.BusinessContractDetails", "IssuerChiefAccountantId", "dbo.Users");
            DropForeignKey("dbo.UserProfiles", "UserId", "dbo.Users");
            DropForeignKey("dbo.UsersDepartments", "Department_Id", "dbo.Departments");
            DropForeignKey("dbo.UsersDepartments", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Users", "LastVisitedOrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.OrganisationDetailCards", "DirectorId", "dbo.Users");
            DropForeignKey("dbo.Departments", "ChiefId", "dbo.Users");
            DropForeignKey("dbo.OrganisationDetailCards", "ChiefAccountantId", "dbo.Users");
            DropForeignKey("dbo.OrganisationDetailCards", "Id", "dbo.Organisations");
            DropForeignKey("dbo.UserSubscriptionEntries", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserSubscriptionEntries", "SubscriptionPaymentId", "dbo.SubscriptionPayments");
            DropForeignKey("dbo.UserSubscriptionEntries", "SubscriptionId", "dbo.Subscriptions");
            DropForeignKey("dbo.SubscriptionPayments", "BuyeerId", "dbo.Organisations");
            DropForeignKey("dbo.SubscriptionPayments", "BoughtSubscriptionId", "dbo.Subscriptions");
            DropForeignKey("dbo.SubscriptionOptions", "TypeId", "dbo.SubscriptionOptionTypes");
            DropForeignKey("dbo.SubscriptionOptionSubscriptions", "Subscription_Id", "dbo.Subscriptions");
            DropForeignKey("dbo.SubscriptionOptionSubscriptions", "SubscriptionOption_Id", "dbo.SubscriptionOptions");
            DropForeignKey("dbo.Subscriptions", "ItProductId", "dbo.ITProducts");
            DropForeignKey("dbo.ITProductClients", "ITProduct_Id", "dbo.ITProducts");
            DropForeignKey("dbo.OrganisationUserInvites", "UserId", "dbo.Users");
            DropForeignKey("dbo.OrganisationUserInvites", "OrganisationUserEntryId", "dbo.OrganisationUserEntries");
            DropForeignKey("dbo.OrganisationUserRoles", "OrganisationUserRoleTypeId", "dbo.OrganisationUserRoleTypes");
            DropForeignKey("dbo.OrganisationUserRoles", "OrganisationUserEntryId", "dbo.OrganisationUserEntries");
            DropForeignKey("dbo.OrganisationUserProfiles", "Id", "dbo.OrganisationUserEntries");
            DropForeignKey("dbo.OrganisationUserEntries", "UserId", "dbo.Users");
            DropForeignKey("dbo.OrganisationUserEntries", "ProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.OrganisationUserEntries", "OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.OrganisationUserInvites", "OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Departments", "OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.Balances", "Id", "dbo.Organisations");
            DropForeignKey("dbo.BalanceEntries", "BalanceId", "dbo.Balances");
            DropIndex("dbo.UsersDepartments", new[] { "Department_Id" });
            DropIndex("dbo.UsersDepartments", new[] { "User_Id" });
            DropIndex("dbo.SubscriptionOptionSubscriptions", new[] { "Subscription_Id" });
            DropIndex("dbo.SubscriptionOptionSubscriptions", new[] { "SubscriptionOption_Id" });
            DropIndex("dbo.OwnerDetails", new[] { "ChiefAccountantId" });
            DropIndex("dbo.OwnerDetails", new[] { "DirectorId" });
            DropIndex("dbo.SubscriptionPaymentBillEntries", new[] { "BillId" });
            DropIndex("dbo.OperationUserEntries", new[] { "UserSubscriptionEntryId" });
            DropIndex("dbo.OperationUserEntries", new[] { "OperationId" });
            DropIndex("dbo.OperationUserEntries", new[] { "UserId" });
            DropIndex("dbo.Operations", new[] { "SubscriptionRefundId" });
            DropIndex("dbo.Operations", new[] { "PaymentId" });
            DropIndex("dbo.Operations", new[] { "OperationStatusId" });
            DropIndex("dbo.Operations", new[] { "OperationTypeId" });
            DropIndex("dbo.OperationDocuments", new[] { "BillId" });
            DropIndex("dbo.OperationDocuments", new[] { "ContractDetailsId" });
            DropIndex("dbo.OperationDocuments", new[] { "BillDetailsId" });
            DropIndex("dbo.OperationDocuments", new[] { "ContractId" });
            DropIndex("dbo.OperationDocuments", new[] { "OperationId" });
            DropIndex("dbo.OperationDocuments", new[] { "DocumentTemplateId" });
            DropIndex("dbo.OperationDocuments", new[] { "DocumentTypeId" });
            DropIndex("dbo.UserSubscriptionEntries", new[] { "SubscriptionId" });
            DropIndex("dbo.UserSubscriptionEntries", new[] { "SubscriptionPaymentId" });
            DropIndex("dbo.UserSubscriptionEntries", new[] { "UserId" });
            DropIndex("dbo.SubscriptionOptions", new[] { "TypeId" });
            DropIndex("dbo.ITProductClients", new[] { "ITProduct_Id" });
            DropIndex("dbo.Subscriptions", new[] { "ItProductId" });
            DropIndex("dbo.SubscriptionPayments", new[] { "PaymentType_Id" });
            DropIndex("dbo.SubscriptionPayments", new[] { "BuyeerId" });
            DropIndex("dbo.SubscriptionPayments", new[] { "BoughtSubscriptionId" });
            DropIndex("dbo.OrganisationUserRoles", new[] { "OrganisationUserRoleTypeId" });
            DropIndex("dbo.OrganisationUserRoles", new[] { "OrganisationUserEntryId" });
            DropIndex("dbo.OrganisationUserProfiles", new[] { "Id" });
            DropIndex("dbo.UserProfiles", new[] { "UserId" });
            DropIndex("dbo.OrganisationUserEntries", new[] { "ProfileId" });
            DropIndex("dbo.OrganisationUserEntries", new[] { "UserId" });
            DropIndex("dbo.OrganisationUserEntries", new[] { "OrganisationId" });
            DropIndex("dbo.OrganisationUserInvites", new[] { "OrganisationUserEntryId" });
            DropIndex("dbo.OrganisationUserInvites", new[] { "UserId" });
            DropIndex("dbo.OrganisationUserInvites", new[] { "OrganisationId" });
            DropIndex("dbo.Departments", new[] { "ChiefId" });
            DropIndex("dbo.Departments", new[] { "OrganisationId" });
            DropIndex("dbo.BalanceEntries", new[] { "BalanceId" });
            DropIndex("dbo.Balances", new[] { "Id" });
            DropIndex("dbo.OrganisationDetailCards", new[] { "ChiefAccountantId" });
            DropIndex("dbo.OrganisationDetailCards", new[] { "DirectorId" });
            DropIndex("dbo.OrganisationDetailCards", new[] { "Id" });
            DropIndex("dbo.Users", new[] { "LastVisitedOrganisationId" });
            DropIndex("dbo.BusinessContractDetails", new[] { "ReceiverChiefAccountantId" });
            DropIndex("dbo.BusinessContractDetails", new[] { "ReceiverDirectorId" });
            DropIndex("dbo.BusinessContractDetails", new[] { "IssuerChiefAccountantId" });
            DropIndex("dbo.BusinessContractDetails", new[] { "IssuerDirectorId" });
            DropTable("dbo.UsersDepartments");
            DropTable("dbo.SubscriptionOptionSubscriptions");
            DropTable("dbo.PaymentTypes");
            DropTable("dbo.OwnerDetails");
            DropTable("dbo.SubscriptionPaymentBillEntries");
            DropTable("dbo.SubscriptionRefunds");
            DropTable("dbo.OperationUserEntries");
            DropTable("dbo.OperationTypes");
            DropTable("dbo.OperationStatuses");
            DropTable("dbo.Operations");
            DropTable("dbo.OperationDocumentTypes");
            DropTable("dbo.OperationDocuments");
            DropTable("dbo.UserSubscriptionEntries");
            DropTable("dbo.SubscriptionOptionTypes");
            DropTable("dbo.SubscriptionOptions");
            DropTable("dbo.ITProductClients");
            DropTable("dbo.ITProducts");
            DropTable("dbo.Subscriptions");
            DropTable("dbo.SubscriptionPayments");
            DropTable("dbo.OrganisationUserRoleTypes");
            DropTable("dbo.OrganisationUserRoles");
            DropTable("dbo.OrganisationUserProfiles");
            DropTable("dbo.UserProfiles");
            DropTable("dbo.OrganisationUserEntries");
            DropTable("dbo.OrganisationUserInvites");
            DropTable("dbo.Departments");
            DropTable("dbo.BalanceEntries");
            DropTable("dbo.Balances");
            DropTable("dbo.Organisations");
            DropTable("dbo.OrganisationDetailCards");
            DropTable("dbo.Users");
            DropTable("dbo.BusinessContractDetails");
            DropTable("dbo.DocumentTemplates");
        }
    }
}
