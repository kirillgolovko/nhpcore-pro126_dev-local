﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using NhpCore.CoreLayer.Entities;
using NhpCore.Infrastructure.Data.Ef.Initializer;
using System.ComponentModel.DataAnnotations.Schema;

namespace NhpCore.Infrastructure.Data.Ef
{
    public interface INhpCoreDbContext
    {
    }

    //[DbConfigurationType(typeof(AzureDbConfiguration))]
    public class NhpCoreDbContext : DbContext, INhpCoreDbContext
    {
        #region ctor

        //static NhpCoreDbContext()
        //{
        //    Database.SetInitializer(new NhpCoreDbContextDropCreateAlwaysInitializer());
        //}

        public NhpCoreDbContext()
            : base("NhpCoreDbContext")
        {
        }

        public NhpCoreDbContext(string connString)
            : base(connString)
        {
        }

        #endregion ctor

        #region create

        public static NhpCoreDbContext Create()
        {
            return new NhpCoreDbContext();
        }

        public static NhpCoreDbContext Create(string connectionString)
        {
            return new NhpCoreDbContext(connectionString);
        }

        #endregion create

        #region DbSets
        #region Operations
        // public DbSet<OperationDocumentDetailsBase> OperationDocumentDetails { get; set; }
        public DbSet<DocumentTemplateBinary> BinaryDocumentTemplates { get; set; }
        public DbSet<DocumentTemplateString> StringDocumentTemplates { get; set; }
        public DbSet<OperationDocumentBase> OperationDocuments { get; set; }

        //  public DbSet<OperationDocument_SubscriptionPaymentContract> SubscriptionPaymentOperationDocuments { get; set; }
        public DbSet<OperationUserEntryBase> OperationUserEntries { get; set; }
        public DbSet<OperationDocumentType> OperationDocumentTypes { get; set; }
        //public DbSet<OperationInformationBase> OperationAdditionalInformations { get; set; }
        public DbSet<OperationBase> Operations { get; set; }
        public DbSet<OperationType> OperationTypes { get; set; }
        public DbSet<OperationStatus> OperationStatuses { get; set; }
        #endregion
        public DbSet<Balance> OrganisationBalances { get; set; }
        public DbSet<BalanceEntry> OrganisationBalanceEntries { get; set; }
        //public DbSet<BusinessContract> BusinessContracts { get; set; }
        //public DbSet<BusinessContractBill> BusinessContractBills { get; set; }
        //public DbSet<BusinessContractBillEntry> BusinessContractBillEntries { get; set; }
        public DbSet<BusinessContractDetails> BusinessContractDetails { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<ITProduct> ITProducts { get; set; }
        public DbSet<ITProductClient> ITProductClients { get; set; }
        public DbSet<Organisation> Organisations { get; set; }
        public DbSet<OrganisationDocumentsStorageAccess> OrganisationDocumentsStorageAccesses { get; set; }
        public DbSet<OrganisationDetailCard> OrganisationDetailCards { get; set; }
        public DbSet<OrganisationUserEntry> OrganisationUserEntries { get; set; }
        public DbSet<OrganisationUserInvite> OrganisationUserInvites { get; set; }
        //public DbSet<OrganisationUserProfile> OrganisationUserProfiles { get; set; }
        public DbSet<OrganisationUserRole> OrganisationUserRoles { get; set; }
        public DbSet<OrganisationUserRoleType> OrganisationUserRoleTypes { get; set; }
        public DbSet<OwnerDetails> OwnerDetails { get; set; }
        public DbSet<PaymentType> PaymentTypes { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<SubscriptionOption> SubscriptionOptions { get; set; }
        public DbSet<SubscriptionOptionType> SubscriptionOptionTypes { get; set; }
        public DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }
        public DbSet<SubscriptionPaymentBillEntry> SubscriptionPaymentBillEntries { get; set; }
        public DbSet<UserSubscriptionEntry> UserSubscriptions { get; set; }
        public DbSet<SubscriptionRefund> SubscriptionRefunds { get; set; }
        // public DbSet<SubscriptionPaymentUserEntry> SubscriptionPaymentUserEntries { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }

        #endregion DbSets

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region OperationBase

            modelBuilder.Entity<OperationBase>()
                .HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            #endregion

            #region SubscriptionPayment

            modelBuilder.Entity<SubscriptionPayment>()
                .HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            #endregion

            #region SubscriptionPaymentBillEntry

            modelBuilder.Entity<SubscriptionPaymentBillEntry>()
                .HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            #endregion

            #region User

            modelBuilder.Entity<User>()
                .HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // many to many
            modelBuilder.Entity<User>()
                .HasMany(x => x.MemberOfDepartments)
                .WithMany(x => x.Members)
                .Map(x => x.ToTable("UsersDepartments"));

            // one to many
            modelBuilder.Entity<User>()
                .HasMany(x => x.ChiefOfDepartments)
                .WithRequired(x => x.Chief)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(x => x.DirectorAt)
                .WithOptional(x => x.Director)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(x => x.AccountantAt)
                .WithOptional(x => x.ChiefAccountant)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(x => x.Profiles)
                .WithRequired(x => x.User)
                .WillCascadeOnDelete(false);
            #endregion User

            #region UserProfile

            modelBuilder.Entity<UserProfile>()
                .HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<UserProfile>().HasMany(x => x.OrganisationUserEntries).WithOptional(x=>x.UserProfile);

            #endregion UserProfile

            #region Organisations

            modelBuilder.Entity<Organisation>()
                .HasRequired(x => x.Balance)
                .WithRequiredPrincipal(x => x.Organisation);

            //modelBuilder.Entity<Organisation>()
            //    .HasMany(x => x.OrganisationUserEntry)
            //    .WithRequired(x => x.Organisation)
            //    .WillCascadeOnDelete(false);

            #endregion Organisations

            #region OrganisationUserEntry

            modelBuilder.Entity<OrganisationUserEntry>()
                .HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            //modelBuilder.Entity<OrganisationUserEntry>()
            //    .HasRequired(x => x.Organisation)
            //    .WithMany(x => x.OrganisationUserEntry);

            // one to one
            //modelBuilder.Entity<OrganisationUserEntry>()
            //    .HasMany(x => x.UserProfile)
            //    .Map(x => x.ToTable("UserProfiles"));

            #endregion OrganisationUserEntry

            #region BalanceEntries

            modelBuilder.Entity<BalanceEntry>()
                .HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            #endregion BalanceEntries

            #region OrganisationUserInvites

            modelBuilder.Entity<OrganisationUserInvite>()
                .HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            #endregion OrganisationUserInvites

            #region UserSubscriptionEntry
            modelBuilder.Entity<UserSubscriptionEntry>()
                .HasRequired(x => x.SubscriptionPayment)
                .WithMany(x => x.UserEntries)
                .WillCascadeOnDelete(false);
            #endregion

        }
    }
}