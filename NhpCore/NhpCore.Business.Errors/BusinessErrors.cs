﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NhpCore.Business.Errors
{
    public static class BusinessErrors
    {
        /// <summary>
        /// Системные ошибки
        /// </summary>
        public static class System
        {
            public const string INVALID_REQUEST = "Неверный запрос";
            public const int INVALID_REQUEST_CODE = 1000;

            public const string ARGUMENT_IS_NULL = "Argument is null";
            public const int ARGUMENT_IS_NULL_CODE = 1001;

            public const string INTERNAL_SERVER_ERROR = "Внутренняя ошибка сервера :( Повторите попытку позже.";
            public const int INTERNAL_SERVER_ERROR_CODE = 1002;
        }

        public static class User
        {
            public const string USER_NOT_FOUND = "Пользователь не найден";
            public const int USER_NOT_FOUND_CODE = 1003;

            public const string USER_ALREADY_EXIST = "Такой пользователь уже существует";
            public const int USER_ALREADY_EXIST_CODE = 1006;

            public const string CREATING_ERROR = "Возникла проблема во время создания пользователя";
            public const int CREATING_ERROR_CODE = 1008;

            public const string USERINVITE_NOT_FOUND = "Приглашение пользователя не найдено";
            public const int USERINVITE_NOT_FOUND_CODE = 1012;

            public const string CLAIM_DISPLAYNAME_NOT_FOUND = "DisplayName не найден";
            public const int CLAIM_DISPLAYNAME_NOT_FOUND_CODE = 1013;
        }

        public static class Organisation
        {
            public const string ORGANISATION_NOT_FOUND = "Организация не найдена";
            public const int ORGANISATION_NOT_FOUND_CODE = 1004;

            public const string ORGANISATION_USER_ENTRY_NOT_FOUND = "Запись о пользователе организации не найдена";
            public const int ORGANISATION_USER_ENTRY_NOT_FOUND_CODE = 1005;

            public const string ORGANISATION_USERINVITE_ENTRY_NOT_FOUND = "Запись о приглашенном организацией пользователе не найдена";
            public const int ORGANISATION_USERINVITE_ENTRY_NOT_FOUND_CODE = 1007;

            public const string CREATING_ERROR = "Возникла проблема при создании организации";
            public const int CREATING_ERROR_CODE = 1009;

            public const string ORGANISATION_USER_ENTRY_CREATING_ERROR = "Возникла проблема при создании записи пользователя в организации";
            public const int ORGANISATION_USER_ENTRY_CREATING_ERROR_CODE = 1010;

            public const string ORGANISATION_USERENTRY_ROLE_CREATING_ERROR = "Возникла проблема при создании организации";
            public const int ORGANISATION_USERENTRY_ROLE_CREATING_ERROR_CODE = 1011;

            public const string ORGANISATION_USERPROFILE_NOT_FOUND = "Профиль пользователя не найден";
            public const int ORGANISATION_USERPROFILE_NOT_FOUND_CODE = 1014;
        }

        public static class UserProfile
        {
            public const string USER_NOT_FOUND = "Пользователь для профиля не найден";
            public const int USER_NOT_FOUND_CODE = 4001;

            public const string USERPROFILE_NOT_FOUND = "Профиль не существует";
            public const int USERPROFILE_NOT_FOUND_CODE = 4002;

            public const string USERPROFILE_CREATING_ERROR = "Возникла проблема при создании профиля";
            public const int USERPROFILE_CREATING_ERROR_CODE = 4003;

            public const string USERPROFILE_EDITING_ERROR = "Возникла проблема при редактировании профиля";
            public const int USERPROFILE_EDITING_ERROR_CODE = 4004;

            public const string USERPROFILE_DELETING_ERROR = "Возникла проблема при удалении профиля";
            public const int USERPROFILE_DELETING_ERROR_CODE = 4005;
        }

    }
}