﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.DTOs
{
    public class DbOrganisationDocumentsStorageAccess
    {
        public int Id { get; set; }
        public string AccessName { get; set; }
        public string Token { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int StorageType { get; set; }
    }
}
