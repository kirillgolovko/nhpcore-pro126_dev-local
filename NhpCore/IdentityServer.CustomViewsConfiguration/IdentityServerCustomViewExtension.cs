﻿using IdentityServer3.Core.Configuration;
using IdentityServer3.Core.Services.Default;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityServer.CustomViewsConfiguration
{
    public static class IdentityServerCustomViewExtension
    {
        public static void ConfigureIdentityServerCustomViews(this IdentityServerServiceFactory factory)
        {
            var viewOptions = new DefaultViewServiceOptions();
            viewOptions.Stylesheets.Add("/Content/IdSrv3/Style/customStyle.css");
            
            viewOptions.CustomViewDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Content\\IdSrv3\\", "ViewTemplates");


            factory.ConfigureDefaultViewService(viewOptions);
        }
    }
}
