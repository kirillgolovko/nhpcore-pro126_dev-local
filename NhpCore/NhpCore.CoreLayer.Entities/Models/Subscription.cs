﻿using Common.EntityFramework;
using System;
using System.Collections;
using System.Collections.Generic;

namespace NhpCore.CoreLayer.Entities
{
    public class Subscription:IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ItProductId { get; set; }
        public virtual ITProduct ItProduct { get; set; }

        public bool IsActive { get; set; }
        public double Price { get; set; }
        public virtual ICollection<SubscriptionPayment> Purchases { get; set; }
        public virtual ICollection<SubscriptionOption> Options { get; set; }

        public Subscription()
        {
            Purchases = new HashSet<SubscriptionPayment>();
            Options = new HashSet<SubscriptionOption>();
        }
    }
}