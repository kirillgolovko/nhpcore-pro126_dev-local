﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class SubscriptionPaymentBillEntry : IEntity
    {
        public Guid Id { get; set; }
        public int Position { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public int BillId { get; set; }
        public OperationDocument_SubscriptionPaymentBill Bill { get; set; }
    }
}
