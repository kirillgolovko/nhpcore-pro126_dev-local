﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class Department
    {
        public int Id { get; set; }
        public int OrganisationId { get; set; }
        public int ParentId { get; set; }
        public virtual Organisation Organisation { get; set; }
        public int? ChiefId { get; set; }
        public virtual User Chief { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<User> Members { get; set; }

        public Department()
        {
            Members = new HashSet<User>();
        }
    }
}