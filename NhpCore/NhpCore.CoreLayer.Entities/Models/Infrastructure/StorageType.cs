﻿namespace NhpCore.CoreLayer.Entities
{
    public enum StorageType
    {
        YandexDisk,
        GoogleDrive,
        MailRuDisk,
        MS_OneDrive,
        MS_SharePoint,
    }
}