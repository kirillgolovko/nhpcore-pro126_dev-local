﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{

    public class SubscriptionRefund : IEntity
    {
        public int Id { get; set; }
        public DateTime RefundDate { get; set; }
        
    }
}
