﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    /// <summary>
    /// Информация о платеже на подписку. 
    /// </summary>
    public class SubscriptionPayment : IEntity
    {
        public Guid Id { get; set; }

        public bool IsPayed { get; set; }

        // купленная подписка
        public int BoughtSubscriptionId { get; set; }
        public virtual Subscription BoughtSubscription { get; set; }

        // Продолжительность подписки в месяцах
        public int Duration { get; set; }

        // дата успешной оплаты
        public DateTime? DateOfSuccesfullyPaid { get; set; }
        public DateTime? InvalidationTime { get; set; }
        // Покупатель
        public int BuyeerId { get; set; }
        public Organisation Buyeer { get; set; }

        public virtual ICollection<UserSubscriptionEntry> UserEntries { get; set; }

        public SubscriptionPayment()
        {
            UserEntries = new HashSet<UserSubscriptionEntry>();
        }
    }
}
