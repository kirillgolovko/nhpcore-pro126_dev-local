﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class OrganisationDetailCard : IEntity
    {
        [Key, ForeignKey("Organisation")]
        public int Id { get; set; }

        public virtual Organisation Organisation { get; set; }

        public string FullName { get; set; }

        [Required]
        public string ShortName { get; set; }

        // ОГРН - основной государственный регистрационный номер
        public string OGRN { get; set; }

        //ИНН - Идентификационный номер налогоплательщика
        [Required]
        public string INN { get; set; }

        // КПП - Код причины постановки на учет
        [Required]
        public string KPP { get; set; }

        // ОКПО - Общероссийский классификатор предприятий и организаций
        public string OKPO { get; set; }

        // ОКАТО - Общероссийский классификатор объектов административно-территориального деления
        public string OKATO { get; set; }

        // Юр.адрес лица
        [Required]
        public string LegalAddress { get; set; }

        // Фактический адрес
        public string PhysicalAddress { get; set; }

        // Номер телефона
        [Required]
        public string PhoneNumber { get; set; }

        // добавочный номер к номеру телефона
        public string PhoneNumberAdditional { get; set; }

        [Required]
        public string Email { get; set; }

        // ФИО Директора, используется если пользователь - "Директор"  еще не зарегистрирован
        [Required]
        public string DirectorString { get; set; }

        // Id директора
        public int? DirectorId { get; set; }

        public virtual User Director { get; set; }

        // ФИО ГлавногоБухгалтера, используется если пользователь - "ГлавБух"  еще не зарегистрирован
        [Required]
        public string ChiefAccountantString { get; set; }

        // Id ГлавБуха
        public int? ChiefAccountantId { get; set; }

        public virtual User ChiefAccountant { get; set; }

        #region Банковские реквизиты
        ///
        ///БАНКОВСКИЕ РЕКВИЗИТЫ
        ///

        // Р/с Расчетный счёт
        [Required]
        public string BankCorporateAccount { get; set; }

        // Название базнка
        [Required]
        public string BankName { get; set; }

        //Адрес банка
        public string BankAddress { get; set; }

        // ОГРН
        [Required]
        public string BankOGRN { get; set; }

        // ОКПО
        [Required]
        public string BankOKPO { get; set; }

        // БИК
        [Required]
        public string BankBIK { get; set; }

        // К/с -  Корреспондентский счет
        [Required]
        public string BankCorrespondentAccount { get; set; }
        #endregion
    }
}