﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    /// <summary>
    /// Актуальные реквизиты в момент выставления счета.
    /// </summary>
    public class BusinessContractDetails:IEntity
    {
        public int Id { get; set; }

        #region Выставитель счета - Issuer
        public string IssuerFullName { get; set; }

        [Required]
        public string IssuerShortName { get; set; }

        // ОГРН - основной государственный регистрационный номер
        public string IssuerOGRN { get; set; }

        //ИНН - Идентификационный номер налогоплательщика
        [Required]
        public string IssuerINN { get; set; }

        // КПП - Код причины постановки на учет
        [Required]
        public string IssuerKPP { get; set; }

        // ОКПО - Общероссийский классификатор предприятий и организаций
        public string IssuerOKPO { get; set; }

        // ОКАТО - Общероссийский классификатор объектов административно-территориального деления
        public string IssuerOKATO { get; set; }

        // Юр.адрес лица
        [Required]
        public string IssuerLegalAddress { get; set; }

        // Фактический адрес
        public string IssuerPhysicalAddress { get; set; }

        // Номер телефона
        [Required]
        public string IssuerPhoneNumber { get; set; }

        // добавочный номер к номеру телефона
        public string IssuerPhoneNumberAdditional { get; set; }

        [Required]
        public string IssuerEmail { get; set; }

        // ФИО Директора, используется если пользователь - "Директор"  еще не зарегистрирован
        [Required]
        public string IssuerDirectorString { get; set; }

        // Id директора
        public int? IssuerDirectorId { get; set; }

        public virtual User IssuerDirector { get; set; }

        // ФИО ГлавногоБухгалтера, используется если пользователь - "ГлавБух"  еще не зарегистрирован
        [Required]
        public string IssuerChiefAccountantString { get; set; }

        // Id ГлавБуха
        public int? IssuerChiefAccountantId { get; set; }

        public virtual User IssuerChiefAccountant { get; set; }

        #region Банковские реквизиты
        ///
        ///БАНКОВСКИЕ РЕКВИЗИТЫ
        ///

        // Р/с Расчетный счёт
        [Required]
        public string IssuerBankCorporateAccount { get; set; }

        // Название базнка
        [Required]
        public string IssuerBankName { get; set; }

        //Адрес банка
        public string IssuerBankAddress { get; set; }

        // ОГРН
        [Required]
        public string IssuerBankOGRN { get; set; }

        // ОКПО
        [Required]
        public string IssuerBankOKPO { get; set; }

        // БИК
        [Required]
        public string IssuerBankBIK { get; set; }

        // К/с -  Корреспондентский счет
        [Required]
        public string IssuerBankCorrespondentAccount { get; set; }
        #endregion

        #endregion

        #region Получатель счета Receiver

        public string ReceiverFullName { get; set; }

        [Required]
        public string ReceiverShortName { get; set; }

        // ОГРН - основной государственный регистрационный номер
        public string ReceiverOGRN { get; set; }

        //ИНН - Идентификационный номер налогоплательщика
        [Required]
        public string ReceiverINN { get; set; }

        // КПП - Код причины постановки на учет
        [Required]
        public string ReceiverKPP { get; set; }

        // ОКПО - Общероссийский классификатор предприятий и организаций
        public string ReceiverOKPO { get; set; }

        // ОКАТО - Общероссийский классификатор объектов административно-территориального деления
        public string ReceiverOKATO { get; set; }

        // Юр.адрес лица
        [Required]
        public string ReceiverLegalAddress { get; set; }

        // Фактический адрес
        public string ReceiverPhysicalAddress { get; set; }

        // Номер телефона
        [Required]
        public string ReceiverPhoneNumber { get; set; }

        // добавочный номер к номеру телефона
        public string ReceiverPhoneNumberAdditional { get; set; }

        [Required]
        public string ReceiverEmail { get; set; }

        // ФИО Директора, используется если пользователь - "Директор"  еще не зарегистрирован
        [Required]
        public string ReceiverDirectorString { get; set; }

        // Id директора
        public int? ReceiverDirectorId { get; set; }

        public virtual User ReceiverDirector { get; set; }

        // ФИО ГлавногоБухгалтера, используется если пользователь - "ГлавБух"  еще не зарегистрирован
        [Required]
        public string ReceiverChiefAccountantString { get; set; }

        // Id ГлавБуха
        public int? ReceiverChiefAccountantId { get; set; }

        public virtual User ReceiverChiefAccountant { get; set; }

        #region Банковские реквизиты
        ///
        ///БАНКОВСКИЕ РЕКВИЗИТЫ
        ///

        // Р/с Расчетный счёт
        [Required]
        public string ReceiverBankCorporateAccount { get; set; }

        // Название базнка
        [Required]
        public string ReceiverBankName { get; set; }

        //Адрес банка
        public string ReceiverBankAddress { get; set; }

        // ОГРН
        [Required]
        public string ReceiverBankOGRN { get; set; }

        // ОКПО
        [Required]
        public string ReceiverBankOKPO { get; set; }

        // БИК
        [Required]
        public string ReceiverBankBIK { get; set; }

        // К/с -  Корреспондентский счет
        [Required]
        public string ReceiverBankCorrespondentAccount { get; set; }
        #endregion

        #endregion
    }
}
