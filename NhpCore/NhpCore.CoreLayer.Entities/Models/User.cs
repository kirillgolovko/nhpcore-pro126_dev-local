﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class User : IEntity, IDeletableState, IDisableableState
    {
        public int Id { get; set; }

        ////  [Index(IsUnique = true)]
        //  public int IdentityId { get; set; }

        public string PhoneNumber { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeleteTime { get; set; }
        public bool IsEnabled { get; set; }
        public DateTime? DisablingTime { get; set; }
        public int? LastVisitedOrganisationId { get; set; }
        public Organisation LastVisitedOrganisation { get; set; }


        public virtual ICollection<UserProfile> Profiles { get; set; }
        public virtual ICollection<OrganisationUserInvite> Invites { get; set; }
        public virtual ICollection<OrganisationUserEntry> Organisations { get; set; }
        public virtual ICollection<Department> MemberOfDepartments { get; set; }
        public virtual ICollection<Department> ChiefOfDepartments { get; set; }
        public virtual ICollection<UserSubscriptionEntry> Subscriptions { get; set; }
        public virtual ICollection<OrganisationDetailCard> DirectorAt { get; set; }
        public virtual ICollection<OrganisationDetailCard> AccountantAt { get; set; }


        public User()
        {
            Profiles = new HashSet<UserProfile>();
            Invites = new HashSet<OrganisationUserInvite>();
            Organisations = new HashSet<OrganisationUserEntry>();
            MemberOfDepartments = new HashSet<Department>();
            ChiefOfDepartments = new HashSet<Department>();
            Subscriptions = new HashSet<UserSubscriptionEntry>();
            DirectorAt = new HashSet<OrganisationDetailCard>();
            AccountantAt = new HashSet<OrganisationDetailCard>();
        }
    }
}