﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class ITProduct
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ITProductClient> Clients { get; set; }
        public virtual ICollection<Subscription> Subscriptions { get; set; }


        public ITProduct()
        {
            Clients = new HashSet<ITProductClient>();
            Subscriptions = new HashSet<Subscription>();
        }
    }
}