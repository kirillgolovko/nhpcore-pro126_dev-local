﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class OrganisationUserInvite : IEntity
    {
        public Guid Id { get; set; }
        public int OrganisationId { get; set; }
        public virtual Organisation Organisation { get; set; }
        public string Email { get; set; }
        public int? UserId { get; set; }
        public virtual User User { get; set; }
        public DateTime InviteSentDate { get; set; }
        public DateTime InviteSentTime { get; set; }
        public DateTime? AcceptDate { get; set; }
        public DateTime? AcceptTime { get; set; }
        public bool IsAccepted { get; set; }
        public Guid InviteCode { get; set; }
        public Guid? OrganisationUserEntryId { get; set; }
        public virtual OrganisationUserEntry OrganisationUserEntry { get; set; }
    }
}