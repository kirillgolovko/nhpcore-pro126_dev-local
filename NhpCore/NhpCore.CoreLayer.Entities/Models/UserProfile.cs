﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class UserProfile : IEntity
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<OrganisationUserEntry> OrganisationUserEntries { get; set; }

        // личные данные
        public string DisplayName { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string PatronymicName { get; set; }
        public DateTime BirthYear { get; set; }
        public DateTime BirthDate { get; set; }
        public string AboutMyself { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        // Пол ->->-> false-Женский, true-Мужской
        public bool Gender { get; set; }

        // Паспортные данные
        public string PassportSeries { get; set; }
        public string PassportNumber { get; set; }
        public string PassportSubdivisionCode { get; set; }
        public string PassportWhoAndWhenGave { get; set; }
        public string PassportRegistrationAddress { get; set; }

        // Данные для бухгалтерского учета
        public string INN { get; set; }
        public string Salary { get; set; }
        public string JobTitle { get; set; }
        public string SNILS { get; set; }

        // Контакты
        public string MobilePhone1 { get; set; }
        public string MobilePhone2 { get; set; }
        public string BusinessPhone { get; set; }
        public string Email { get; set; }
        public string Skype { get; set; }
        public string Telegram { get; set; }
        public string IsTelegram { get; set; }
        public string IsEmail { get; set; }
        public string ICQ { get; set; }
        public string Facebook { get; set; }
        public string LinkedIn { get; set; }
        public string VK { get; set; }
        public string Skills { get; set; }
        public string Interests { get; set; }


        public UserProfile()
        {
            OrganisationUserEntries = new HashSet<OrganisationUserEntry>();
        }
    }
}
