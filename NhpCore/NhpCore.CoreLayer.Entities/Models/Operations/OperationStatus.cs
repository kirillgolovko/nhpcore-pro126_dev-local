﻿using Common.EntityFramework;
using NhpCore.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    /// <summary>
    /// Статус операции
    /// </summary>
    [Table(NhpConstants.EntityTableNames.OperationStatus)]
    public class OperationStatus : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<OperationBase> Operations { get; set; }

        public OperationStatus()
        {
            Operations = new HashSet<OperationBase>();
        }
    }
}
