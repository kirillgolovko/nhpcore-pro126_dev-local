﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class OperationDocument_SubscriptionPaymentContract : OperationDocumentBase
    {
        public int? ContractDetailsId { get; set; }
        public virtual BusinessContractDetails ContractDetails { get; set; }

        [Index()]
        public int? BillId { get; set; }
    }

    [MetadataType(typeof(OperationDocument_SubscriptionPaymentContract))]
    public class OperationDocument_BusinessContract_Metadata : OperationDocumentBase_Annotation
    {
        [Required]
        public object ContractDetailsId { get; set; }
        [Required]
        public object BillId { get; set; }
    }
}
