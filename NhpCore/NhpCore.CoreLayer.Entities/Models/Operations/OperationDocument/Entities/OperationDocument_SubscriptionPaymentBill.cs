﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    [MetadataType(typeof(OperationDocument_SubscriptionPaymentBill_Annotation))]
    public class OperationDocument_SubscriptionPaymentBill : OperationDocumentBase
    {
        [Index()]
        public int? ContractId { get; set; }
        public int? BillDetailsId { get; set; }
        public virtual BusinessContractDetails BillDetails { get; set; }

        public virtual ICollection<SubscriptionPaymentBillEntry> Entries { get; set; }
        public OperationDocument_SubscriptionPaymentBill()
        {
            Entries = new HashSet<SubscriptionPaymentBillEntry>();
        }
    }
    [MetadataType(typeof(OperationDocument_SubscriptionPaymentBill))]
    public class OperationDocument_SubscriptionPaymentBill_Annotation : OperationDocumentBase_Annotation
    {
        [Required]
        public object ContractId { get; set; }
        [Required]
        public object BillDetailsId { get; set; }
    }
}
