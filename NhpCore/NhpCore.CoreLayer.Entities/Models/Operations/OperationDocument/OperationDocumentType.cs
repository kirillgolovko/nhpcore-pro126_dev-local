﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class OperationDocumentType
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<OperationDocumentBase> Documents { get; set; }

        public OperationDocumentType()
        {
            Documents = new HashSet<OperationDocumentBase>();
        }
    }
}
