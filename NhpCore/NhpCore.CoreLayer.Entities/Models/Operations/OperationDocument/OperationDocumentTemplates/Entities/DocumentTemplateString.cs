﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    [MetadataType(typeof(DocumentTemplateString_Annotation))]
    public class DocumentTemplateString : DocumentTemplateBase
    {
        public string StringTemplate { get; set; }
    }

    [MetadataType(typeof(DocumentTemplateString))]
    public class DocumentTemplateString_Annotation : DocumentTemplateBase_Annotation
    {
        [Required]
        public object StringTemplate { get; set; }
    }
}
