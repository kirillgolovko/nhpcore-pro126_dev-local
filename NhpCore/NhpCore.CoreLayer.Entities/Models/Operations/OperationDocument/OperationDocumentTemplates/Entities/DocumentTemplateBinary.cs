﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    [MetadataType(typeof(DocumentTemplateBinary_Annotation))]
    public class DocumentTemplateBinary : DocumentTemplateBase
    {
        public int? FileSize { get; set; }
        public byte[] BinaryTemplate { get; set; }
        public string FileExtension { get; set; }
    }

    [MetadataType(typeof(DocumentTemplateBinary))]
    public class DocumentTemplateBinary_Annotation : DocumentTemplateBase_Annotation
    {
        [Required]
        public object FileSize { get; set; }
        [Required]
        public object BinaryTemplate { get; set; }
        [Required]
        public object FileExtension { get; set; }
    }

}
