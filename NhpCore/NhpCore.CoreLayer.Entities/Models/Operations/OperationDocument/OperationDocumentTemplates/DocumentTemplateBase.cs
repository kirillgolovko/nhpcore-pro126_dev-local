﻿using Common.EntityFramework;
using NhpCore.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    [Table(NhpConstants.EntityTableNames.DocumentTemplateBase)]
    [MetadataType(typeof(DocumentTemplateBase_Annotation))]
    public abstract class DocumentTemplateBase : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }

    [MetadataType(typeof(DocumentTemplateBase))]
    public class DocumentTemplateBase_Annotation
    {

        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
