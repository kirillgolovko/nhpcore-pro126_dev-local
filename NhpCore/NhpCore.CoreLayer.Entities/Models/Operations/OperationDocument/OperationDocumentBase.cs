﻿using Common.EntityFramework;
using NhpCore.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    [Table(NhpConstants.EntityTableNames.OperationDocumentBase)]
    [MetadataType(typeof(OperationDocumentBase_Annotation))]
    public abstract class OperationDocumentBase : IEntity
    {
        public int Id { get; set; }
        public int DocumentTypeId { get; set; }
        public virtual OperationDocumentType DocumentType { get; set; }
        // усложнение с динамической таблицей деталей убрано. 
        //public int? OperationDocumentDetailsId { get; set; }
        //public virtual OperationDocumentDetailsBase OperationDocumentDetails { get; set; }
        public int? DocumentTemplateId { get; set; }
        public virtual DocumentTemplateBase DocumentTemplate { get; set; }
        public Guid OperationId { get; set; }
        public virtual OperationBase Operation { get; set; }
    }

    [MetadataType(typeof(OperationDocumentBase))]
    public class OperationDocumentBase_Annotation
    {
        public object Id { get; set; }

        public object DocumentTypeId { get; set; }
        //public object OperationDocumentDetailsId { get; set; }
        public object DocumentTemplateId { get; set; }
    }
}
