﻿using Common.EntityFramework;
using NhpCore.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    [Table(NhpConstants.EntityTableNames.OperationUserEntryBase)]
    [MetadataType(typeof(OperationUserEntryBase_Annotation))]
    public abstract class OperationUserEntryBase : IEntity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public Guid OperationId { get; set; }
        public virtual OperationBase Operation { get; set; }
    }

    [MetadataType(typeof(OperationUserEntryBase))]
    public class OperationUserEntryBase_Annotation
    {
        [Required]
        public object UserId { get; set; }
        [Required]
        public object OperationId { get; set; }
    }
}
