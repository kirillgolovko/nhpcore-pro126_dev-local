﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    [MetadataType(typeof(OperationUserEntry_SubscriptionPayment_Annotation))]
    public class OperationUserEntry_SubscriptionPayment : OperationUserEntryBase
    {
        // ссылка на UserSubscription
        public int? UserSubscriptionEntryId { get; set; }
        public virtual UserSubscriptionEntry UserSubscriptionEntry { get; set; }
    }

    [MetadataType(typeof(OperationUserEntry_SubscriptionPayment))]
    public class OperationUserEntry_SubscriptionPayment_Annotation : OperationUserEntryBase_Annotation
    {

    }
}
