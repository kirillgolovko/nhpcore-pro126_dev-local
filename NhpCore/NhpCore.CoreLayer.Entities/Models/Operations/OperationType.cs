﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class OperationType: IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
