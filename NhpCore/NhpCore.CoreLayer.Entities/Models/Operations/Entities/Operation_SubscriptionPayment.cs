﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    [MetadataType(typeof(Operation_SubscriptionPayment_Annotation))]
    public class Operation_SubscriptionPayment : OperationBase
    {
        public Guid? PaymentId { get; set; }
        public virtual SubscriptionPayment Payment { get; set; }

    }

    [MetadataType(typeof(Operation_SubscriptionPayment))]
    public class Operation_SubscriptionPayment_Annotation : OperationBase_Annotation
    {
        [Required]
        public object PaymentId { get; set; }
    }
}
