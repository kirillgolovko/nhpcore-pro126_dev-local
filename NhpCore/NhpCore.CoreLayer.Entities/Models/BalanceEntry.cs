﻿using System;

namespace NhpCore.CoreLayer.Entities
{
    public class BalanceEntry
    {
        public Guid Id { get; set; }
        public int BalanceId { get; set; }
        public virtual Balance Balance { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double Value { get; set; }
    }
}