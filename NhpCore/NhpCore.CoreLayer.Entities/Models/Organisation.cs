﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NhpCore.CoreLayer.Entities
{
    public class Organisation : IEntity, IDeletableState
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public DateTime RegisterDate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeleteTime { get; set; }
        public virtual Balance Balance { get; set; }
        public virtual OrganisationDetailCard Details { get; set; }
        public virtual ICollection<OrganisationUserInvite> Invites { get; set; }
        public virtual ICollection<OrganisationUserEntry> OrganisationUserEntry { get; set; }
        public virtual ICollection<SubscriptionPayment> PaidSubscriptions { get; set; }
        public virtual ICollection<Department> Departments { get; set; }

        public Organisation()
        {
            Invites = new HashSet<OrganisationUserInvite>();
            OrganisationUserEntry = new HashSet<OrganisationUserEntry>();
            PaidSubscriptions = new HashSet<SubscriptionPayment>();
            Departments = new HashSet<Department>();
        }
    }
}