﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class OrganisationDocumentsStorageAccess:IEntity
    {
        public int Id { get; set; }
        public string AccessName { get; set; }
        public StorageType StorageType { get; set; }
        public AccessType AccessType { get; set; }
        public int OrganisationId { get; set; }
        public virtual Organisation Organisation { get; set; }
        public int AssignerId { get; set; }
        public virtual User Assigner { get; set; }
        public string Token { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string RefreshToken { get; set; }
        public string ExpiresIn { get; set; }
    }
}
