﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NhpCore.CoreLayer.Entities
{
    public class OrganisationUserEntry : IEntity, IDeletableState, IDisableableState
    {
        public Guid Id { get; set; }
        public int OrganisationId { get; set; }
        public int UserId { get; set; }
        public bool? IsOwner { get; set; }
        public bool? IsPersonal { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeleteTime { get; set; }
        public DateTime? DisablingTime { get; set; }
        public int? ProfileId { get; set; }
        
        public virtual Organisation Organisation { get; set; }
        public virtual User User { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public virtual ICollection<OrganisationUserRole> UserRoles { get; set; }

        public OrganisationUserEntry()
        {
            UserRoles = new HashSet<OrganisationUserRole>();
        }
    }
}