﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    public class UserSubscriptionEntry:IEntity
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public Guid SubscriptionPaymentId { get; set; }
        public virtual SubscriptionPayment SubscriptionPayment { get; set; }
        public int SubscriptionId { get; set; }
        public virtual Subscription Subscription { get; set; }
        public DateTime? ActivateDate { get; set; }
        public bool IsRefund { get; set; }
        public DateTime? RefundDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime? ExpirationDate2 { get; set; }
    }
}
