﻿using Common.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Entities
{
    /// <summary>
    /// Позиция в выставленном счете
    /// </summary>
    public class BusinessContractBillEntry:IEntity
    {
        public int Id { get; set; }

        // наименование
        public string  Title { get; set; }

        // количество
        public int Quantity { get; set; }

        // цена
        public double Price { get; set; }

        // счет к которому относится позиция
        public int BillId { get; set; }
        public virtual BusinessContractBill Bill { get; set; }
    }
}
