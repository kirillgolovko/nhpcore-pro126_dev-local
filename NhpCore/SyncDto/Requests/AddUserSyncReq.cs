﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncDto
{
    public class AddUserSyncReq
    {
        public int CoreId { get; set; }
        public string Email { get; set; }
    }
}
