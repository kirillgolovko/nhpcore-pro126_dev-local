﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncDto
{
    public class GetUserSync
    {
        public int UserID { get; set; }
        public string Email { get; set; }
    }
}
