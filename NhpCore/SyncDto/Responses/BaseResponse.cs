﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncDto
{
    public abstract class BaseResponse
    {
        public ResponseTypes Response { get; set; }
        public string Info { get; set; }
    }
}
