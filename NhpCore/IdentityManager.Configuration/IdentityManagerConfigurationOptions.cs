﻿using Microsoft.Owin.Security.OpenIdConnect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityManager.Configuration
{
    public class IdentityManagerConfigurationOptions
    {
        public string IdentityDbConnStr { get; set; }
        public bool RequireSsl { get; set; }
    }
}
