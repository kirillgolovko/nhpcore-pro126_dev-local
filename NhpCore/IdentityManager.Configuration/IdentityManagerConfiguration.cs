﻿using Microsoft.Owin.Security.OpenIdConnect;
using NhpCore.CoreLayer.Entities;
using NhpCore.Infrastructure.Data.Ef;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;

namespace IdentityManager.Configuration
{
    public static class IdentityManagerConfiguration
    {
        public static void ConfigureIdentityManager(this IAppBuilder app, IdentityManagerConfigurationOptions options)
        {
            var idMngrFactory = new IdentityManagerServiceFactory();

            idMngrFactory.Register(new Registration<NhpIdentityDbContext>(resolver => new NhpIdentityDbContext(options.IdentityDbConnStr)));
            idMngrFactory.Register(new Registration<CustomUserStore>());
            idMngrFactory.Register(new Registration<CustomRoleStore>());
            idMngrFactory.Register(new Registration<CustomUserManager>());
            idMngrFactory.Register(new Registration<CustomRoleManager>());
            idMngrFactory.IdentityManagerService = new Registration<IIdentityManagerService, CustomIdentityManagerService>();

            var hostSecurity = new HostSecurityConfiguration()
            {
                RequireSsl = options.RequireSsl,
                HostAuthenticationType = "Cookies",
                AdditionalSignOutType = "oidc"
            };
            app.Map("/identity-manager", appManager =>
            {
                appManager.UseIdentityManager(new IdentityManagerOptions()
                {
                    Factory = idMngrFactory,
                    SecurityConfiguration = hostSecurity,
                });
            });
        }
    }
}