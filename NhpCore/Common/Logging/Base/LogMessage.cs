﻿using System;
namespace Common.Logging
{
    /// <summary>Сообщение логгера</summary>
    public class LogMessage
    {
        public LogMessageType? Type { get; set; }

        public string Message { get; set; }

        public DateTime? Date { get; set; }
    }
}
