﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.EntityFramework
{
    public interface IDeletableState
    {
        bool IsDeleted { get; set; }
        DateTime? DeleteTime { get; set; }
    }
}