﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Validation
{
    public sealed class AnnotationValidationResult
    {
        public Dictionary<string, string> Errors { get; private set; }
        public bool IsSucceded { get { return Errors.Count > 0; } }
        public AnnotationValidationResult(Dictionary<string, string> errors)
        {
            Errors = errors;
        }
    }
}
