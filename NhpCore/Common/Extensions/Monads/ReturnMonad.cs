﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Extensions.Monads
{
    public static class ReturnMonad
    {
        public static TResult Return<TInput, TResult>(this TInput source, Func<TInput, TResult> evaluator, TResult defaultValue)
            where TInput : class
        {
            return source == null ? defaultValue : evaluator(source);
        }
    }
}
