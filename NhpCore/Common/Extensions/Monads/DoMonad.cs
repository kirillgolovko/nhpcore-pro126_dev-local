﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Extensions
{
    public static class DoMonad
    {
        public static TInput Do<TInput>(this TInput source, Action<TInput> action)
            where TInput : class
        {
            if (source == null)
            {
                return null;
            }

            action(source);

            return source;
        }
    }
}
