﻿using System;

namespace Common.Commands
{
    public abstract class BaseBusinessCommandResult : IBusinessCommandResult
    {
        public virtual bool IsSuccess { get; set; }

        public virtual string ErrorMessage { get; set; }
        public int ErrorCode { get; set; }
    }
}