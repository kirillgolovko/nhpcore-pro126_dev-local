﻿namespace Common.Commands
{
    /// <summary>Результат выполнения команды</summary>
    public interface IBusinessCommandResult
    {
        /// <summary>True, если команда завешилась успешно, иначе false</summary>
        bool IsSuccess { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>

        string ErrorMessage { get; set; }

        /// <summary>
        /// Код ошибки
        /// </summary>
        int ErrorCode { get; set; }
    }
}