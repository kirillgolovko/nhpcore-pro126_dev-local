﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Utils.ServiceSynchronization
{
    public interface IServiceSynchronization
    {
        Task AddUser(int id, string email, string displayName);

        Task AddOrganisation(int id, string name, DateTime registerDate);

        Task AddUserToOrganisation(int userId, int organisationId);
    }
}
