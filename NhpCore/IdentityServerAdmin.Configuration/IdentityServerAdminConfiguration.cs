﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Owin;
using IdentityAdmin.Configuration;
using IdentityAdmin.Core;
using IdentityServerAdmin.Configuration.InMemoryService;

namespace IdentityServerAdmin.Configuration
{
    public static class IdentityServerAdminConfiguration
    {
        public static void ConfigureIdentityServerAdmin(this IAppBuilder app, IdentityServerAdminConfigurationOptions options)
        {
            var admFactory = new IdentityAdminServiceFactory();
            var securityConfig = new AdminHostSecurityConfiguration();
            securityConfig.RequireSsl = options.IsSslRequired;
            securityConfig.HostAuthenticationType = "Cookies";
            securityConfig.AdditionalSignOutType = "oidc";

#if DEBUG
            admFactory.IdentityAdminService = new Registration<IIdentityAdminService, IdentityAdminManagerService>();
#else
            admFactory.IdentityAdminService = new Registration<IIdentityAdminService, InMemoryIdentityManagerService>();
#endif


            app.Map("/identity-admin", appAdm =>
            {
                appAdm.UseIdentityAdmin(new IdentityAdminOptions
                {
                    Factory = admFactory,
                    AdminSecurityConfiguration = securityConfig,
                    
                });
            });
        }
    }
}
