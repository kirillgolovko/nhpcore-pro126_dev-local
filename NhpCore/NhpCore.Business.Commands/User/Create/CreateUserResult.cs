﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Business.Commands
{
    public class CreateUserResult : BaseBusinessCommandResult
    {
        /// <summary>
        /// Id созданного пользователя
        /// </summary>
        public int CreatedUserId { get; set; }

        /// <summary>
        /// IdentityId созданного пользователя пользователя
        /// </summary>
        public int IdentityId { get; set; }
    }
}