﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using Common.Logging;
using NhpCore.Business.Errors;
using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Services;

namespace NhpCore.Business.Commands
{
    public class CreateUserCommand : BaseBusinessCommand<CreateUserArgs, CreateUserResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _unitOfWork;
        private readonly IUserService _userService;

        private User _user;

        public CreateUserCommand(
            IUnitOfWork<NhpCoreDbContext> unitOfWork,
            IUserService userService,
            ILoggerUtility logger) : base(logger)
        {
            if (unitOfWork == null) { throw new ArgumentNullException(nameof(unitOfWork)); }
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            _unitOfWork = unitOfWork;
            _userService = userService;
        }

        protected override async Task<CreateUserResult> PerformCommand(CreateUserArgs arguments)
        {
            var result = new CreateUserResult { IsSuccess = true };
            var user = new User
            {
                //IdentityId = arguments.IdentityId,
                Id = arguments.Id,
                IsEnabled = true,
                IsDeleted = false
            };

            try
            {
                _unitOfWork.BeginTransaction();

                var userRepo = _unitOfWork.GetRepository<User>().Add(user);

                await _unitOfWork.SaveChangesAsync();

                _unitOfWork.CommitTransaction();

                result.CreatedUserId = user.Id;
                //result.IdentityId = user.IdentityId;
                result.IdentityId = user.Id;
            }
            catch (Exception)
            {
                _unitOfWork.RollBackTransaction();
                result.AddError(BusinessErrors.User.CREATING_ERROR, BusinessErrors.User.CREATING_ERROR_CODE);
            }

            return result;
        }

        protected override async Task<CreateUserResult> Validate(CreateUserArgs arguments)
        {
            var result = new CreateUserResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            //_user = await _userService.GetCoreUserByIdentityId(arguments.IdentityId);
            _user = await _userService.GetCoreUser(arguments.Id);

            if (_user != null)
            {
                result.AddError(BusinessErrors.User.USER_ALREADY_EXIST, BusinessErrors.User.USER_ALREADY_EXIST_CODE);
                return result;
            }

            return result;
        }
    }
}