﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class CreateUserProfileCommand : BaseBusinessCommand<CreateUserProfileArgs, CreateUserProfileResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IUserService _userService;

        private User _user;

        public CreateUserProfileCommand(IUnitOfWork<NhpCoreDbContext> uow,IUserService userService,ILoggerUtility logger) : base(logger)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            _uow = uow;
            _userService = userService;
        }

        protected override async Task<CreateUserProfileResult> PerformCommand(CreateUserProfileArgs arguments)
        {
            var result = new CreateUserProfileResult { IsSuccess = true };
            var _Profile = arguments.Profile;

            try
            {
                _uow.BeginTransaction();

                var repository = _uow.GetRepository<UserProfile>().Add(_Profile);

                await _uow.SaveChangesAsync();

                _uow.CommitTransaction();

                result.Id = _Profile.Id;
                result.UserId = _Profile.UserId;
            }
            catch (Exception)
            {
                _uow.RollBackTransaction();
                result.AddError(BusinessErrors.UserProfile.USERPROFILE_CREATING_ERROR, BusinessErrors.UserProfile.USERPROFILE_CREATING_ERROR_CODE);
            }

            return result;
        }

        protected override async Task<CreateUserProfileResult> Validate(CreateUserProfileArgs arguments)
        {
            var result = new CreateUserProfileResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }
            _user = await _userService.GetCoreUser(arguments.Profile.UserId);

            if (_user == null)
            {
                result.AddError(BusinessErrors.UserProfile.USER_NOT_FOUND, BusinessErrors.UserProfile.USER_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}