﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Business.Commands
{
    public class CreateUserProfileResult : BaseBusinessCommandResult
    {
        /// <summary>
        /// Id созданного профиля
        /// </summary>
        public int Id { get; set; }
        public int UserId { get; set; }
    }
}