﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Business.Commands
{
    public class DeleteUserProfileArgs : IBusinessCommandArguments
    {
        public int Id { get; set; }
        public int UserId { get; set; }
    }
}