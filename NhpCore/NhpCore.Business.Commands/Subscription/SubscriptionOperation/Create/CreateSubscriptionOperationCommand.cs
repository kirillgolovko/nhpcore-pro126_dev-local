﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;

namespace NhpCore.Business.Commands
{
    public class CreateSubscriptionOperationCommand : BaseBusinessCommand<CreateSubscriptionOperationArgs, CreateSubscriptionOperationResult>
    {
        public CreateSubscriptionOperationCommand(ILoggerUtility loggerUtility) : base(loggerUtility)
        {
        }

        protected override Task<CreateSubscriptionOperationResult> PerformCommand(CreateSubscriptionOperationArgs arguments)
        {
            throw new NotImplementedException();
        }

        protected override Task<CreateSubscriptionOperationResult> Validate(CreateSubscriptionOperationArgs arguments)
        {
            throw new NotImplementedException();
        }
    }
}