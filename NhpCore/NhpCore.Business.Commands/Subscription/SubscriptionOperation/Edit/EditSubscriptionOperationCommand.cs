﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;

namespace NhpCore.Business.Commands
{
    public class EditSubscriptionOperationCommand : BaseBusinessCommand<EditSubscriptionOperationArgs, EditSubscriptionOperationResult>
    {
        public EditSubscriptionOperationCommand(ILoggerUtility loggerUtility) : base(loggerUtility)
        {
        }

        protected override Task<EditSubscriptionOperationResult> PerformCommand(EditSubscriptionOperationArgs arguments)
        {
            throw new NotImplementedException();
        }

        protected override Task<EditSubscriptionOperationResult> Validate(EditSubscriptionOperationArgs arguments)
        {
            throw new NotImplementedException();
        }
    }
}