﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;

namespace NhpCore.Business.Commands
{
    public class DeleteSubscriptionOperationCommand : BaseBusinessCommand<DeleteSubscriptionOperationArgs, DeleteSubscriptionOperationResult>
    {
        public DeleteSubscriptionOperationCommand(ILoggerUtility loggerUtility) : base(loggerUtility)
        {
        }

        protected override Task<DeleteSubscriptionOperationResult> PerformCommand(DeleteSubscriptionOperationArgs arguments)
        {
            throw new NotImplementedException();
        }

        protected override Task<DeleteSubscriptionOperationResult> Validate(DeleteSubscriptionOperationArgs arguments)
        {
            throw new NotImplementedException();
        }
    }
}