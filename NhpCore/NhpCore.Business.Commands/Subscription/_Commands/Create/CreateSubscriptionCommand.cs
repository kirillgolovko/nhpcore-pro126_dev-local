﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;

namespace NhpCore.Business.Commands
{
    public class CreateSubscriptionCommand : BaseBusinessCommand<CreateSubscriptionArgs, CreateSubscriptionResult>
    {
        public CreateSubscriptionCommand(ILoggerUtility loggerUtility) : base(loggerUtility)
        {
        }

        protected override Task<CreateSubscriptionResult> PerformCommand(CreateSubscriptionArgs arguments)
        {
            throw new NotImplementedException();
        }

        protected override Task<CreateSubscriptionResult> Validate(CreateSubscriptionArgs arguments)
        {
            throw new NotImplementedException();
        }
    }
}