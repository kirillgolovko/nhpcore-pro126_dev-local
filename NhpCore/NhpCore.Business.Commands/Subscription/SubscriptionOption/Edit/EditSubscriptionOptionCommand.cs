﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;

namespace NhpCore.Business.Commands
{
    public class EditSubscriptionOptionCommand : BaseBusinessCommand<EditSubscriptionOptionArgs, EditSubscriptionOptionResult>
    {
        public EditSubscriptionOptionCommand(ILoggerUtility loggerUtility) : base(loggerUtility)
        {
        }

        protected override Task<EditSubscriptionOptionResult> PerformCommand(EditSubscriptionOptionArgs arguments)
        {
            throw new NotImplementedException();
        }

        protected override Task<EditSubscriptionOptionResult> Validate(EditSubscriptionOptionArgs arguments)
        {
            throw new NotImplementedException();
        }
    }
}