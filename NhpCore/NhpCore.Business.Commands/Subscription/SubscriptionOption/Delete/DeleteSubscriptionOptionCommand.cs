﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;

namespace NhpCore.Business.Commands
{
    public class DeleteSubscriptionOptionCommand : BaseBusinessCommand<DeleteSubscriptionOptionArgs, DeleteSubscriptionOptionResult>
    {
        public DeleteSubscriptionOptionCommand(ILoggerUtility loggerUtility) : base(loggerUtility)
        {
        }

        protected override Task<DeleteSubscriptionOptionResult> PerformCommand(DeleteSubscriptionOptionArgs arguments)
        {
            throw new NotImplementedException();
        }

        protected override Task<DeleteSubscriptionOptionResult> Validate(DeleteSubscriptionOptionArgs arguments)
        {
            throw new NotImplementedException();
        }
    }
}