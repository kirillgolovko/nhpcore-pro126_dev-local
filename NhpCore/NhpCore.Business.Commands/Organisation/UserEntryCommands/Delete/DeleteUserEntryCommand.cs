﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using NhpCore.Business.Errors;
using NhpCore.Infrastructure.Data.Ef;
using Common.DataAccess.UnitOfWork;
using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Services;

namespace NhpCore.Business.Commands
{
    public class DeleteUserEntryCommand : BaseBusinessCommand<DeleteUserEntryArgs, DeleteUserEntryResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _coreUoW;
        private readonly IUserService _userService;

        private OrganisationUserEntry _userEntry;

        public DeleteUserEntryCommand(
            IUnitOfWork<NhpCoreDbContext> coreUoW,
            IUserService userService,
            ILoggerUtility loggerUtility) : base(loggerUtility)
        {
            if (coreUoW == null) { throw new ArgumentNullException(nameof(coreUoW)); }
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }

            _coreUoW = coreUoW;
            _userService = userService;
        }

        protected override async Task<DeleteUserEntryResult> PerformCommand(DeleteUserEntryArgs arguments)
        {
            var result = new DeleteUserEntryResult { IsSuccess = true };

            _coreUoW.GetRepository<OrganisationUserEntry>().Remove(_userEntry);
            await _coreUoW.SaveChangesAsync();

            return result;
        }

        protected override async Task<DeleteUserEntryResult> Validate(DeleteUserEntryArgs arguments)
        {
            var result = new DeleteUserEntryResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            _userEntry = await _userService.GetUserEntry(arguments.Id);

            if (_userEntry == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_USER_ENTRY_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_USER_ENTRY_NOT_FOUND_CODE);
            }

            return result;
        }
    }
}
