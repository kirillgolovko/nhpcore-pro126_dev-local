﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DataAccess.UnitOfWork;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Infrastructure.Data.Ef;
using Common.Logging;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class CreateUserEntryCommand : BaseBusinessCommand<CreateUserEntryArgs, CreateUserEntryResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _unitOfWork;
        private readonly IUserService _userService;
        private readonly IOrganisationService _organisationService;


        private User _user;
        private Organisation _organisation;
        private string _displayName;

        public CreateUserEntryCommand(IUnitOfWork<NhpCoreDbContext> unitOfWork, IUserService userService, IOrganisationService organisationService, ILoggerUtility logger) : base(logger)
        {
            if (unitOfWork == null) { throw new ArgumentNullException(nameof(unitOfWork)); }
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }

            _unitOfWork = unitOfWork;
            _userService = userService;
            _organisationService = organisationService;
        }

        protected override async Task<CreateUserEntryResult> PerformCommand(CreateUserEntryArgs arguments)
        {
            var result = new CreateUserEntryResult { IsSuccess = true };

            OrganisationUserEntry entry = new OrganisationUserEntry
            {
                OrganisationId = _organisation.Id,
                UserId = _user.Id,
                IsEnabled = true,
                UserProfile = new UserProfile { DisplayName = _displayName },

            };
            entry.IsPersonal = arguments.IsPersonal.HasValue == true ? arguments.IsPersonal.Value : new bool?();
            entry.IsOwner = arguments.IsOwner.HasValue == true ? arguments.IsOwner.Value : new bool?();

            var repository = _unitOfWork.GetRepository<OrganisationUserEntry>().Add(entry);

            this.LoggerUtility.Trace($"{nameof(CreateUserEntryCommand)} SaveChangesAsync started");

            await _unitOfWork.SaveChangesAsync();

            this.LoggerUtility.Trace($"{nameof(CreateUserEntryCommand)} SaveChangesAsync finished");

            result.CreatedUserEntryId = entry.Id;
            return result;
        }

        protected override async Task<CreateUserEntryResult> Validate(CreateUserEntryArgs arguments)
        {
            var result = new CreateUserEntryResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            this.LoggerUtility.Debug($"{nameof(CreateUserEntryArgs)}:{nameof(arguments.IdentityId)} = {arguments.IdentityId}");
            this.LoggerUtility.Debug($"{nameof(CreateUserEntryArgs)}:{nameof(arguments.OrganisationId)} = {arguments.OrganisationId}");
            this.LoggerUtility.Debug($"{nameof(CreateUserEntryArgs)}:{nameof(arguments.IsOwner)} = {arguments.IsOwner}");
            this.LoggerUtility.Debug($"{nameof(CreateUserEntryArgs)}:{nameof(arguments.IsPersonal)} = {arguments.IsPersonal}");

            // _user = await _userService.GetCoreUserByIdentityId(arguments.IdentityId);
            _user = await _userService.GetCoreUser(arguments.IdentityId);
            _organisation = await _organisationService.GetOrganisation(arguments.OrganisationId);
            _displayName = await _userService.GetDisplayName(arguments.IdentityId);

            if (_user == null)
            {
                result.AddError(BusinessErrors.User.USER_NOT_FOUND, BusinessErrors.User.USER_NOT_FOUND_CODE);
                return result;
            }

            if (_organisation == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_NOT_FOUND_CODE);
                return result;
            }

            if (_displayName == string.Empty)
            {
                result.AddError(BusinessErrors.User.CLAIM_DISPLAYNAME_NOT_FOUND, BusinessErrors.User.CLAIM_DISPLAYNAME_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}