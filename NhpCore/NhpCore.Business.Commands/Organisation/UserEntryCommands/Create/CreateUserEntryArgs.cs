﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Business.Commands
{
    public class CreateUserEntryArgs : IBusinessCommandArguments
    {
        /// <summary>
        /// IdentityId пользователя для которого создается запись в организации
        /// </summary>
        public int IdentityId { get; set; }

        /// <summary>
        /// Id организации в которой создается запись о пользователе
        /// </summary>
        public int OrganisationId { get; set; }

        public bool? IsPersonal { get; set; }
        public bool? IsOwner { get; set; }
    }
}