﻿using Common.Commands;
using System;
using System.Threading.Tasks;
using Common.DataAccess.UnitOfWork;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Infrastructure.Data.Ef;
using Common.Logging;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class CreateUserRoleCommand : BaseBusinessCommand<CreateUserRoleArgs, CreateUserRoleResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _unitOfWork;
        private readonly IUserService _userService;
        private OrganisationUserEntry _userEntry;

        public CreateUserRoleCommand(IUnitOfWork<NhpCoreDbContext> unitOfWork, IUserService userService, ILoggerUtility logger) : base(logger)
        {
            if (unitOfWork == null) { throw new ArgumentNullException(nameof(unitOfWork)); }
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }

            _unitOfWork = unitOfWork;
            _userService = userService;
        }

        protected override async Task<CreateUserRoleResult> PerformCommand(CreateUserRoleArgs arguments)
        {
            var result = new CreateUserRoleResult { IsSuccess = true };

            OrganisationUserRole userRole = new OrganisationUserRole
            {
                OrganisationUserEntryId = arguments.OrganisationUserEntryId,
                OrganisationUserRoleTypeId = arguments.UserRoleTypeId
            };

            var repo = _unitOfWork.GetRepository<OrganisationUserRole>();
            repo.Add(userRole);

            await _unitOfWork.SaveChangesAsync();

            result.CretedUserRoleId = userRole.Id;
            return result;
        }

        protected override async Task<CreateUserRoleResult> Validate(CreateUserRoleArgs arguments)
        {
            var result = new CreateUserRoleResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            _userEntry = await _userService.GetUserEntry(arguments.OrganisationUserEntryId);

            if (_userEntry == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_USER_ENTRY_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_USER_ENTRY_NOT_FOUND_CODE);
                return result;
            }
            return result;
        }
    }
}