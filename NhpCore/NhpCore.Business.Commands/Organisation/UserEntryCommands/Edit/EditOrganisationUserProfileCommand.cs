﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    //public class EditOrganisationUserProfileCommand : BaseBusinessCommand<EditOrganisationUserProfileArgs, EditOrganisationUserProfileResult>
    //{
    //    private readonly IUnitOfWork<NhpCoreDbContext> _uow;
    //    private readonly IUserService _userService;

    //    private OrganisationUserProfile _profile;

    //    public EditOrganisationUserProfileCommand(
    //        IUnitOfWork<NhpCoreDbContext> uow,
    //        IUserService userService,
    //        ILoggerUtility loggerUtility) : base(loggerUtility)
    //    {
    //        if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
    //        if (userService == null) { throw new ArgumentNullException(nameof(userService)); }

    //        _uow = uow;
    //        _userService = userService;
    //    }

    //    protected override async Task<EditOrganisationUserProfileResult> PerformCommand(EditOrganisationUserProfileArgs arguments)
    //    {
    //        var result = new EditOrganisationUserProfileResult { IsSuccess = true };

    //        _profile = arguments.Profile;

    //        await _uow.SaveChangesAsync();

    //        return result;
    //    }

    //    protected override async Task<EditOrganisationUserProfileResult> Validate(EditOrganisationUserProfileArgs arguments)
    //    {
    //        var result = new EditOrganisationUserProfileResult { IsSuccess = true };

    //        if (arguments == null)
    //        {
    //            result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
    //            return result;
    //        }

    //        _profile = await _userService.GetOrganisationUserProfile(arguments.Profile.Id);

    //        if (_profile == null)
    //        {
    //            result.AddError(BusinessErrors.Organisation.ORGANISATION_USERPROFILE_NOT_FOUND,
    //                BusinessErrors.Organisation.ORGANISATION_USERPROFILE_NOT_FOUND_CODE);
    //            return result;
    //        }


    //        return result;
    //    }
    //}
}
