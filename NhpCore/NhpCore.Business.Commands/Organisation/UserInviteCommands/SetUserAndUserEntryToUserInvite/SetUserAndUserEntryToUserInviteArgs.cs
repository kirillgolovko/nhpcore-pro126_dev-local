﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Business.Commands
{
    public class SetUserAndUserEntryToUserInviteArgs : IBusinessCommandArguments
    {
        /// <summary>
        /// Id инвайта
        /// </summary>
        public Guid InviteId { get; set; }

        /// <summary>
        /// Id записи для пользователя созданной в организации
        /// </summary>
        public Guid UserEntryId { get; set; }
    }
}
