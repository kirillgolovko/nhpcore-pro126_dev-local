﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using NhpCore.Business.Errors;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.Business.Commands
{
    public class SetUserAndUserEntryToUserInviteCommand : BaseBusinessCommand<SetUserAndUserEntryToUserInviteArgs, SetUserAndUserEntryToUserInviteResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _coreUoW;
        private readonly IUserService _userService;


        private OrganisationUserEntry _userEntry;
        private OrganisationUserInvite _userInvite;

        public SetUserAndUserEntryToUserInviteCommand(
            IUserService userService,
            IUnitOfWork<NhpCoreDbContext> coreUoW,
            ILoggerUtility loggerUtility) : base(loggerUtility)
        {
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }
            if (coreUoW == null) { throw new ArgumentNullException(nameof(coreUoW)); }

            _userService = userService;
            _coreUoW = coreUoW;
        }


        protected override async Task<SetUserAndUserEntryToUserInviteResult> PerformCommand(SetUserAndUserEntryToUserInviteArgs arguments)
        {
            var result = new SetUserAndUserEntryToUserInviteResult { IsSuccess = true };

            _userInvite.UserId = _userEntry.UserId;
            _userInvite.OrganisationUserEntryId = _userEntry.Id;

            await _coreUoW.SaveChangesAsync();

            result.OrganisationUserEntryId = _userEntry.Id;

            return result;
        }

        protected override async Task<SetUserAndUserEntryToUserInviteResult> Validate(SetUserAndUserEntryToUserInviteArgs arguments)
        {
            var result = new SetUserAndUserEntryToUserInviteResult { IsSuccess = true };

            if (result == null)
            {
                return result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
            }

            _userEntry = await _userService.GetUserEntry(arguments.UserEntryId);
            _userInvite = await _userService.GetUserInvite(arguments.InviteId);


            if (_userEntry == null)
            {
                return result.AddError(BusinessErrors.Organisation.ORGANISATION_USER_ENTRY_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_USER_ENTRY_NOT_FOUND_CODE);
            }
            if (_userInvite == null)
            {
                return result.AddError(BusinessErrors.Organisation.ORGANISATION_USERINVITE_ENTRY_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_USERINVITE_ENTRY_NOT_FOUND_CODE);
            }

            return result;
        }
    }
}
