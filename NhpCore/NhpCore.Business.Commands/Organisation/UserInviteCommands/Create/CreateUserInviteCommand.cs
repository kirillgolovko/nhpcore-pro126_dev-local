﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using NhpCore.CoreLayer.Services;

namespace NhpCore.Business.Commands
{
    public class CreateUserInviteCommand : BaseBusinessCommand<CreateUserInviteArgs, CreateUserInviteResult>
    {
        private readonly IOrganisationService _organisationService;

        public CreateUserInviteCommand(IOrganisationService organisationService, ILoggerUtility loggerUtility) : base(loggerUtility)
        {
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }

            _organisationService = organisationService;
        }

        protected override Task<CreateUserInviteResult> PerformCommand(CreateUserInviteArgs arguments)
        {
            return null;
        }

        protected override Task<CreateUserInviteResult> Validate(CreateUserInviteArgs arguments)
        {
            return null;
        }
    }
}
