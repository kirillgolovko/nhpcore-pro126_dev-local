﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using NhpCore.Business.Errors;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Infrastructure.Data.Ef;
using Common.DataAccess.UnitOfWork;

namespace NhpCore.Business.Commands
{
    public class AcceptUserInviteCommand : BaseBusinessCommand<AcceptUserInviteArgs, AcceptUserInviteResult>
    {
        private readonly IUserService _userService;
        private readonly IUnitOfWork<NhpCoreDbContext> _coreUoW;


        private OrganisationUserInvite _invite;

        public AcceptUserInviteCommand(
            IUnitOfWork<NhpCoreDbContext> coreUoW,
            IUserService userService,
            ILoggerUtility loggerUtility) : base(loggerUtility)
        {
            if (coreUoW == null) { throw new ArgumentNullException(nameof(coreUoW)); }
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }

            _coreUoW = coreUoW;
            _userService = userService;
        }

        protected override async Task<AcceptUserInviteResult> PerformCommand(AcceptUserInviteArgs arguments)
        {
            var result = new AcceptUserInviteResult { IsSuccess = true };
            var dtNow = DateTime.Now;

            _invite.AcceptDate = dtNow.Date;
            _invite.AcceptTime = dtNow;
            _invite.IsAccepted = true;
            var repository = _coreUoW.GetRepository<OrganisationUserInvite>().Update(_invite);

            await _coreUoW.SaveChangesAsync();

            result.UserInviteEntity = _invite;
            return result;
        }

        protected override async Task<AcceptUserInviteResult> Validate(AcceptUserInviteArgs arguments)
        {
            var result = new AcceptUserInviteResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
            }

            _invite = await _userService.GetUserInviteByInviteCode(arguments.InviteCode);

            if (_invite == null)
            {
                result.AddError(BusinessErrors.User.USERINVITE_NOT_FOUND, BusinessErrors.User.USERINVITE_NOT_FOUND_CODE);
            }

            return result;
        }
    }
}
