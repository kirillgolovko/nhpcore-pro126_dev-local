﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;
using NhpCore.Infrastructure.Data.Ef;
using Common.DataAccess.UnitOfWork;

namespace NhpCore.Business.Commands
{
    public class DeleteUserInviteCommand : BaseBusinessCommand<DeleteUserInviteArgs, DeleteUserInviteResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _coreUoW;
        private readonly IOrganisationService _organisationService;
        private OrganisationUserInvite _invite;

        public DeleteUserInviteCommand(IUnitOfWork<NhpCoreDbContext> coreUoW, IOrganisationService organisationService, ILoggerUtility loggerUtility) : base(loggerUtility)
        {
            if (coreUoW == null) { throw new ArgumentNullException(nameof(coreUoW)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }

            _coreUoW = coreUoW;
            _organisationService = organisationService;
        }

        protected override async Task<DeleteUserInviteResult> PerformCommand(DeleteUserInviteArgs arguments)
        {
            var result = new DeleteUserInviteResult { IsSuccess = true };

            _coreUoW.GetRepository<OrganisationUserInvite>()
                .Remove(_invite);

            await _coreUoW.SaveChangesAsync();

            result.DeletedId = _invite.Id;
            return result;
        }

        protected override async Task<DeleteUserInviteResult> Validate(DeleteUserInviteArgs arguments)
        {
            var result = new DeleteUserInviteResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            _invite = await _organisationService.GetInvitedUser(arguments.Id);

            if (_invite == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_USERINVITE_ENTRY_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_USERINVITE_ENTRY_NOT_FOUND_CODE);
            }

            return result;
        }
    }
}
