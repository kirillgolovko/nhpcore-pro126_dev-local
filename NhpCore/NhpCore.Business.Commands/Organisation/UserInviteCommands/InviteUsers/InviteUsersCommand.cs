﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.DataAccess.UnitOfWork;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.Infrastructure.Utils.MessageSender;
using System.Text.RegularExpressions;
using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Services;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class InviteUsersCommand : BaseBusinessCommand<InviteUsersArgs, InviteUsersResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IOrganisationService _organisationService;
        private readonly IMessageSender _sender;

        private Organisation _inviteOrganisation;


        public InviteUsersCommand(IUnitOfWork<NhpCoreDbContext> uow, IOrganisationService organisationService, IMessageSender sender, ILoggerUtility loggerUtility) : base(loggerUtility)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }
            if (sender == null) { throw new ArgumentNullException(nameof(sender)); }


            _uow = uow;
            _organisationService = organisationService;
            _sender = sender;
        }

        protected override async Task<InviteUsersResult> PerformCommand(InviteUsersArgs arguments)
        {
            var result = new InviteUsersResult { IsSuccess = true };
            var inviteStructure = new List<Tuple<string, Guid>>();

            // добавляем имейлы в базу
            arguments.Emails.ForEach(mail =>
            {
                var guid = Guid.NewGuid();
                var dtNow = DateTime.Now;

                inviteStructure.Add(new Tuple<string, Guid>(mail, guid));

                _uow.GetRepository<OrganisationUserInvite>()
                    .Add(new OrganisationUserInvite
                    {
                        Email = mail,
                        InviteCode = guid,
                        InviteSentDate = dtNow.Date,
                        InviteSentTime = dtNow,
                        IsAccepted = false,
                        OrganisationId = _inviteOrganisation.Id,
                    });
            });

            var saveChangesTask = _uow.SaveChangesAsync();

            var taskList = new List<Task>();

            //делаем рассылку
            inviteStructure.ForEach(tuple =>
            {
                var isExistedEmail = arguments.ExistedEmails.ContainsKey(tuple.Item1);
                var urlPath = isExistedEmail == true ? arguments.AcceptInviteUrl : arguments.RegistrationUrl;


                var messageUrl = $"{urlPath}?inviteCode={tuple.Item2}";

                if (isExistedEmail)
                {
                    var id = arguments.ExistedEmails[tuple.Item1];
                    messageUrl += $"&identityId={id}";
                }

                var messageBody = "Для принятия приглашения - щелкните <a href=" + messageUrl + ">здесь</a>";

                var message = new Message
                {
                    Destination = tuple.Item1,
                    Subject = $"Приглашение присоединиться к компании {_inviteOrganisation.Name} в сервисе НХП Онлайн",
                    Body = messageBody,
                };
                taskList.Add(_sender.SendAsync(message));
            });

            // TODO: ждем окончания рассылок. Определиться нужно ли? их можно возвращать в списке обратно со статусом рассылки 
            var senderTask = Task.WhenAll(taskList.ToArray());
            await Task.WhenAll(senderTask, saveChangesTask);

            var failed = new List<string>();
            taskList.ForEach(x =>
            {
                if (x.Status == TaskStatus.Faulted)
                {
                    failed.Add(((Message)x.AsyncState).Destination);
                }
            });

            result.FailedEmails = failed;

            return result;
        }

        // TODO: Сделать нормальную валидацию мэйлов
        protected override async Task<InviteUsersResult> Validate(InviteUsersArgs arguments)
        {
            var result = new InviteUsersResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            _inviteOrganisation = await _organisationService.GetOrganisation(arguments.OrganisationId);

            if (_inviteOrganisation == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_NOT_FOUND_CODE);
            }

            return result;
        }
    }
}
