﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DataAccess.UnitOfWork;
using Common.Logging;

namespace NhpCore.Business.Commands
{
    public class EditOrganisationCommand : BaseBusinessCommand<EditOrganisationArgs, EditOrganisationResult>
    {
        public EditOrganisationCommand(ILoggerUtility logger) : base(logger)
        {
        }

        protected override Task<EditOrganisationResult> PerformCommand(EditOrganisationArgs arguments)
        {
            throw new NotImplementedException();
        }

        protected override Task<EditOrganisationResult> Validate(EditOrganisationArgs arguments)
        {
            throw new NotImplementedException();
        }
    }
}