﻿using Common.Commands;
using Common.DataAccess.UnitOfWork;
using Common.Logging;
using NhpCore.CoreLayer.Services;
using NhpCore.Infrastructure.Data.Ef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Business.Commands
{
    public class RegisterOrganisationCommand : BaseBusinessCommand<RegisterOrganisationArgs, RegisterOrganisationResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _unitOfWork;
        private readonly IOrganisationService _organisationService;

        public RegisterOrganisationCommand(IUnitOfWork<NhpCoreDbContext> unitOfWork, IOrganisationService organisationService, ILoggerUtility logger)
            : base(logger)
        {
            if (unitOfWork==null) { throw new ArgumentNullException(nameof(unitOfWork)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }


            _unitOfWork = unitOfWork;
            _organisationService = organisationService;
        }

        protected override Task<RegisterOrganisationResult> PerformCommand(RegisterOrganisationArgs arguments)
        {
            throw new NotImplementedException();
        }

        protected override Task<RegisterOrganisationResult> Validate(RegisterOrganisationArgs arguments)
        {
            throw new NotImplementedException();
        }
    }
}