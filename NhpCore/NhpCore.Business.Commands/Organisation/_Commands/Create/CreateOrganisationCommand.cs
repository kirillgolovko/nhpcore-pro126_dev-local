﻿using Common.Commands;
using System;
using System.Threading.Tasks;
using Common.DataAccess.UnitOfWork;
using NhpCore.CoreLayer.Entities;
using NhpCore.CoreLayer.Services;
using NhpCore.Infrastructure.Data.Ef;
using Common.Logging;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class CreateOrganisationCommand : BaseBusinessCommand<CreateOrganisationArgs, CreateOrganisationResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _unitOfWork;
        private IUserService _userService;

        //private User _creator;

        private const string _emptyDetailString = "none";

        public CreateOrganisationCommand(IUnitOfWork<NhpCoreDbContext> uow, IUserService userService, ILoggerUtility logger) : base(logger)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (userService == null) { throw new ArgumentNullException(nameof(userService)); }


            _unitOfWork = uow;
            _userService = userService;
        }

        protected override async Task<CreateOrganisationResult> PerformCommand(CreateOrganisationArgs arguments)
        {
            var result = new CreateOrganisationResult { IsSuccess = true };
            var dtNow = DateTime.Now;

            var newOrganisation = new Organisation
            {
                Name = arguments.ShortName,
                RegisterDate = dtNow,
                Balance = new Balance { Value = 0 },
                Details = GetEmptyOrganisationDetailCard(arguments.ShortName)
            };

            var repository = _unitOfWork.GetRepository<Organisation>().Add(newOrganisation);

            await _unitOfWork.SaveChangesAsync();

            result.OrganisationId = newOrganisation.Id;
            result.RegistrationDate = dtNow;
            result.Name = arguments.ShortName;

            return result;
        }

        private OrganisationDetailCard GetEmptyOrganisationDetailCard(string shortName)
        {
            return new OrganisationDetailCard
            {
                ShortName = shortName,
                INN = _emptyDetailString,
                KPP = _emptyDetailString,
                LegalAddress = _emptyDetailString,
                PhoneNumber = _emptyDetailString,
                PhoneNumberAdditional = _emptyDetailString,
                Email = _emptyDetailString,
                DirectorString = _emptyDetailString,
                ChiefAccountantString = _emptyDetailString,
                OGRN = _emptyDetailString,
                OKPO = _emptyDetailString,


                BankName = _emptyDetailString,
                BankBIK = _emptyDetailString,
                BankCorporateAccount = _emptyDetailString,
                BankCorrespondentAccount = _emptyDetailString,
                BankOGRN = _emptyDetailString,
                BankOKPO = _emptyDetailString
            };
        }

        protected override async Task<CreateOrganisationResult> Validate(CreateOrganisationArgs arguments)
        {
            CreateOrganisationResult result = new CreateOrganisationResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            //var _creator = await _userService.GetCoreUserByIdentityId(arguments.CreatorId);

            //TODO:: БАААААГ
            var _creator = await _userService.GetCoreUser(arguments.CreatorId);

            if (_creator == null)
            {
                result.AddError(BusinessErrors.User.USER_NOT_FOUND, BusinessErrors.User.USER_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}