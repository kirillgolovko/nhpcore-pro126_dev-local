﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Business.Commands
{
    public class CreateOrganisationResult : BaseBusinessCommandResult
    {
        public int OrganisationId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string Name { get; set; }
    }
}