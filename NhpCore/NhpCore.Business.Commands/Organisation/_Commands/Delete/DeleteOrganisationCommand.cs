﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DataAccess.UnitOfWork;
using Common.Logging;
using NhpCore.Infrastructure.Data.Ef;
using NhpCore.CoreLayer.Services;
using NhpCore.CoreLayer.Entities;
using NhpCore.Business.Errors;

namespace NhpCore.Business.Commands
{
    public class DeleteOrganisationCommand : BaseBusinessCommand<DeleteOrganisationArgs, DeleteOrganisationResult>
    {
        private readonly IOrganisationService _organisationService;
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;

        private Organisation _organisation;

        public DeleteOrganisationCommand(
            IUnitOfWork<NhpCoreDbContext> uow,
            IOrganisationService organisationService,
            ILoggerUtility logger) : base(logger)
        {
            if (uow == null) { throw new ArgumentNullException(nameof(uow)); }
            if (organisationService == null) { throw new ArgumentNullException(nameof(organisationService)); }

            _uow = uow;
            _organisationService = organisationService;
        }

        protected override async Task<DeleteOrganisationResult> PerformCommand(DeleteOrganisationArgs arguments)
        {
            var result = new DeleteOrganisationResult { IsSuccess = true };

            _uow.GetRepository<Organisation>().Remove(_organisation);
            await _uow.SaveChangesAsync();

            return result;
        }

        protected override async Task<DeleteOrganisationResult> Validate(DeleteOrganisationArgs arguments)
        {
            var result = new DeleteOrganisationResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            _organisation = await _organisationService.GetOrganisation(arguments.OrganisationId);

            if (_organisation == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}