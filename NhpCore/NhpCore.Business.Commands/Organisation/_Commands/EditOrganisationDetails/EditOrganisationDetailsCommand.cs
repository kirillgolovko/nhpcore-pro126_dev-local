﻿using Common.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;

namespace NhpCore.Business.Commands
{
    public class EditOrganisationDetailsCommand : BaseBusinessCommand<EditOrganisationDetailsArgs, EditOrganisationDetailsResult>
    {
        public EditOrganisationDetailsCommand(ILoggerUtility loggerUtility) : base(loggerUtility)
        {
        }

        protected override Task<EditOrganisationDetailsResult> PerformCommand(EditOrganisationDetailsArgs arguments)
        {
            throw new NotImplementedException();
        }

        protected override Task<EditOrganisationDetailsResult> Validate(EditOrganisationDetailsArgs arguments)
        {
            throw new NotImplementedException();
        }
    }
}
