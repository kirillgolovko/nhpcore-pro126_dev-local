﻿using Common.Commands;
using Common.DataAccess.UnitOfWork;
using Common.Logging;
using NhpCore.Business.Errors;
using NhpCore.CoreLayer.Services;
using NhpCore.Infrastructure.Data.Ef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.Business.Commands
{
    public class AddStorageCredentialsCommand : BaseBusinessCommand<AddStorageCredentialsArgs, AddStorageCredentialsResult>
    {
        private readonly IUnitOfWork<NhpCoreDbContext> _uow;
        private readonly IOrganisationService _organisationService;
        private readonly IUserService _userService;

        private Organisation _organisation;
        private User _user;

        public AddStorageCredentialsCommand(IUnitOfWork<NhpCoreDbContext> uow,
            IOrganisationService organisationService,
            IUserService userService,
            ILoggerUtility logger)
            : base(logger)
        {
            if (uow == null) throw new ArgumentNullException(nameof(uow));
            if (organisationService == null) throw new ArgumentNullException(nameof(organisationService));
            if (userService == null) throw new ArgumentNullException(nameof(userService));


            _uow = uow;
            _organisationService = organisationService;
            _userService = userService;
        }

        protected override async Task<AddStorageCredentialsResult> PerformCommand(AddStorageCredentialsArgs arguments)
        {
            var result = new AddStorageCredentialsResult { IsSuccess = true };

            var newCredential = new OrganisationDocumentsStorageAccess
            {
                AccessType = AccessType.Token,
                OrganisationId = _organisation.Id,
                AssignerId = _user.Id,
                StorageType = arguments.StorageType
            };
            if (arguments.AccessType == AccessType.Token)
            {
                newCredential.Token = arguments.Token;
                newCredential.RefreshToken = arguments.RefreshToken;
                newCredential.ExpiresIn = arguments.ExpiresIn;
            }
            else if (arguments.AccessType == AccessType.LoginPassword)
            {
                newCredential.Email = arguments.Email;
                newCredential.Password = arguments.Password;
            }

            var repo = _uow.GetRepository<OrganisationDocumentsStorageAccess>().Add(newCredential);
            try
            {
                await _uow.SaveChangesAsync();
            }
            catch (Exception)
            {

                throw;
            }
            

            result.CredentialId = newCredential.Id;

            return result;
        }

        protected override async Task<AddStorageCredentialsResult> Validate(AddStorageCredentialsArgs arguments)
        {
            var result = new AddStorageCredentialsResult { IsSuccess = true };

            if (arguments == null)
            {
                result.AddError(BusinessErrors.System.INVALID_REQUEST, BusinessErrors.System.INVALID_REQUEST_CODE);
                return result;
            }

            _organisation = await _organisationService.GetOrganisation(arguments.OrganisationId);
            if (_organisation == null)
            {
                result.AddError(BusinessErrors.Organisation.ORGANISATION_NOT_FOUND, BusinessErrors.Organisation.ORGANISATION_NOT_FOUND_CODE);
                return result;
            }

            _user = await _userService.GetCoreUser(arguments.UserId);
            if (_user == null)
            {
                result.AddError(BusinessErrors.User.USER_NOT_FOUND, BusinessErrors.User.USER_NOT_FOUND_CODE);
                return result;
            }

            return result;
        }
    }
}
