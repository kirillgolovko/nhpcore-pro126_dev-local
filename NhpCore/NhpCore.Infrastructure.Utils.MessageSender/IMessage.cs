﻿namespace NhpCore.Infrastructure.Utils.MessageSender
{
    public interface IMessage
    {
        // Summary:
        //     Destination, i.e. To email, or SMS phone number
        string Destination { get; set; }
        //
        // Summary:
        //     Subject
        string Subject { get; set; }
        //
        // Summary:
        //     Message contents
        string Body { get; set; }
    }
}