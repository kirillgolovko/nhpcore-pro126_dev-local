﻿using NhpCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Utils.MessageSender
{
    public class EmailMessageSender : IMessageSender
    {
        public Task SendAsync(IMessage message)
        {
            MailAddress from = new MailAddress(NhpConstants.EmailServiceConstants.SenderDisplayedEmail, NhpConstants.EmailServiceConstants.SenderDisplayedName);
            MailAddress to = new MailAddress(message.Destination);
            MailMessage mail = new MailMessage(from, to);
            mail.Subject = message.Subject;
            mail.Body = message.Body;
            mail.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient(NhpConstants.EmailServiceConstants.SmtpHost, NhpConstants.EmailServiceConstants.SmtpPort);

            smtp.Credentials = new NetworkCredential(NhpConstants.EmailServiceConstants.SmtpEmail, NhpConstants.EmailServiceConstants.SmtpPassword);
          //  smtp.EnableSsl = true;

            try
            {
                return smtp.SendMailAsync(mail);
            }
            catch (Exception ex)
            {
                return Task.FromResult(ex);
            }
        }
    }
}
