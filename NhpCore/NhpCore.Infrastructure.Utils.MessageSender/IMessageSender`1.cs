﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Utils.MessageSender
{
    public interface IMessageSender<T>:IMessageSender where T: class
    {
    }
}
