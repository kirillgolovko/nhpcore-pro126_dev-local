﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;
using Microsoft.AspNet.Identity;
using NhpCore.Infrastructure.Data.Ef;

namespace NhpCore.CoreLayer.Entities
{
    public static class CustomUserExtension
    {
        public static async Task<ClaimsIdentity> GenerateUserIdentityAsync(this CustomUser user, CustomUserManager manager/*, string authenticationType*/)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте настраиваемые утверждения пользователя
            return userIdentity;
        }
    }
}