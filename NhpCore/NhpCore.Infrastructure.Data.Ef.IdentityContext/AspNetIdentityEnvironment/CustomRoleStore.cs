﻿using Microsoft.AspNet.Identity.EntityFramework;
using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Infrastructure.Data.Ef
{
    public class CustomRoleStore : RoleStore<CustomRole, int, CustomUserRole>
    {
        public CustomRoleStore(NhpIdentityDbContext ctx)
            : base(ctx)
        {
        }
    }
}
