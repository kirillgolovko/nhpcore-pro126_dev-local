//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ERDiagrams
{
    using System;
    using System.Collections.Generic;
    
    public partial class DocumentTemplate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DocumentTemplate()
        {
            this.OperationDocuments = new HashSet<OperationDocument>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<int> FileSize { get; set; }
        public byte[] BinaryTemplate { get; set; }
        public string FileExtension { get; set; }
        public string StringTemplate { get; set; }
        public string Discriminator { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OperationDocument> OperationDocuments { get; set; }
    }
}
