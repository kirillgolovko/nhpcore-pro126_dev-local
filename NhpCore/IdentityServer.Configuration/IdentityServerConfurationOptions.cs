﻿using IdentityServer3.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityServer.Configuration
{
    public class IdentityServerConfurationOptions
    {
        public string CertificatePath { get; set; }
        public string CertificateName { get; set; }
        public string CertificatePassword { get; set; }
        public string IdentityDbConnStr { get; set; }
        public string IdSrvDbConnStr { get; set; }
        public string SiteName { get; set; }
        public bool RequireSsl { get; set; }
        public Action<IdentityServerServiceFactory> SetCustomViews { get; set; }
        public bool IsDebugWithInMemoryManagers { get; set; }
    }
}
