﻿using IdentityServer3.Core;
using IdentityServer3.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreApp.IdentityServer
{
    public class Scopes
    {
        public static IEnumerable<Scope> Get()
        {
            var result = new List<Scope>();
            result.Add(new Scope
            {
                Name = "read",
                DisplayName = "Read data",
                Type = ScopeType.Resource,
                Emphasize = false,

                ScopeSecrets = new List<Secret>
                        {
                            new Secret("secret".Sha256())
                        }
            });
            result.Add(new Scope
            {
                Name = "write",
                DisplayName = "Write data",
                Type = ScopeType.Resource,
                Emphasize = true,

                ScopeSecrets = new List<Secret>
                        {
                            new Secret("secret".Sha256())
                        }
            });
            result.Add(new Scope
            {
                Name = "idmgr",
                DisplayName = "IdentityManager",
                Type = ScopeType.Identity,
                Description = "Authorization for IdentityManager",
                //Emphasize = true,
                //ShowInDiscoveryDocument = false,

                Claims = new List<ScopeClaim>
                        {
                            new ScopeClaim(Constants.ClaimTypes.Name),
                            new ScopeClaim(Constants.ClaimTypes.Role)
                        }
            });
            result.Add(new Scope
            {
                Name = "idAdmin",
                DisplayName = "IdentityAdmin",
                Description = "Authorization for IdentityAdmin",
                Type = ScopeType.Identity,
                //Emphasize = true,
                //ShowInDiscoveryDocument = false,

                Claims = new List<ScopeClaim>
                        {
                            new ScopeClaim(Constants.ClaimTypes.Name),
                            new ScopeClaim(Constants.ClaimTypes.Role)
                        }
            });
            result.AddRange(new List<Scope>
            {
                new Scope { Name = "core.accounts" },
                new Scope { Name = "core.licenses" },
                new Scope { Name = "core.organisations" },
                new Scope {
                    Name = "core.subscriptions",
                    Claims = new List<ScopeClaim> {
                        new ScopeClaim("subscription")
                    },
                    IncludeAllClaimsForUser = true
                },
                new Scope { Name = "core.bundles" },
                new Scope { Name = "prj.tasks" },
                new Scope { Name = "prj.notifications" }

            });
            result.Add(StandardScopes.OpenId);
            result.Add(StandardScopes.Profile);
            result.Add(StandardScopes.OfflineAccess);
            return result;
        }
    }
}