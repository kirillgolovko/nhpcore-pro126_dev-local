﻿using IdentityServer3.Core.Events;
using IdentityServer3.Core.Services;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityServer.Configuration
{
    class SeqEventService : IEventService
    {
        static readonly ILogger Log;

        static SeqEventService()
        {
            Log = new LoggerConfiguration()
                .WriteTo.Seq("http://my-seq.westeurope.cloudapp.azure.com:5341")
                .WriteTo.File($"{AppDomain.CurrentDomain.BaseDirectory}\\logs\\IdentityServer3\\idsrv-events.txt")
                .CreateLogger();
        }

        public Task RaiseAsync<T>(Event<T> evt)
        {
            Log.Information("{Id}: {Name} / {Category} ({EventType}), Context: {@context}, Details: {@details}",
                evt.Id,
                evt.Name,
                evt.Category,
                evt.EventType,
                evt.Context,
                evt.Details);

            return Task.FromResult(0);
        }
    }
}
