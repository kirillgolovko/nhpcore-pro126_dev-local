﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.Common
{
    public static class NhpConstants
    {
        public static class ConnectionStrings
        {
            public const string Identity = "NhpIdentityDbContext";
            public const string IdSrv = "NhpIdSrvDbContext";
            public const string Core = "NhpCoreDbContext";
        }

        public static class EmailServiceConstants
        {
            public  const string SenderDisplayedEmail = "noreply@nhp-post.info";

            public const string SenderDisplayedName = "НХП Почтальон";

            public const string SmtpHost = "mail.nhp-post.info";

            public const int SmtpPort = 587;

            public const string SmtpEmail = "noreply";

            public const string SmtpPassword = "314131Otchet";
        }

        public static class Controllers
        {
            //controllers ---
            public class Account
            {
                public const string OrganisationRegType = "organisation";
                public const string UserRegType = "user";
            }

            public class Organisation
            {
            }

            public class Subscription
            {
            }
        }

        public static class EntityTableNames
        {
            public const string OperationBase = "Operations";
            public const string OperationUserEntryBase = "OperationUserEntries";
            public const string OperationDocumentBase = "OperationDocuments";
            public const string OperationStatus = "OperationStatuses";
            public const string DocumentTemplateBase = "DocumentTemplates";
        }

        public static class Partials
        {
            public const string Header = "Header";
            public const string NavigationMenu = "NavigationMenu";
        }

        public static class NhpClaimTypes
        {
            public const string  DisplayName = "displayname";
        }
    }
}