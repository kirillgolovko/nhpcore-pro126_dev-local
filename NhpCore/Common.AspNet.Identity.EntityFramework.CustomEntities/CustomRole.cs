﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.AspNet.Identity.EntityFramework.CustomEntities
{
    public class CustomRole : IdentityRole<int, CustomUserRole> { }
}
