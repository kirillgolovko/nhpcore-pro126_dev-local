﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;

namespace NhpCore.CoreLayer.Services
{
    public interface ISubscriptionService
    {
        Task<IEnumerable<Subscription>> GetAvailable();
        Task<IEnumerable<UserSubscriptionEntry>> GetUserEntries(int subscriptionId, IEnumerable<int> userIds);

    }
}
