﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Services
{
    public interface IOrganisationService
    {
        Task<Organisation> GetOrganisation(int id);
        Task<IEnumerable<OrganisationUserEntry>> GetUserEntries(int organisationId);
        Task<IEnumerable<OrganisationUserInvite>> GetInvitedUsers(int organisationId);
        Task<OrganisationUserInvite> GetInvitedUser(Guid inviteId);
        Task<OrganisationUserRoleType> GetOrganisationUserRoleTypeByTitle(string title);
        Task<IEnumerable<CustomUser>> GetIdentityUsers(IEnumerable<string> emails);
        Task<IEnumerable<OrganisationDocumentsStorageAccess>> GetStorageAccess(int organisationId);
    }
}