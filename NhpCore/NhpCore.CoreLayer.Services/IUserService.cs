﻿using NhpCore.CoreLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhpCore.CoreLayer.Services
{
    public interface IUserService
    {
        Task<User> GetCoreUser(int id);

        Task<User> GetCoreUserByIdentityId(int identityId);

        Task<CustomUser> GetIdentityUserById(int identityId);

        Task<OrganisationUserEntry> GetUserEntry(Guid userEntryId);

        Task<IEnumerable<Organisation>> GetAvailableOrganisations(int userId);
        Task<IEnumerable<Organisation>> GetAvailableOrganisationsByIdentityId(int identityId);

        Task<OrganisationUserInvite> GetUserInvite(Guid id);
        Task<OrganisationUserInvite> GetUserInviteByInviteCode(string inviteCode);
        Task<OrganisationUserInvite> GetUserInviteByInviteCode(Guid inviteCode);
        Task<string> GetDisplayName(int identityId);
        Task<OrganisationUserEntry> GetUserEntryByIdentityId(int organisationId, int identityUserId);
        //Task<OrganisationUserProfile> GetOrganisationUserProfile(Guid id);
        Task<UserProfile> GetUserProfile(int id);
        Task<IEnumerable<UserProfile>> GetAvailableProfiles(int userId);
    }
}