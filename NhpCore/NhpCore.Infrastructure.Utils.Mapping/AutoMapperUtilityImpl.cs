﻿using System;
using System.Collections.Generic;
using AutoMapper;
using JetBrains.Annotations;
using AutoMapperInterface = AutoMapper.IMapper;
using Common.Mapping;

namespace NhpCore.Infrastructure.Utils.Mapping
{
    /// <summary>
    /// Реализация маппера с использованием AutoMapper
    /// </summary>
    public class AutoMapperUtilityImpl : IMapperUtility
    {        
        private AutoMapperInterface _mapper;
        private bool _initialized = false;

        /// <summary>
        /// Инициализирует маппер, чтобы он использовал конфигурацию
        /// </summary>
        /// <param name="configuration">конфигурация, через которую будет работать маппе</param>
        /// <exception cref="MappingException">Возникает когда несколько раз пытаются инициализировать маппер</exception>
        public void Initialize([NotNull]MapperConfiguration configuration)
        {
            if (_initialized)
                throw new MappingException("Mapper already initialized");

            _mapper = configuration.CreateMapper();
            _initialized = true;
        }

        public TDestination Map<TSource, TDestination>(TSource source)
        {
            if (!_initialized)
                throw new MappingException("Mapper not initialized");            

            try
            {
                return _mapper.Map<TSource, TDestination>(source);
            }
            catch (Exception ex)
            {
                throw new MappingException("Mapping failed. See inner exception", ex);
            }
        }

        public IEnumerable<TDestination> MapEnumerable<TSource, TDestination>(IEnumerable<TSource> source)
        {
            return Map<IEnumerable<TSource>, IEnumerable<TDestination>>(source);
        }
    }
}
