﻿using Autofac;
using Common.Commands;
using NhpCore.Business.Commands;
using NhpCore.CoreLayer.Services;
using NhpCore.Infrastructure.Data.Ef.Services;

namespace NhpCore.Infrastructure.DependencyResolution
{
    public class BusinessServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            #region Services

            builder.RegisterType<DepartmentService>().As<IDepartmentService>();
            builder.RegisterType<OrganisationService>().As<IOrganisationService>();
            builder.RegisterType<SubscriptionService>().As<ISubscriptionService>();
            builder.RegisterType<UserService>().As<IUserService>();

            #endregion Services

            #region Organisation

            builder.RegisterType<RegisterOrganisationCommand>()
                .As<IBusinessCommand<RegisterOrganisationArgs, RegisterOrganisationResult>>();

            builder.RegisterType<CreateOrganisationCommand>()
                .As<IBusinessCommand<CreateOrganisationArgs, CreateOrganisationResult>>();

            builder.RegisterType<DeleteOrganisationCommand>()
                .As<IBusinessCommand<DeleteOrganisationArgs, DeleteOrganisationResult>>();

            builder.RegisterType<EditOrganisationCommand>()
                .As<IBusinessCommand<EditOrganisationArgs, EditOrganisationResult>>();

            builder.RegisterType<EditOrganisationDetailsCommand>()
                .As<IBusinessCommand<EditOrganisationDetailsArgs, EditOrganisationDetailsResult>>();

            #endregion Organisation

            #region OrganisationUserEntry

            builder.RegisterType<CreateUserEntryCommand>()
                .As<IBusinessCommand<CreateUserEntryArgs, CreateUserEntryResult>>();

            builder.RegisterType<DeleteUserEntryCommand>().
                As<IBusinessCommand<DeleteUserEntryArgs, DeleteUserEntryResult>>();

            //builder.RegisterType<EditOrganisationUserProfileCommand>()
            //    .As<IBusinessCommand<EditOrganisationUserProfileArgs, EditOrganisationUserProfileResult>>();

            #endregion OrganisationUserEntry

            #region OrganisationUserInvite

            builder.RegisterType<CreateUserInviteCommand>()
                .As<IBusinessCommand<CreateUserInviteArgs, CreateUserInviteResult>>();

            builder.RegisterType<DeleteUserInviteCommand>()
                .As<IBusinessCommand<DeleteUserInviteArgs, DeleteUserInviteResult>>();

            builder.RegisterType<InviteUsersCommand>()
                .As<IBusinessCommand<InviteUsersArgs, InviteUsersResult>>();

            builder.RegisterType<AcceptUserInviteCommand>()
                .As<IBusinessCommand<AcceptUserInviteArgs, AcceptUserInviteResult>>();

            builder.RegisterType<SetUserAndUserEntryToUserInviteCommand>()
                .As<IBusinessCommand<SetUserAndUserEntryToUserInviteArgs, SetUserAndUserEntryToUserInviteResult>>();

            #endregion

            #region OrganisationUserRole

            builder.RegisterType<CreateUserRoleCommand>()
                .As<IBusinessCommand<CreateUserRoleArgs, CreateUserRoleResult>>();

            #endregion

            #region OrganisationDocumentsStorageAccess

            builder.RegisterType<AddStorageCredentialsCommand>()
                .As<IBusinessCommand<AddStorageCredentialsArgs, AddStorageCredentialsResult>>();

            #endregion

            #region User

            builder.RegisterType<CreateUserCommand>()
                .As<IBusinessCommand<CreateUserArgs, CreateUserResult>>();

            builder.RegisterType<EditUserCommand>()
                .As<IBusinessCommand<EditUserArgs, EmptyBusinessCommandResult>>();

            builder.RegisterType<DeleteUserCommand>()
                .As<IBusinessCommand<DeleteUserArgs, EmptyBusinessCommandResult>>();

            #endregion User

            #region Subscription

            builder.RegisterType<CreateSubscriptionCommand>()
                .As<IBusinessCommand<CreateSubscriptionArgs, CreateSubscriptionResult>>();

            builder.RegisterType<EditSubscriptionCommand>()
                .As<IBusinessCommand<EditSubscriptionArgs, EditSubscriptionResult>>();

            builder.RegisterType<DeleteSubscriptionCommand>()
                .As<IBusinessCommand<DeleteSubscriptionArgs, DeleteSubscriptionResult>>();

            #endregion Subscription

            #region SubscriptionOption

            builder.RegisterType<CreateSubscriptionOptionCommand>()
                .As<IBusinessCommand<CreateSubscriptionOptionArgs, CreateSubscriptionOptionResult>>();

            builder.RegisterType<EditSubscriptionOptionCommand>()
                .As<IBusinessCommand<EditSubscriptionOptionArgs, EditSubscriptionOptionResult>>();

            builder.RegisterType<DeleteSubscriptionOptionCommand>()
                .As<IBusinessCommand<DeleteSubscriptionOptionArgs, DeleteSubscriptionOptionResult>>();

            #endregion SubscriptionOption

            #region SubscriptionOperation

            builder.RegisterType<CreateSubscriptionOperationCommand>()
                .As<IBusinessCommand<CreateSubscriptionOperationArgs, CreateSubscriptionOperationResult>>();

            builder.RegisterType<EditSubscriptionOperationCommand>()
                .As<IBusinessCommand<EditSubscriptionOperationArgs, EditSubscriptionOperationResult>>();

            builder.RegisterType<DeleteSubscriptionOperationCommand>()
                .As<IBusinessCommand<DeleteSubscriptionOperationArgs, DeleteSubscriptionOperationResult>>();

            #endregion SubscriptionOperation

            #region UserProfile

            builder.RegisterType<CreateUserProfileCommand>()
                .As<IBusinessCommand<CreateUserProfileArgs, CreateUserProfileResult>>();

            builder.RegisterType<EditUserProfileCommand>()
                .As<IBusinessCommand<EditUserProfileArgs, EditUserProfileResult>>();

            builder.RegisterType<DeleteUserProfileCommand>()
                .As<IBusinessCommand<DeleteUserProfileArgs, EmptyBusinessCommandResult>>();

            #endregion UserProfile
        }
    }
}