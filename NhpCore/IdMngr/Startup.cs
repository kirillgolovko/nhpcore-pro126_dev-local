﻿using IdentityManager.Configuration;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
namespace IdMngr
{
    public static class ClaimsExtensions
    {
        public static string GetValue(this IEnumerable<Claim> claims, string type)
        {
            if (claims == null) throw new ArgumentNullException("claims");
            if (String.IsNullOrWhiteSpace(type)) throw new ArgumentNullException("type");

            var claim = claims.FirstOrDefault(x => x.Type == type);
            if (claim != null)
            {
                return claim.Value;
            }

            return null;
        }
    }
    public class Startup
    {
        bool _isSslRequired;
        string _connstrIdentity = "NhpCoreDbContext";

        public void Configuration(IAppBuilder app)
        {
            app.UseOpenIdConnectAuthentication(new Microsoft.Owin.Security.OpenIdConnect.OpenIdConnectAuthenticationOptions
            {
                AuthenticationType = "oidc",
                Authority = "http://localhost:57556/identity",
                ClientId = "idmgr_client",
                RedirectUri = "http://localhost:62050/identity-manager",
                ResponseType = "id_token",
                UseTokenLifetime = false,
                Scope = "openid idmgr",
                SignInAsAuthenticationType = "Cookies",
                Notifications = new Microsoft.Owin.Security.OpenIdConnect.OpenIdConnectAuthenticationNotifications
                {
                    SecurityTokenValidated = n =>
                    {
                        n.AuthenticationTicket.Identity.AddClaim(new Claim("id_token", n.ProtocolMessage.IdToken));
                        return Task.FromResult(0);
                    },
                    RedirectToIdentityProvider = async n =>
                    {
                        if (n.ProtocolMessage.RequestType == Microsoft.IdentityModel.Protocols.OpenIdConnectRequestType.LogoutRequest)
                        {
                            var res = await n.OwinContext.Authentication.AuthenticateAsync("Cookies");
                            if (res != null)
                            {
                                var id_token = res.Identity.Claims.GetValue("id_token");

                                if (id_token != null)
                                {
                                    n.ProtocolMessage.IdTokenHint = id_token;
                                    n.ProtocolMessage.PostLogoutRedirectUri = "http://localhost:62050/identity-manager";
                                }
                            }
                        }
                    }
                }
            });

            app.ConfigureIdentityManager(GetOptions());
        }

        private IdentityManagerConfigurationOptions GetOptions()
        {
            IdentityManagerConfigurationOptions result = new IdentityManagerConfigurationOptions();

            result.RequireSsl = _isSslRequired;
            result.IdentityDbConnStr = _connstrIdentity;

            return result;

        }
    }
}