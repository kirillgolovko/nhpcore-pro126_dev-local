﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.EntityFramework;

namespace NhpProject.Core.Entities
{
    public class ProgramVersion : IEntity
    {
        #region Properties

        public int Id { get; set; }

        [MaxLength(10)]
        [Required]
        public string Version { get; set; }

        public bool IsActive { get; set; }

        #endregion Properties
    }
}