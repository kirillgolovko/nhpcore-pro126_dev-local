namespace NhpProject.Core.Entities
{
    using Common.EntityFramework;
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("NHP_AdvancedDescription")]
    public partial class AdvancedDescription : IEntity
    {
        #region Properties

        public int Id { get; set; }

        public int TaskId { get; set; }

        public int? FileSize { get; set; }

        public byte[] Description { get; set; }

        public virtual TaskEntity Task { get; set; }

        #endregion Properties
    }
}