namespace NhpProject.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Entities;
    using Common.EntityFramework;

    [Table("NHP_Tasks")]
    public partial class TaskEntity : IEntity
    {
        #region Constructors

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TaskEntity()
        {
            AdvancedDescriptions = new HashSet<AdvancedDescription>();
            TaskAllerts = new HashSet<TaskAllert>();
            TaskNotes = new HashSet<TaskNote>();
            UserRoles = new HashSet<UserRole>();
        }

        #endregion Constructors

        #region Properties

        [Key]
        public int TaskID { get; set; }

        public int? OrganisationId { get; set; }

        [StringLength(250)]
        public string Name { get; set; }

        [Column("Creator_F")]
        public int? CreatorId { get; set; }

        public int? ParentTask { get; set; }

        [StringLength(16777215)]
        public string Description { get; set; }

        public bool IsProject { get; set; }

        public int? ProjTaskID { get; set; }

        public Guid Guid { get; set; }

        public bool IsCompleted { get; set; }

        public int Progress { get; set; }

        public bool IsMilestone { get; set; }

        public DateTime? Deadline { get; set; }

        [Column("StartDate")]
        public DateTime? StartDate { get; set; }

        public DateTime? StartDateFact { get; set; }

        [Column("FinishDate")]
        public DateTime? EndDateFact { get; set; }

        public bool IsUnderControl { get; set; }

        public int? RemindFor { get; set; }

        public int? LastRemind { get; set; }

        [Column("EndDate")]
        public DateTime? EndDatePlan { get; set; }

        public int? TaskStateId { get; set; }

        public int Position_in_parent { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdvancedDescription> AdvancedDescriptions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TaskAllert> TaskAllerts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TaskNote> TaskNotes { get; set; }

        public virtual TaskStateEntity TaskState { get; set; }

        public virtual User Creator { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserRole> UserRoles { get; set; }

        #endregion Properties
    }
}