namespace NhpProject.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Entities;
    using Common.EntityFramework;

    [Table("NHP_TaskUserState")]
    public partial class TaskUserState : IEntity
    {
        #region Constructors

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TaskUserState()
        {
            UserRoles = new HashSet<UserRole>();
        }

        #endregion Constructors

        #region Properties

        public int Id { get; set; }

        [StringLength(1073741823)]
        public string Name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserRole> UserRoles { get; set; }

        #endregion Properties
    }
}