namespace NhpProject.Core.Entities
{
    using Common.EntityFramework;
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("NHP_AllowToNotification")]
    public partial class AllowToNotification : IEntity
    {
        #region Properties

        public int Id { get; set; }

        public int OrganisationId { get; set; }

        public bool Allow { get; set; }

        public virtual Organisation Organisation { get; set; }

        #endregion Properties
    }
}