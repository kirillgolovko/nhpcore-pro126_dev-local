namespace NhpProject.Core.Entities
{
    using Common.EntityFramework;
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("NHP_TaskNotes")]
    public partial class TaskNote : IEntity
    {
        #region Properties

        [Key]
        public int NoteID { get; set; }

        public int TaskID { get; set; }

        [StringLength(1073741823)]
        public string Text { get; set; }

        public virtual TaskEntity Task { get; set; }

        #endregion Properties
    }
}