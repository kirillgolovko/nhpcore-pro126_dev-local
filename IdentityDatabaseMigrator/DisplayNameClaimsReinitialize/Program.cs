﻿using NhpCore.Infrastructure.Data.Ef;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NhpCore.CoreLayer.Entities;

namespace DisplayNameClaimsReinitialize
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = "data source=nhp-soft.database.windows.net;initial catalog=NhpCore-Identity;persist security info=True;user id=autumninthecity240490;password=Helgard240490;MultipleActiveResultSets=True;App=EntityFramework";
            using (var db = new NhpIdentityDbContext())
            {
                var users = db.Users.ToList();
                users.ForEach(x =>
                {
                    var claim = x.Claims.FirstOrDefault(c => c.ClaimType == "displayname");

                    if (claim == null)
                    {
                        x.Claims.Add(new CustomUserClaim() { ClaimType = "displayname", ClaimValue = x.UserName, UserId = x.Id });
                    }
                });

                db.SaveChanges();
            }
        }
    }
}
