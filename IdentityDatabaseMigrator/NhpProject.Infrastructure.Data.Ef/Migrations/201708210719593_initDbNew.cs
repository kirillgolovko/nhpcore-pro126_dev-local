namespace NhpProject.Infrastructure.Data.Ef
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initDbNew : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccessRoles",
                c => new
                    {
                        RoleValue = c.Int(nullable: false),
                        RoleID = c.Int(nullable: false, identity: true),
                        RoleDescription = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.RoleValue);
            
            CreateTable(
                "dbo.NHP_Users",
                c => new
                    {
                        UserID = c.Int(nullable: false),
                        OrganisationId = c.Int(),
                        Email = c.String(nullable: false, maxLength: 45),
                        Password = c.String(nullable: false, maxLength: 45),
                        DisplayName = c.String(maxLength: 45),
                        FirstName = c.String(maxLength: 45),
                        LastName = c.String(maxLength: 45),
                        PatronymicName = c.String(maxLength: 45),
                        Salary = c.Int(),
                        DepartmentID_F = c.Int(),
                        IsDepartmentChief = c.Boolean(nullable: false),
                        AccessRole_F = c.Int(nullable: false),
                        IsMailSended = c.Boolean(nullable: false),
                        PhoneNumber = c.String(maxLength: 20),
                        TelegramUserName = c.String(maxLength: 45),
                        TelegramChatId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsBlocked = c.Boolean(nullable: false),
                        IdentityId = c.Int(nullable: false),
                        CoreId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("dbo.NHP_Organisations", t => t.OrganisationId)
                .ForeignKey("dbo.NHP_Departments", t => t.DepartmentID_F)
                .ForeignKey("dbo.AccessRoles", t => t.AccessRole_F)
                .Index(t => t.OrganisationId)
                .Index(t => t.DepartmentID_F)
                .Index(t => t.AccessRole_F);
            
            CreateTable(
                "dbo.NHP_Departments",
                c => new
                    {
                        DepartmentID = c.Int(nullable: false, identity: true),
                        OrganisationId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 150),
                        Description = c.String(maxLength: 250),
                        CoreId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DepartmentID)
                .ForeignKey("dbo.NHP_Organisations", t => t.OrganisationId, cascadeDelete: true)
                .Index(t => t.OrganisationId);
            
            CreateTable(
                "dbo.NHP_Organisations",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 250),
                        RegisterDate = c.DateTime(nullable: false),
                        CoreId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NHP_AllowToNotification",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrganisationId = c.Int(nullable: false),
                        Allow = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NHP_Organisations", t => t.OrganisationId, cascadeDelete: true)
                .Index(t => t.OrganisationId);
            
            CreateTable(
                "dbo.NHP_Licensing",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrganisationId = c.Int(nullable: false),
                        LicenseExpire = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NHP_Organisations", t => t.OrganisationId, cascadeDelete: true)
                .Index(t => t.OrganisationId);
            
            CreateTable(
                "dbo.NHP_TaskAllerts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        AllertDate = c.DateTime(nullable: false),
                        OrganisationId = c.Int(nullable: false),
                        AllertText = c.String(maxLength: 1000),
                        IsSended = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NHP_Tasks", t => t.TaskId, cascadeDelete: true)
                .ForeignKey("dbo.NHP_Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.TaskId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.NHP_Tasks",
                c => new
                    {
                        TaskID = c.Int(nullable: false, identity: true),
                        OrganisationId = c.Int(),
                        Name = c.String(maxLength: 250),
                        Creator_F = c.Int(),
                        ParentTask = c.Int(),
                        Description = c.String(),
                        IsProject = c.Boolean(nullable: false),
                        ProjTaskID = c.Int(),
                        Guid = c.Guid(nullable: false),
                        IsCompleted = c.Boolean(nullable: false),
                        Progress = c.Int(nullable: false),
                        IsMilestone = c.Boolean(nullable: false),
                        Deadline = c.DateTime(),
                        StartDate = c.DateTime(),
                        StartDateFact = c.DateTime(),
                        FinishDate = c.DateTime(),
                        IsUnderControl = c.Boolean(nullable: false),
                        RemindFor = c.Int(),
                        LastRemind = c.Int(),
                        EndDate = c.DateTime(),
                        TaskStateId = c.Int(),
                        Position_in_parent = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TaskID)
                .ForeignKey("dbo.NHP_TaskStates", t => t.TaskStateId)
                .ForeignKey("dbo.NHP_Users", t => t.Creator_F)
                .Index(t => t.Creator_F)
                .Index(t => t.TaskStateId);
            
            CreateTable(
                "dbo.NHP_AdvancedDescription",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskId = c.Int(nullable: false),
                        FileSize = c.Int(),
                        Description = c.Binary(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NHP_Tasks", t => t.TaskId, cascadeDelete: true)
                .Index(t => t.TaskId);
            
            CreateTable(
                "dbo.NHP_TaskNotes",
                c => new
                    {
                        NoteID = c.Int(nullable: false, identity: true),
                        TaskID = c.Int(nullable: false),
                        Text = c.String(),
                    })
                .PrimaryKey(t => t.NoteID)
                .ForeignKey("dbo.NHP_Tasks", t => t.TaskID, cascadeDelete: true)
                .Index(t => t.TaskID);
            
            CreateTable(
                "dbo.NHP_TaskStates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 45),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NHP_UserRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrganisationId = c.Int(nullable: false),
                        TaskId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        TaskUserStateId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NHP_Organisations", t => t.OrganisationId, cascadeDelete: true)
                .ForeignKey("dbo.NHP_Tasks", t => t.TaskId, cascadeDelete: true)
                .ForeignKey("dbo.NHP_TaskUserState", t => t.TaskUserStateId, cascadeDelete: true)
                .ForeignKey("dbo.NHP_Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.OrganisationId)
                .Index(t => t.TaskId)
                .Index(t => t.UserId)
                .Index(t => t.TaskUserStateId);
            
            CreateTable(
                "dbo.NHP_TaskUserState",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NHP_UserNotificationPreferences",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        IsEmail = c.Boolean(nullable: false),
                        IsTelegram = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.NHP_Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserNotificationStatus",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        LastDateOfDailyStartingTaskSended = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NHP_Users", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.NHP_AssignedTasks",
                c => new
                    {
                        OrganisationId = c.Int(nullable: false),
                        Employee_F = c.Int(nullable: false),
                        Task_F = c.Int(nullable: false),
                        Project_F = c.Int(),
                    })
                .PrimaryKey(t => new { t.OrganisationId, t.Employee_F, t.Task_F });
            
            CreateTable(
                "dbo.ProgramVersions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Version = c.String(nullable: false, maxLength: 10),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NHP_Structure",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        OrganisationId = c.Int(nullable: false),
                        ParentID = c.Int(),
                        ParentType = c.Boolean(),
                        CurrentID = c.Int(nullable: false),
                        CurrentType = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.OrganisationUsers",
                c => new
                    {
                        Organisation_Id = c.Int(nullable: false),
                        User_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Organisation_Id, t.User_Id })
                .ForeignKey("dbo.NHP_Organisations", t => t.Organisation_Id, cascadeDelete: true)
                .ForeignKey("dbo.NHP_Users", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Organisation_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NHP_Users", "AccessRole_F", "dbo.AccessRoles");
            DropForeignKey("dbo.UserNotificationStatus", "Id", "dbo.NHP_Users");
            DropForeignKey("dbo.NHP_UserNotificationPreferences", "UserId", "dbo.NHP_Users");
            DropForeignKey("dbo.NHP_Tasks", "Creator_F", "dbo.NHP_Users");
            DropForeignKey("dbo.NHP_TaskAllerts", "UserId", "dbo.NHP_Users");
            DropForeignKey("dbo.NHP_UserRoles", "UserId", "dbo.NHP_Users");
            DropForeignKey("dbo.NHP_UserRoles", "TaskUserStateId", "dbo.NHP_TaskUserState");
            DropForeignKey("dbo.NHP_UserRoles", "TaskId", "dbo.NHP_Tasks");
            DropForeignKey("dbo.NHP_UserRoles", "OrganisationId", "dbo.NHP_Organisations");
            DropForeignKey("dbo.NHP_Tasks", "TaskStateId", "dbo.NHP_TaskStates");
            DropForeignKey("dbo.NHP_TaskNotes", "TaskID", "dbo.NHP_Tasks");
            DropForeignKey("dbo.NHP_TaskAllerts", "TaskId", "dbo.NHP_Tasks");
            DropForeignKey("dbo.NHP_AdvancedDescription", "TaskId", "dbo.NHP_Tasks");
            DropForeignKey("dbo.NHP_Users", "DepartmentID_F", "dbo.NHP_Departments");
            DropForeignKey("dbo.NHP_Users", "OrganisationId", "dbo.NHP_Organisations");
            DropForeignKey("dbo.OrganisationUsers", "User_Id", "dbo.NHP_Users");
            DropForeignKey("dbo.OrganisationUsers", "Organisation_Id", "dbo.NHP_Organisations");
            DropForeignKey("dbo.NHP_Licensing", "OrganisationId", "dbo.NHP_Organisations");
            DropForeignKey("dbo.NHP_Departments", "OrganisationId", "dbo.NHP_Organisations");
            DropForeignKey("dbo.NHP_AllowToNotification", "OrganisationId", "dbo.NHP_Organisations");
            DropIndex("dbo.OrganisationUsers", new[] { "User_Id" });
            DropIndex("dbo.OrganisationUsers", new[] { "Organisation_Id" });
            DropIndex("dbo.UserNotificationStatus", new[] { "Id" });
            DropIndex("dbo.NHP_UserNotificationPreferences", new[] { "UserId" });
            DropIndex("dbo.NHP_UserRoles", new[] { "TaskUserStateId" });
            DropIndex("dbo.NHP_UserRoles", new[] { "UserId" });
            DropIndex("dbo.NHP_UserRoles", new[] { "TaskId" });
            DropIndex("dbo.NHP_UserRoles", new[] { "OrganisationId" });
            DropIndex("dbo.NHP_TaskNotes", new[] { "TaskID" });
            DropIndex("dbo.NHP_AdvancedDescription", new[] { "TaskId" });
            DropIndex("dbo.NHP_Tasks", new[] { "TaskStateId" });
            DropIndex("dbo.NHP_Tasks", new[] { "Creator_F" });
            DropIndex("dbo.NHP_TaskAllerts", new[] { "UserId" });
            DropIndex("dbo.NHP_TaskAllerts", new[] { "TaskId" });
            DropIndex("dbo.NHP_Licensing", new[] { "OrganisationId" });
            DropIndex("dbo.NHP_AllowToNotification", new[] { "OrganisationId" });
            DropIndex("dbo.NHP_Departments", new[] { "OrganisationId" });
            DropIndex("dbo.NHP_Users", new[] { "AccessRole_F" });
            DropIndex("dbo.NHP_Users", new[] { "DepartmentID_F" });
            DropIndex("dbo.NHP_Users", new[] { "OrganisationId" });
            DropTable("dbo.OrganisationUsers");
            DropTable("dbo.NHP_Structure");
            DropTable("dbo.ProgramVersions");
            DropTable("dbo.NHP_AssignedTasks");
            DropTable("dbo.UserNotificationStatus");
            DropTable("dbo.NHP_UserNotificationPreferences");
            DropTable("dbo.NHP_TaskUserState");
            DropTable("dbo.NHP_UserRoles");
            DropTable("dbo.NHP_TaskStates");
            DropTable("dbo.NHP_TaskNotes");
            DropTable("dbo.NHP_AdvancedDescription");
            DropTable("dbo.NHP_Tasks");
            DropTable("dbo.NHP_TaskAllerts");
            DropTable("dbo.NHP_Licensing");
            DropTable("dbo.NHP_AllowToNotification");
            DropTable("dbo.NHP_Organisations");
            DropTable("dbo.NHP_Departments");
            DropTable("dbo.NHP_Users");
            DropTable("dbo.AccessRoles");
        }
    }
}
