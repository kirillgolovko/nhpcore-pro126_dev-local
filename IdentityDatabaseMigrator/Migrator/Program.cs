﻿using DbUserToIdentity;
using NhpCore.Infrastructure.Data.Ef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreCustomUser = NhpCore.CoreLayer.Entities.CustomUser;
using NhpProject.Core.Entities;
using System.IO;

namespace Migrator
{
    class Program
    {
        static void Main(string[] args)
        {
            var projUsers = Migration.Start();
            using (StreamWriter sw = new StreamWriter("D:\\projectUsers.txt"))
            {

                projUsers.ForEach(x =>
                {
                    string line = $"{x.Id}:{x.Email}:{x.Password};";
                    sw.Write(line);
                });
            }
            Console.WriteLine("finished");
            Console.ReadLine();
        }
    }
}
